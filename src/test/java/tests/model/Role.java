package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Role {
    private String entity, event, domain, roleName, roleType, targetStatus;
    private JSONArray channel;
    private JSONArray subProduct;
    private JSONArray accounts;
    private JSONArray virtualAccounts;

    public Role(JSONObject object) {
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setDomain(object.get("domain").toString());
        setRoleName(object.get("roleName").toString());
        setRoleType(object.get("roleType").toString());
        setChannel((JSONArray) object.get("channel"));
        setSubProduct((JSONArray) object.get("subProducts"));
        setAccounts((JSONArray) object.get("accounts"));
        setVirtualAccounts((JSONArray) object.get("virtualAccounts"));
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    private void setDomain(String domain) {
        this.domain = domain;
    }

    private void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    private void setRoleType(String roleType) {
        this.roleType = roleType;
    }


    private void setChannel(JSONArray channel) {
        this.channel = channel;
    }

    private void setSubProduct(JSONArray subProduct) {
        this.subProduct = subProduct;
    }

    private void setAccounts(JSONArray accounts) {
        this.accounts = accounts;
    }

    private void setVirtualAccounts(JSONArray virtualAccounts) {
        this.virtualAccounts = virtualAccounts;
    }

    public String getDomain() {
        return domain;
    }

    public String getRoleName() {
        return roleName;
    }

    public String getRoleType() {
        return roleType;
    }

    public JSONArray getChannel() {
        return channel;
    }

    public JSONArray getSubProduct() {
        return subProduct;
    }

    public JSONArray getAccounts() {
        return accounts;
    }

    public JSONArray getVirtualAccounts() {
        return virtualAccounts;
    }
}
