package tests;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import framework.JSONDataProvider;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.Login;
import pages.LoginEnhance;
import pages.users.Confirmation;
import pages.users.Create;
import pages.users.Review;
import pages.users.Summary;
import tests.model.User;


public class UserCreation {
    // local vars
    private LoginEnhance<WebElement> login = null;
    private pages.users.Summary<WebElement> userSummary = null;
    private Create<WebElement> userCreate = null;
    private Review<WebElement> userReview = null;
    private Confirmation<WebElement> userConfirm = null;

    private static final String DATA_FILE = "src\\data\\CreateUser.json";


    // constructor
    public UserCreation() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes

        login = new LoginEnhance<>();
        userSummary = new pages.users.Summary<WebElement>();
        userCreate = new pages.users.Create<WebElement>();
        userReview = new pages.users.Review<WebElement>();
        userConfirm = new pages.users.Confirmation<WebElement>();

        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;

    }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {

    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {
         }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);
        userSummary.navigateToUser();
        login.logoutTargetApp();

    }

    // test methods

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_createUser(String rowID,
                                   String description,
                                   JSONObject testData) throws Exception {

        User user = new User(testData);
        String targetStatus = user.getTargetStatus();
        String entity = user.getEntity();
        String event = user.getEvent();
        String orgName = user.getOrganisation().get("Name").toString();
        String orgAlias = user.getOrganisation().get("Alias").toString();
        String domain = user.getOrganisation().get("domain").toString();
        String domainId = user.getOrganisation().get("domainId").toString();
        String entityOrg = user.getOrganisation().get("entityOrg").toString();
        String primarySignIn = user.getOrganisation().get("primarySignIn").toString();
        String secondarySignIn = user.getOrganisation().get("secondarySignIn").toString();
        String languageRegion = user.getUserPreference().get("languageRegion").toString();
        String amountFormat = user.getUserPreference().get("amountFormat").toString();
        String dateFormat = user.getUserPreference().get("dateFormat").toString();
        String timeFormat = user.getUserPreference().get("timeFormat").toString();
        String referenceCurrency = user.getUserPreference().get("referenceCurrency").toString();
        String currencyDefault = user.getUserPreference().get("currencyDefault").toString();
        String userName = user.getUserInfo().get("userName").toString();
        String surName = user.getUserInfo().get("surName").toString();
        String loginId = user.getUserInfo().get("loginId").toString();
        String loginIdOrgAlias = user.getUserInfo().get("loginIdOrgAlias").toString();
        String mobileCode = user.getUserInfo().get("mobileCode").toString();
        String mobileValue = user.getUserInfo().get("mobileValue").toString();
        String email = user.getUserInfo().get("email").toString();
        String udomain = user.getUserInfo().get("domain").toString();
        String udomainId = user.getUserInfo().get("domainId").toString();
        String udomainRole = user.getUserInfo().get("domainRole").toString();
        String uprimaryEntity = user.getUserInfo().get("primaryEntity").toString();
        String uprimarySignIn = user.getUserInfo().get("primarySignIn").toString();
        String uprimaryTokenId = user.getUserInfo().get("primaryTokenId").toString();
        String usecondarySignIn = user.getUserInfo().get("secondarySignIn").toString();
        String usecondaryTokenId = user.getUserInfo().get("secondaryTokenId").toString();
        String txnAuthType = user.getUserInfo().get("transactionAuthType").toString();
        String txnAuthToken = user.getUserInfo().get("transactionAuthToken").toString();
        String txnAuthThresholdAmount = user.getUserInfo().get("transactionAuthThresholdAmount").toString();

        System.out.println(
                entity+", "+targetStatus+", "+
                event+", "+
                orgName +", "
                        +orgAlias+", "
                        +domain +", "
                        +domainId+", "
                        +entityOrg+", "
                        +primarySignIn+", "
                        +secondarySignIn+", "
                        +languageRegion+", "
                        +amountFormat+", "+dateFormat+", "+timeFormat+", "+referenceCurrency+", "
                        +currencyDefault+", "+userName+", "+surName+", "+loginId+", "+loginIdOrgAlias+", "+mobileCode+", "
                        +mobileValue+", "+email+", "+udomain+", "+udomainId+", "+udomainRole+", "+uprimaryEntity+", "
                        +uprimarySignIn+", "+uprimaryTokenId+", "+usecondarySignIn+", "+usecondaryTokenId);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();
        userCreate.loadUserForm();
        userCreate.setGeneralSettings(orgName, orgAlias, domain, domainId, entityOrg, primarySignIn, secondarySignIn, languageRegion, amountFormat, dateFormat, referenceCurrency);
        userCreate.setUserInfo(userName, surName, loginId, mobileCode, mobileValue, email, udomainRole, uprimaryTokenId, usecondarySignIn, usecondaryTokenId);
        userCreate.setTransactionAuthentication(txnAuthType, txnAuthToken, txnAuthThresholdAmount);
        userReview.verifyUserDetails();
        userConfirm.checkUserSubmission(orgName);
        userSummary.assertCreatedData(targetStatus,loginId);
    }

}
