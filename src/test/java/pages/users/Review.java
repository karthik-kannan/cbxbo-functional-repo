package pages.users;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Review<M extends WebElement> extends Base {
    // local variables


    Base base = new Base();

    //constructor
    public Review() throws Exception {
        super();

    }

    @FindBy(xpath = "//ul/li/a[contains(text(),'Roles')]")
    protected M roleTab;

    @FindBy(xpath = "//button[@type='submit']")
    protected M submitUser;

    @FindBy(xpath = "//button[@aria-label='Open']")
    protected M expandUserDetails;

    @FindBy(xpath = "//button/span[contains(text(),'Edit')]")
    protected M editUser;

    @FindBy(xpath = "//button/span[contains(text(),'Delete')]")
    protected M deleteUser;

    @FindBy(xpath = "//clr-dg-cell[1]/span[1]")
    protected M userFirstName;

    @FindBy(xpath = "//clr-dg-cell[1]/span[3]")
    protected M userLastName;

    @FindBy(xpath = "//clr-dg-cell[2]/span")
    protected M userLoginId;

    @FindBy(xpath = "//div/clr-dg-cell[3]/div")
    protected M userDomain;

    @FindBy(xpath = "//div/clr-dg-cell[4]/div")
    protected M userRole;

    @FindBy(xpath = "//div/clr-dg-cell[5]/div")
    protected M userPrimaryEntity;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[1]/div[1]/div[2]")
    protected M userMobileNo;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[1]/div[2]/div[2]")
    protected M userEmail;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]")
    protected M userLangPref;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]")
    protected M userDateTimePref;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[2]/div[2]/div[1]/div[2]/div[2]")
    protected M userAmountPref;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]")
    protected M userRefCurrencyPref;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/span")
    protected M userRefCurrencyFlag;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[3]/table/tbody/tr[1]/td[2]")
    protected M userPrimaryAuthType;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[3]/table/tbody/tr[1]/td[3]")
    protected M userPrimaryAuthTokenId;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[3]/table/tbody/tr[2]/td[2]")
    protected M userSecondaryAuthType;

    @FindBy(xpath = "//clr-dg-row-detail/div[2]/div[3]/table/tbody/tr[2]/td[3]")
    protected M userSecondaryAuthTokenId;


    public void verifyUserDetails() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(expandUserDetails,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            js.executeScript("arguments[0].scrollIntoView(true);", expandUserDetails);
            BrowserUtils.waitForVisibility(expandUserDetails,TIMEOUT_MINUTE);
            expandUserDetails.click();
            // To implement the details verification

            submit();
        }catch (Exception e){
            System.out.println("Exception in verifying the user details "+e);
        }


    }

    private void submit() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(submitUser,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            js.executeScript("arguments[0].scrollIntoView(true);", submitUser);
            if (submitUser.isDisplayed()){
                submitUser.click();
                System.out.println("Submitted user creation");
            }else{
                System.out.println("Exception in user creation from Review page");
            }

        }catch(Exception e){
            System.out.println("Exception while navigating from User form "+e);
        }



    }
}
