package pages.Mailbox;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

public class Details<M extends WebElement> extends Base {
    Base base = new Base();

    //local variables

    //constructor
    public Details() throws Exception{
        super();
    }

    //elements

    @FindBy(xpath = "//clr-dg-cell[7]/div/div[2]/button")
    protected M viewMessageDetails;

}
