package tests;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import framework.JSONDataProvider;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.Login;
import pages.LoginEnhance;
import tests.model.Deleteflow;
import tests.model.Workflow;

import static framework.Global_VARS.TARGET_URL;


public class Deleteflows {
    // local vars

    private LoginEnhance<WebElement> login = null;
    private pages.users.Summary<WebElement> userSummary = null;
    private pages.roles.Summary<WebElement> roleSummary = null;
    private pages.rules.Summary<WebElement> ruleSummary = null;
    private pages.limits.Summary<WebElement> limitSummary = null;

    private static final String DATA_FILE = "src\\data\\Deleteflow.json";


    // constructor
    public Deleteflows() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes
        login = new LoginEnhance<>();
        userSummary = new pages.users.Summary<>();
        roleSummary = new pages.roles.Summary<>();
        ruleSummary = new pages.rules.Summary<>();
        limitSummary = new pages.limits.Summary<>();

        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;

       }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {

    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {
          }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);
        userSummary.navigateToUser();
        login.logoutTargetApp();
    }

    // test methods

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_deleteflow(String rowID,
                                   String description,
                                   JSONObject testData) throws Exception {

        Deleteflow deleteflow = new Deleteflow(testData);

        String entity = deleteflow.getEntity();
        String event = deleteflow.getEvent();
        String key = deleteflow.getKey();
        String value = deleteflow.getValue();
        String action = deleteflow.getAction();
        String targetStatus = deleteflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.deleteFilter(key,value,action,targetStatus);
        } if (entity.contains("limit")){
            limitSummary.navigateToLimits();
            limitSummary.deleteFilter(key,value,action,targetStatus);

        }

    }


    }

