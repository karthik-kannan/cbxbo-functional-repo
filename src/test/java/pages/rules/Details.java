package pages.rules;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

public class Details<M extends WebElement> extends Base {
    // local variables

    Base base = new Base();

    //constructor
    public Details() throws Exception {
        super();

    }

    @FindBy(xpath = "//ul/li/a[contains(text(),'Workflow rules')]")
    protected M ruleTab;



    public void navigateToRule() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(ruleTab, Global_VARS.TIMEOUT_MINUTE);
        base.clickTheMenu(ruleTab);
    }
}
