package pages.rules;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.Base;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertNotNull;

public class Create<M extends WebElement> extends Base {

    Base base = new Base();
    
    // local variables

    // constructor
    public Create() throws Exception {
        super();

    }

    // elements
    @FindBy(xpath = "//button[contains(text(),'Create a new rule')]")
    protected M createRule;

    @FindBy(xpath = "//div/h1[contains(text(),'Create workflow rule')]")
    protected M pageTitle;

    ////div[contains(text(),'Select domain')]
    ////ng-select/div/div/div[1]
    @FindBy(xpath = "//div[@role='combobox']/input")
    protected M domainKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]/span")
    protected M domainValue;

    @FindBy(xpath = "//domain-control/div/div[1]/span")
    protected M domainId;

    @FindBy(xpath = "//input[@placeholder='Enter rule name']")
    protected M ruleName;
    //div/h1[contains(text(),'Review & Submit')]

    @FindBy(xpath = "//label[@class='clr-control-label']")
    protected M channels;

    @FindBy(xpath = "//input[@placeholder='Select start date']")
    protected M fromDate;

    @FindBy(xpath = "//clr-date-container[1]/div/div/div/button/clr-icon")
    protected M fromDateCalIcon;

    @FindBy(xpath = "//clr-datepicker-view-manager/clr-daypicker/div[2]/div[1]/button[2]")
    protected M dateManager;

    @FindBy(xpath = "//input[@placeholder='Select end date']")
    protected M toDate;

    @FindBy(xpath = "//clr-date-container[2]/div/div/div/button/clr-icon")
    protected M toDateCalIcon;

    @FindBy(xpath = "//select[@placeholder='Select workflow type']")
    protected M workFlowType;

    @FindBy(xpath = "//select[@placeholder='Select Product']")
    protected M product;

    @FindBy(xpath = "//button[@name='next']")
    protected M domainNext;

    @FindBy(xpath = "//div[@class='ui-criteria-footer ng-star-inserted']")
    protected M accountGridSummary;

    @FindBy(xpath = "//button[@name='next']")
    protected M accountNext;

    @FindBy(xpath = "//clr-dg-cell/div")
    protected M linkAccountCellData;

    @FindBy(xpath = "//div[@class='ui-criteria-footer ng-star-inserted']")
    protected M virtualaccountGridSummary;

    @FindBy(xpath = "//button[@name='next']")
    protected M vaNext;

    @FindBy(xpath = "//label[contains(text(),'Yes')]")
    protected M authYes;

    @FindBy(xpath = "//label[contains(text(),'No')]")
    protected M authNo;

    @FindBy(xpath = "//span[contains(text(),'Group roles')]")
    protected M groupRoles;

    @FindBy(xpath = "//a[contains(text(),'Add')]")
    protected M addRole;

    @FindBy(xpath = "//a[contains(text(),'Delete')]")
    protected M deleteRole;

    @FindBy(xpath = "//button/span[contains(text(),'Review & submit')]")
    protected M reviewSubmit;

    @FindBy(xpath = "//button/span[contains(text(),'Save as draft')]")
    protected M saveDraft;

    @FindBy(xpath = "//button/span[contains(text(),'Cancel')]")
    protected M cancel;

    @FindBy(xpath = "//div[1]/div/clr-select-container[1]/div/div/select")
    protected M roleCount1;

    @FindBy(xpath = "//div[1]/div/clr-select-container[2]/div/div/select")
    protected M roleValue1;

    @FindBy(xpath = "//div[2]/div[2]/clr-select-container[1]/div/div/select")
    protected M roleCount2;

    @FindBy(xpath = "//div[2]/div[2]/clr-select-container[2]/div/div/select")
    protected M roleValue2;

    @FindBy(xpath = "//div[3]/div[2]/clr-select-container[1]/div/div/select")
    protected M roleCount3;

    @FindBy(xpath = "//div[3]/div[2]/clr-select-container[2]/div/div/select")
    protected M roleValue3;


    public void loadRuleForm() {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(createRule,TIMEOUT_MINUTE);
            createRule.click();
            BrowserUtils.waitFor("Create rule",TIMEOUT_MINUTE);
            //  assertEquals(pageTitle, "Create role");
            return;
        }catch(Exception e){
            System.out.println("Exception in loading the rule creation form");
        }

    }

    public void setDomainInfo(String data1, String data2, String data3, String data4, String data5, String data6, String data7) throws Exception {
        setDomain(data1);
        setRuleName(data2);
        setChannels(data3);
        setFrom(data4);
        setTo(data5);
        setWorkFlowType(data7);
        setProduct(data6);
        saveDomainNav();
    }

    private void setWorkFlowType(String data7) throws Exception{

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(workFlowType, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            BrowserUtils.waitForClickable(workFlowType, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", workFlowType);

            if (workFlowType.isEnabled()){

               Select select = new Select(workFlowType);
               select.selectByVisibleText(data7);
            }
            System.out.println("Selected the workflow type: "+data7);
        }catch(Exception e){
            System.out.println("Exception in setting workflow type "+e);
        }


    }

    private void saveDomainNav() {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(domainNext,TIMEOUT_MINUTE);
            if(domainNext.isDisplayed()) {
                domainNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception is saving the domain info");
        }



    }

    private void setProduct(String data) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(product, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            BrowserUtils.waitForClickable(product, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", product);

            if (product.isEnabled()) {
                System.out.println("The element product is displayed and available for selection");
                Select select = new Select(product);
                select.selectByVisibleText(data);
                System.out.println("Selected the product: "+data);
            }else
            {
                System.out.println("The element product is not available");
            }
        }catch(Exception e){
            System.out.println("Exception in setting product "+e);
        }

    }

    private void setTo(String data) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(data!=null){
                BrowserUtils.waitForClickable(toDateCalIcon,TIMEOUT_ELEMENT);
                toDateCalIcon.click();
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                String toDate = getCurrentDate();
                String lookupToDateValue = "//clr-day/button[contains(text(),'"+toDate+"')]";
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                driver.findElement(By.xpath(lookupToDateValue)).click();

                //base.clickAndEnter(toDate, data);
                //assertEquals(roleName.getText(), data);
            }
        }catch (Exception e){
            System.out.println("Exception in setting from date");
        }

    }

    private void setFrom(String data) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(data!=null){
                BrowserUtils.waitForClickable(fromDateCalIcon,TIMEOUT_ELEMENT);
                fromDateCalIcon.click();
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                String fromDate = getCurrentDate();
                String lookupFromDateValue = "//clr-day/button[contains(text(),'"+fromDate+"')]";
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                driver.findElement(By.xpath(lookupFromDateValue)).click();

                //base.clickAndEnter(fromDate, data);
                //assertEquals(roleName.getText(), data);
            }
        }catch (Exception e){
            System.out.println("Exception in setting from date");
        }
    }

    private String getCurrentDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd");
        LocalDate currentDateTime = LocalDate.now();
        String dateString = currentDateTime.toString();
        System.out.println("Current Date & Time: "+dateString);
        System.out.println("Calling the split functionality to get current date");
        return splitDate(dateString);
    }

    private String splitDate(String dateString) {
        String [] dateComponent = dateString.split("-");
        String year = dateComponent[0];
        String month = dateComponent[1];
        String date = dateComponent[2];
        System.out.println("Returning the current date: "+date);
        return date;
    }

    private void setChannels(String data3) {
    }

    private void setRuleName(String data) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(data!=null){
                BrowserUtils.waitForClickable(ruleName,TIMEOUT_ELEMENT);
                base.clickAndEnter(ruleName, data);
                //assertEquals(roleName.getText(), data);
            }
        }catch (Exception e){
            System.out.println("Exception in setting rule name");
        }


    }

    private void setDomain(String data) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(data!=null){
                BrowserUtils.waitForClickable(domainKey,TIMEOUT_MINUTE);
                base.clickAndEnter(domainKey, data);
                BrowserUtils.waitForClickable(domainValue,TIMEOUT_ELEMENT);
                base.selectFilteredValue(domainValue);
                assertNotNull(domainId.getText());
                System.out.println("The Domain ID for the selected Domain is "+domainId.getText());
            }
        }catch(Exception e){
            System.out.println("Exception in setting domain"+e);
        }

    }


    public void setAccountInfo() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForGridData(accountGridSummary,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try {
            js.executeScript("arguments[0].scrollIntoView();", accountGridSummary);
            if (accountGridSummary.isDisplayed()) {
                String accountSelected = accountGridSummary.getText();
                if (accountSelected.contains("accounts selected")) {
                    saveAccountNav();
                }
            }

        } catch (Exception e) {
            System.out.println("Exception in setting account information "+e);

        }


    }

    private void saveAccountNav() {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", accountNext);
        try{

            BrowserUtils.waitForGridData(linkAccountCellData,TIMEOUT_MINUTE);
            BrowserUtils.waitForClickable(accountNext,TIMEOUT_MINUTE);
            if(accountNext.isDisplayed()) {
                accountNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception in saving the account info"+e);
        }


    }

    public void setVirtualAccountInfo() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForGridData(virtualaccountGridSummary,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try {
            js.executeScript("arguments[0].scrollIntoView();", virtualaccountGridSummary);
            if (virtualaccountGridSummary.isDisplayed()) {
                String accountSelected = virtualaccountGridSummary.getText();
                if (accountSelected.contains("virtual accounts selected")) {
                    saveVirtualAccountNav();
                }
            }

        } catch (Exception e) {
            System.out.println("Exception in setting virtual account information "+e);

        }

    }

    private void saveVirtualAccountNav() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", accountNext);
        try{
            BrowserUtils.waitForGridData(linkAccountCellData,TIMEOUT_MINUTE);
            BrowserUtils.waitForClickable(accountNext,TIMEOUT_MINUTE);
            if(accountNext.isDisplayed()) {
                accountNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception in saving the account info"+e);
        }


    }

    public void setAuthorisation(String authRequired, String roleMap, String operator, String group) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        if (authRequired.contains("No")){
            noAuthFlow();
        }else if (authRequired.contains("Yes")){
            mapAuthRoles(roleMap,operator,group);
        }
    }

    private void mapAuthRoles(String roleMap, String operator, String group) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(authYes, TIMEOUT_MINUTE);

        String[] s = roleMap.split("_");
        int roleSize = s.length;

        String data[] = new String[roleSize];

        String type = null;
        String index1 = null;
        String index2 = null;
        String index3 = null;

        System.out.println("Number of elements applicable for role mapping "+s.length);
        for (int i=0; i<roleSize; i++){
           data[i] = s[i];
            System.out.println("Values in role size is: "+data[i]);
        }


        int limit = 0;
        String roleDataCount1 = data[0].toString();
        String roleDataValue1 = data[1].toString();


        if (authYes.isEnabled()){
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", authYes);

            try{
                authYes.click();
                System.out.println("Selected the authorization required to Yes");
            }catch (Exception e){
                System.out.println("Exception in setting authorization to Yes "+e);
            }

        }

        switch (roleSize) {
            case 2:
                type = "simple";
                index1 = "2";
                index2 = "1";
                index3 = "1";
                limit = 2;
                break;
            case 4:
                type = "moderate";
                index1 = "2";
                index2 = "2";
                index3 = "2";
                limit = 4;
                break;
            case 6:
                type = "complex";
                index1 = "2";
                index2 = "3";
                index3 = "2";
                limit = 6;
                break;

        }
        String old_customRoleCountQuery = "//div["+index1+"]/div["+index2+"]/clr-select-container[1]/div/div/select";
        String old_customRoleValueQuery = "//div["+index1+"]/div["+index2+"]/clr-select-container[2]/div/div/select";

        String customRoleCountQuery = "//div["+index1+"]/div["+index2+"]/div["+index3+"]/clr-select-container[1]/div/div/select";
        String customRoleValueQuery = "//div["+index1+"]/div["+index2+"]/div["+index3+"]/clr-select-container[2]/div/div/select";

        String changed_customRoleCountQuery = "//div["+index1+"]/div["+index2+"]/div/clr-select-container[1]/div/div/select";
        String changed_customRoleValueQuery = "//div["+index1+"]/div["+index2+"]/div/clr-select-container[2]/div/div/select";

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", cancel);
        WebElement roleCount = driver.findElement(By.xpath(customRoleCountQuery));
        WebElement roleValue = driver.findElement(By.xpath(customRoleValueQuery));
        try{
            System.out.println("Expected element: "+roleCount);

           // ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", roleCount);
            BrowserUtils.waitForClickable(roleCount, TIMEOUT_MINUTE);

          Select selectRoleCount1 = new Select(roleCount);
          selectRoleCount1.selectByVisibleText(roleDataCount1);

          BrowserUtils.waitForClickable(roleValue, TIMEOUT_MINUTE);
         // ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", roleValue);
          Select selectRoleValue1 = new Select(roleValue);
            selectRoleValue1.selectByVisibleText(roleDataValue1);

          saveAuthorisationNav();
        }
        catch (Exception e){
            System.out.println("Exception in setting up the roles for authorisation "+e);
        }

    }

    private void setRole(String type, WebElement roleCount, WebElement roleValue) {
    }

    private void mapAuthRolesTemp(String roleMap, String operator, String group) {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        String[] s = roleMap.split("_");
        int roleSize = s.length;

        String data[] = new String[roleSize];

        String type = null;
        String index1 = null;
        String index2 = null;

        for (int i=0; i<roleSize; i++){
            data[i] = s[i];
        }

        System.out.println("Number of elements applicable for role mapping "+s.length);
        int limit = 0;
        String roleDataCount1 = data[0].toString();
        String roleDataValue1 = data[1].toString();
        String roleDataCount2 = data[2].toString();
        String roleDataValue2 = data[3].toString();
        String roleDataCount3 = data[4].toString();
        String roleDataValue3 = data[5].toString();

        switch (roleSize) {
            case 2:
                type = "simple";
                index1 = "1";
                index2 = "1";
                limit = 2;
                break;
            case 4:
                type = "moderate";
                index1 = "2";
                index2 = "2";
                limit = 4;
                break;
            case 6:
                type = "complex";
                index1 = "3";
                index2 = "2";
                limit = 6;
                break;

        }
        String customRoleCountQuery = "//div["+index1+"]/div["+index2+"]/clr-select-container[1]/div/div/select";
        String customRoleValueQuery = "//div["+index1+"]/div["+index2+"]/clr-select-container[2]/div/div/select";


        WebElement roleCount = driver.findElement(By.xpath(customRoleCountQuery));
        WebElement roleValue = driver.findElement(By.xpath(customRoleValueQuery));
        try{
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", roleCount);
            BrowserUtils.waitForClickable(roleCount, TIMEOUT_MINUTE);
            Select selectCount = new Select(roleCount);
            selectCount.selectByVisibleText(roleDataCount1);

            BrowserUtils.waitForClickable(roleValue, TIMEOUT_MINUTE);
            Select selectValue = new Select(roleValue);
            selectValue.selectByVisibleText(roleDataValue1);

        }catch (Exception e){
            System.out.println("Exception in setting up the roles for authorisation "+e);
        }

    }

    private void noAuthFlow() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (!authNo.isSelected()){
                //((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", authNo);
                BrowserUtils.waitForClickable(authNo,TIMEOUT_MINUTE);
                authNo.click();
            }
            saveAuthorisationNav();
        }catch (Exception e){
            System.out.println("Exception in setting no roles flow "+e);
        }
    }

    private void saveAuthorisationNav() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(reviewSubmit,TIMEOUT_MINUTE);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", reviewSubmit);
        try{
            if(reviewSubmit.isDisplayed()) {
                reviewSubmit.click();
            }
        }catch(Exception e){
            System.out.println("Exception in saving the authorisation info"+e);
        }
    }
}
