package tests;

import framework.*;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.Base;
import pages.Broadcast.Confirmation;
import pages.Broadcast.Create;
import pages.Broadcast.Review;
import pages.Broadcast.Summary;
import pages.Login;
import pages.LoginEnhance;
import tests.model.BroadcastMessage;
import tests.model.UAMflow;

@Listeners(TestNG_ConsoleRunner.class)
public class BroadcastMessageCreation {
    Logger log = Logger.getLogger(BroadcastMessageCreation.class);

    // local vars

    private LoginEnhance<WebElement> login = null;

    private pages.users.Summary<WebElement> userSummary = null;
    private pages.roles.Summary<WebElement> roleSummary = null;
    private pages.rules.Summary<WebElement> ruleSummary = null;

    private pages.Broadcast.Summary<WebElement> broadCastMessageSummary = null;
    private pages.Broadcast.Create<WebElement> broadCastMessageCreate = null;
    private pages.Broadcast.Review<WebElement> broadCastMessageReview = null;
    private pages.Broadcast.Confirmation<WebElement> broadCastMessageConfirmation = null;
    private pages.Broadcast.Details<WebElement> broadCastMessageDetails = null;

    private static final String DATA_FILE = "src\\data\\CreateBroadcastMessage.json";


    // constructor
    public BroadcastMessageCreation() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {
        Base.cleanUpData();

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes

        login = new LoginEnhance<>();
        userSummary = new pages.users.Summary<>();
        broadCastMessageSummary = new Summary<WebElement>();
        broadCastMessageCreate = new Create<WebElement>();
        broadCastMessageReview = new Review<WebElement>();
        broadCastMessageConfirmation = new Confirmation<WebElement>();


        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;
    }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {
    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {


    }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);
        userSummary.navigateToUser();
        login.logoutTargetApp();

    }

    // test methods

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_createbroadcastmessage(String rowID,
                                   String description,
                                   JSONObject testData) throws Exception {

        BroadcastMessage broadcastMessage = new BroadcastMessage(testData);

        String entity = broadcastMessage.getEntity();
        String event = broadcastMessage.getEvent();
        String targetStatus = broadcastMessage.getTargetStatus();
        String key = broadcastMessage.getKey();
        String value = broadcastMessage.getValue();
        String validFromDate = broadcastMessage.getValidFromDate();
        String validFromTime = broadcastMessage.getValidFromTime();
        String validToDate = broadcastMessage.getValidToDate();
        String validToTime = broadcastMessage.getValidToTime();
        String type = broadcastMessage.getApplicableFor().get("context").toString();
        String contextMap = broadcastMessage.getApplicableFor().get("contextValue").toString();
        String language = broadcastMessage.getMessage().get("language").toString();
        String messageValue = broadcastMessage.getMessage().get("messageValue").toString();
        String isPrimary = broadcastMessage.getMessage().get("isPrimary").toString();

        System.out.println(entity+", "+event+", "+targetStatus+", "+key+", "+value+", "+validFromDate+", "+validFromTime+", "
                +validToDate+", "+validToTime+", "+type+", "+contextMap+", "+language+", "+messageValue+", "+isPrimary);

        login.doContextualLogin(entity, event);
        broadCastMessageSummary.navigateToBroadcast();
        broadCastMessageCreate.loadBroadcastForm();
        broadCastMessageCreate.setbaseInfo(validFromDate, validFromTime, validToDate, validToTime, type, contextMap);
        broadCastMessageCreate.setMessageInfo(language, messageValue, isPrimary);
        broadCastMessageReview.verifyBroadcastMessage(type, language, messageValue);
        broadCastMessageConfirmation.checkBroadCastMessageSubmission(targetStatus);
        broadCastMessageSummary.assertCreatedData(targetStatus);

    }


    }

