package framework;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    public static String getCurrentDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("uuuu/MM/dd");
        LocalDate currentDateTime = LocalDate.now();
        String dateString = currentDateTime.toString();
        System.out.println("Current Date & Time: "+dateString);
        System.out.println("Calling the split functionality to get current date");
        return splitDate(dateString);
    }

    public static String splitDate(String dateString) {
        String [] dateComponent = dateString.split("-");
        String year = dateComponent[0];
        String month = dateComponent[1];
        String date = dateComponent[2];
        String completeDate = date+"/"+month+"/"+year;
        System.out.println("Returning the current date: "+date);
        System.out.println("Returning the complete date: "+completeDate);
        return date;
    }
}
