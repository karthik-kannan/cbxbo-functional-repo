package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ServiceRequestflow {
    private String entity, event, targetStatus, action, referenceId, acceptanceStatus, completionNotes;
    private JSONArray comments;

    public ServiceRequestflow(JSONObject object){
        setAction(object.get("action").toString());
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setReferenceId(object.get("referenceId").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setComments((JSONArray) object.get("comments"));
        setAcceptanceStatus(object.get("acceptanceStatus").toString());
        setCompletionNotes(object.get("completionNotes").toString());
    }

    public String getAcceptanceStatus() {
        return acceptanceStatus;
    }

    public void setAcceptanceStatus(String acceptanceStatus) {
        this.acceptanceStatus = acceptanceStatus;
    }

    public String getCompletionNotes() {
        return completionNotes;
    }

    public void setCompletionNotes(String completionNotes) {
        this.completionNotes = completionNotes;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public JSONArray getComments() {
        return comments;
    }

    public void setComments(JSONArray comments) {
        this.comments = comments;
    }
}
