package pages.servicerequests;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base {
    // local variables
    Base base = new Base();
    Summary summarySRQ = new Summary();

    // constructor
    public Confirmation() throws Exception{
        super();
    }

    // elements
    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/h2")
    protected M confirmationModal;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]")
    protected M confirmationModalText;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div/b[1]")
    protected M generatedReferenceId;

    @FindBy(xpath = "//button[contains(text(),'Go back to service request summary')]")
    protected M goToSRQSummary;

    public String checkServiceRequestSubmission(String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(confirmationModal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(confirmationModalText,TIMEOUT_MINUTE);
            if (!confirmationModalText.getText().isEmpty()){
                System.out.println("Service Request processing is completed.");
                System.out.println("Confirmation Text: "+confirmationModalText.getText());
                String referenceId = generatedReferenceId.getText();
                System.out.println("Calling the assert functionality in Summary");
                goToSRQSummary.click();
                summarySRQ.assertProcessedRequest(referenceId, targetStatus);

            }else System.out.println("Processing Error in SRQ");
        } catch (Exception e){
            System.out.println("Exception in processing SRQ "+e);
        }
        return null;


    }
}
