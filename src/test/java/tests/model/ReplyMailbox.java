package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ReplyMailbox {
    private String entity, event, targetStatus, referenceId, message, attachment, action;

    public ReplyMailbox(JSONObject object){
    setEntity(object.get("entity").toString());
    setEvent(object.get("event").toString());
    setTargetStatus(object.get("targetStatus").toString());
    setReferenceId(object.get("referenceId").toString());
    setMessage(object.get("message").toString());
    setAttachment(object.get("attachment").toString());
    setAction(object.get("action").toString());
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
}
