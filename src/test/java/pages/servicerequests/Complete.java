package pages.servicerequests;

import org.json.simple.JSONArray;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

public class Complete<M extends WebElement> extends Base {
    // local variables
    Base base = new Base();

    // constructor
    public Complete() throws Exception{
        super();
    }

    //elements
    @FindBy(xpath = "//button[@title='Service requests']/span")
    protected M servicerequestIcon;

    @FindBy(xpath = "//clr-input-container/div/div/input")
    protected M hostRefNumber;

    @FindBy(xpath = "//clr-textarea-container/div/div/textarea")
    protected M notes;

    @FindBy(xpath = "//button[contains(text(),'Complete')]")
    protected M completeSRQ;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    protected M cancelSRQ;

    public void CompleteServiceRequest(String entity, String event, String targetStatus, String action, String referenceId, JSONArray comments, String acceptanceStatus) {
    }
}
