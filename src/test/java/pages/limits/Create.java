package pages.limits;

import com.google.sitebricks.client.Web;
import framework.BrowserUtils;
import framework.CreateDriver;
import org.json.simple.JSONArray;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.Base;

import javax.xml.crypto.dsig.spec.ExcC14NParameterSpec;

import java.util.ArrayList;
import java.util.List;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertNotNull;

public class Create<M extends WebElement> extends Base {

    // local variables
    Base base = new Base();

    // constructor
    public Create() throws Exception {
        super();
    }

    // elements
    @FindBy(xpath = "//button[contains(text(),'Create a new limit')]")
    protected M createLimits;

    @FindBy(xpath = "//div[@role='combobox']/input")
    protected M domainKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]/span")
    protected M domainValue;

    @FindBy(xpath = "//domain-control/div/div[1]/span")
    protected M domainId;

    @FindBy(xpath = "//input[@placeholder='Enter profile name']")
    protected M profileName;

    @FindBy(xpath = "//formly-field-select/select")
    protected M roleNameList;

    @FindBy(xpath = "//button[@name='next']")
    protected M domainNext;

    @FindBy(xpath = "//button[@name='reviewAndSubmit']")
    protected M limitNext;

    @FindBy(xpath = "//button/span[contains(text(),'Add limit')]")
    protected M addLimits;

    @FindBy(xpath = "//div/h2")
    protected M subProductModalTitle;

    @FindBy(xpath = "//button[@name='reviewAndSubmit']")
    protected M subProductGridLoadElement;

    @FindBy(xpath = "//input[@formcontrolname='daily_singletxn_initiation_count']")
    protected M maxInitiationNumberPerDaySingle;

    @FindBy(xpath = "//input[@formcontrolname='daily_bulktxn_initiation_count']")
    protected M maxInitiationNumberPerDayBulk;

    @FindBy(xpath = "//input[@formcontrolname='daily_consolidatedtxn_initiation_count']")
    protected M maxInitiationNumberPerDayConsolidated;

    @FindBy(xpath = "//input[@formcontrolname='daily_singletxn_initiation_amount']")
    protected M maxInitiationAmountPerDaySingle;

    @FindBy(xpath = "//input[@formcontrolname='daily_bulktxn_initiation_amount']")
    protected M maxInitiationAmountPerDayBuld;

    @FindBy(xpath = "//input[@formcontrolname='daily_consolidatedtxn_initiation_amount']")
    protected M maxInitiationAmountPerDayConsolidated;

    @FindBy(xpath = "//input[@formcontrolname='daily_singletxn_approval_amount']")
    protected M maxApprovalAmountPerDaySingle;

    @FindBy(xpath = "//input[@formcontrolname='daily_bulktxn_approval_amount']")
    protected M maxApprovalAmountPerDayBulk;

    @FindBy(xpath = "//input[@formcontrolname='daily_consolidatedtxn_approval_amount']")
    protected M maxApprovalAmountPerDayConsolidated;

    @FindBy(xpath = "//input[@formcontrolname='approval_amount_per_singletxn']")
    protected M maxApprovalAmountPerTxnSingle;

    @FindBy(xpath = "//input[@formcontrolname='approval_amount_per_bulktxn']")
    protected M maxApprovalAmountPerTxnBulk;

    @FindBy(xpath = "//div/clr-radio-wrapper[1]/label")
    protected M selfAuthEnableYes;

    @FindBy(xpath = "//div/clr-radio-wrapper[2]/label")
    protected M selfAuthEnableNo;

    @FindBy(xpath = "//input[@placeholder='Enter self approval limit']")
    protected M selfAuthEnableValue;

    @FindBy(xpath = "//div[2]/input[@name='maxInitPerDay']")
    protected M commonSectionMaxInit;

    @FindBy(xpath = "//div[2]/div[3]/div/clr-checkbox-wrapper/label")
    protected M commonSectionMaxInitApplyAll;

    @FindBy(xpath = "//div[2]/input[@name='maxApprPerDay']")
    protected M commonSectionMaxApproval;

    @FindBy(xpath = "//div[3]/div[3]/div/clr-checkbox-wrapper/label")
    protected M commonSectionMaxApprovalApplyAll;

    @FindBy(xpath = "//button[contains(text(),'Save')]")
    protected M subProductWidgetSave;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    protected M subProductWidgetCancel;

    @FindBy(xpath = "//table/tfoot/tr/td[3]")
    protected M numOfSubProductsInGrid;

    @FindBy(xpath = "//button/span[contains(text(),'Cancel')]")
    protected M cancelBtn;

    @FindBy(xpath = "//button/span[contains(text(),'Save as draft')]")
    protected M saveAsDraftBtn;

    @FindBy(xpath = "//button/span[contains(text(),'Review & submit')]")
    protected M submitBtn;


    public void setDomainInfo(String domainNameID, String limitsProfileName, String roleName) throws Exception {
        setDomain(domainNameID);
        setProfileName(limitsProfileName);
        selectRole(roleName);
        saveDomainNav();
    }

    private void saveDomainNav() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", domainNext);
            BrowserUtils.waitForClickable(domainNext,TIMEOUT_MINUTE);
            if(domainNext.isDisplayed()) {
                domainNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception is saving the domain info");
        }



    }

    private void selectRole(String roleName) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(roleNameList, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(roleNameList, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", roleNameList);

            if (roleNameList.isDisplayed()){
                Select select = new Select(roleNameList);
                select.selectByVisibleText(roleName);
            }
            System.out.println("Selected the role name: "+roleName);
        }catch (Exception e){
            System.out.println("Exception in setting role name "+e);

        }

    }

    private void setProfileName(String limitsProfileName) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(limitsProfileName!=null){
                BrowserUtils.waitForClickable(profileName,TIMEOUT_ELEMENT);
                base.clickAndEnter(profileName, limitsProfileName);
            }
        }catch (Exception e){
            System.out.println("Exception in setting profile name");
        }


    }

    private void setDomain(String domainNameID) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(domainNameID!=null) {
                String[] domainIn = getDomainName(domainNameID);
                System.out.println("Values returned by getDomainName method for input data is '" + domainIn[0] + "' and '" + domainIn[1] + "'");
                BrowserUtils.waitForClickable(domainKey, TIMEOUT_MINUTE);
                base.clickAndEnter(domainKey, domainIn[0]);
                BrowserUtils.waitForClickable(domainValue, TIMEOUT_ELEMENT);
                //base.selectFilteredValue(domainValue);
                List<WebElement> filterNameElements = driver.findElements(By.xpath("//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]"));
                System.out.println("Domain Name results for the entered filter are " + filterNameElements.size());
                List<WebElement> filterIdElements = driver.findElements(By.xpath("//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[2]/span[2]"));
                System.out.println("Domain Id associated with Domain Name results for the entered filter are " + filterIdElements.size());

                for (WebElement nameElement : filterNameElements) {
                    for (WebElement idElement : filterIdElements) {
                        if (nameElement.getText().contains(domainIn[0]) && idElement.getText().contains(domainIn[1])) {
                            BrowserUtils.waitForClickable(nameElement, TIMEOUT_MINUTE);
                            System.out.println("Clicking on "+idElement.getText() +" list value");
                            idElement.click();
                            System.out.println("Clicked on selection of the domain and waiting for the domainId display");
                            BrowserUtils.waitForVisibility(domainId, TIMEOUT_MINUTE);
                            assertNotNull(domainId.getText());
                            System.out.println("The Domain ID for the selected Domain is " + domainId.getText());
                        } else {
                            System.out.println();
                        }
                    }
                }

            }
        }catch(Exception e){
            System.out.println("Exception in setting domain "+e);
        }

    }

    private String[] getDomainName(String domainNameID) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            String[] domainData = domainNameID.split("_");
            String[] domainOut = new String[domainData.length];
            System.out.println("Input domain contains "+domainData.length+" variables");
            for (int i =0; i<domainData.length; i++){
                System.out.println("Domain Data @ index "+i +" is "+domainData[i].toString());
                domainOut[i] = domainData[i];
            }
            return domainOut;

        }catch (Exception e){
            System.out.println("Exception in getting the domain input data "+e);
        }
        return null;
    }

    public void setLimitsInfo(String dailySingletxnInitiationCount, String dailySingletxnInitiationAmount, String dailyBulktxnInitiationCount, String dailyBulktxnInitiationAmount, String dailyConsolidatedtxnInitiationCount, String dailyConsolidatedtxnInitiationAmount, String dailySingletxnApprovalAmount, String dailyBulktxnApprovalAmount, String dailyConsolidatedtxnApprovalAmount, String approvalAmountPerSingletxn, String approvalAmountPerBulktxn, String selfApprovalAmount, String selfApprovalRequired, JSONArray subProductLimits, String applicableToSubProductLimits) {
        setRoleLevelLimits(dailySingletxnInitiationCount, dailySingletxnInitiationAmount, dailyBulktxnInitiationCount, dailyBulktxnInitiationAmount, dailyConsolidatedtxnInitiationCount, dailyConsolidatedtxnInitiationAmount, dailySingletxnApprovalAmount, dailyBulktxnApprovalAmount, dailyConsolidatedtxnApprovalAmount, approvalAmountPerSingletxn, approvalAmountPerBulktxn, selfApprovalAmount, selfApprovalRequired);
        setSubProductLevelLimits(subProductLimits, applicableToSubProductLimits);
        saveLimitNav();
    }

    private void saveLimitNav() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", limitNext);
            BrowserUtils.waitForClickable(limitNext,TIMEOUT_MINUTE);
            if(limitNext.isDisplayed()) {
                limitNext.click();
            }
        }catch(Exception e){
            System.out.println("Exception is saving the limit info "+e);
        }
    }

    private void setSubProductLevelLimits(JSONArray subProductLimitsData, String applicableToSubProductLimits) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            if (applicableToSubProductLimits.contains("Yes")){
                System.out.println("Sub Product limits applicable for this limit profiling");
                addLimits.click();
                BrowserUtils.waitForVisibility(subProductModalTitle, TIMEOUT_MINUTE);
                if (subProductModalTitle.isDisplayed() && subProductModalTitle.getText().contains("Add sub-product limits")){
                    System.out.println("Modal loaded for adding Sub-Products");
                    js.executeScript("arguments[0].scrollIntoView();", numOfSubProductsInGrid);
                    BrowserUtils.waitForVisibility(numOfSubProductsInGrid, TIMEOUT_MINUTE);
                    if (numOfSubProductsInGrid.isDisplayed() && numOfSubProductsInGrid.getText().contains("sub-products")){
                        System.out.println("Sub-product grid is loaded. Setting limits at sub-product level");
                        for (int i=0; i<subProductLimitsData.size();i++){
                            System.out.println("Length of raw sub-product list is "+subProductLimitsData.size());
                            System.out.println("Values of raw sub-product list is "+subProductLimitsData.get(i).toString());
                            String[] actualSubProductLimitsData = subProductLimitsData.get(i).toString().split("_");
                            System.out.println("Actual sub-products data variables are "+actualSubProductLimitsData[0]+", "+actualSubProductLimitsData[1]+", "+actualSubProductLimitsData[2]+", "+actualSubProductLimitsData[3]);
                            List<WebElement> subProductElements = driver.findElements(By.xpath("//tr/td[1]/span"));
                            for (WebElement subProductElement : subProductElements){
                                System.out.println("Sub Product listed in grid is "+subProductElement.getText());
                                if (subProductElement.getText().contains(actualSubProductLimitsData[1])){
                                    System.out.println("Test Data for sub-product displayed in grid.");
                                    System.out.println("Text in grid "+subProductElement.getText());
                                    System.out.println("Text from data input "+actualSubProductLimitsData[1]);
                                    int j = i+1;
                                    String maxInitiateAmount = "//table/tbody/tr["+j+"]/td[2]/div/div/input[@formcontrolname='daily_consolidatedtxn_initiation_amount']";
                                    String maxApproveAmount = "//table/tbody/tr["+j+"]/td[3]/div/div/input[@formcontrolname='daily_consolidatedtxn_approval_amount']";

                                    driver.findElement(By.xpath(maxInitiateAmount)).sendKeys(actualSubProductLimitsData[2]);
                                    driver.findElement(By.xpath(maxApproveAmount)).sendKeys(actualSubProductLimitsData[3]);
                                    System.out.println("Entered the initiation and approval amount for sub product");
                                }
                            }

                        }
                        BrowserUtils.waitForClickable(subProductWidgetSave, TIMEOUT_MINUTE);
                        subProductWidgetSave.click();
                        js.executeScript("arguments[0].scrollIntoView();", limitNext);
                    }
                }
            }else if (applicableToSubProductLimits.contains("No")){
                System.out.println("Sub Product limits not applicable for this limit profiling");
            }
        }catch (Exception e){
            System.out.println("Exception in setting subproduct level limits "+e);
        }
    }

    private void setRoleLevelLimits(String dailySingletxnInitiationCount, String dailySingletxnInitiationAmount, String dailyBulktxnInitiationCount, String dailyBulktxnInitiationAmount, String dailyConsolidatedtxnInitiationCount, String dailyConsolidatedtxnInitiationAmount, String dailySingletxnApprovalAmount, String dailyBulktxnApprovalAmount, String dailyConsolidatedtxnApprovalAmount, String approvalAmountPerSingletxn, String approvalAmountPerBulktxn, String selfApprovalAmount, String selfApprovalRequired) {
        setdailySingletxnInitiationCount(dailySingletxnInitiationCount);
        setdailyBulktxnInitiationCount(dailyBulktxnInitiationCount);
        setdailyConsolidatedtxnInitiationCount(dailyConsolidatedtxnInitiationCount);
        setdailySingletxnInitiationAmount(dailySingletxnInitiationAmount);
        setdailyBulktxnInitiationAmount(dailyBulktxnInitiationAmount);
        setdailyConsolidatedtxnInitiationAmount(dailyConsolidatedtxnInitiationAmount);
        setdailySingletxnApprovalAmount(dailySingletxnApprovalAmount);
        setdailyBulktxnApprovalAmount(dailyBulktxnApprovalAmount);
        setdailyConsolidatedtxnApprovalAmount(dailyConsolidatedtxnApprovalAmount);
        setapprovalAmountPerSingletxn(approvalAmountPerSingletxn);
        setapprovalAmountPerBulktxn(approvalAmountPerBulktxn);
        setselfApprovalAmount(selfApprovalAmount, selfApprovalRequired);
    }

    private void setselfApprovalAmount(String selfApprovalAmount, String selfApprovalRequired) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (!selfApprovalAmount.isEmpty()){
                if (selfApprovalRequired.contains("Yes")){
                    selectSelfAuthYes(selfApprovalAmount);
                }else {
                    System.out.println("Self auth '"+"No"+"' condition executed");
                }

            }else {
                System.out.println("Self auth input values is not set");
            }
        }catch (Exception e){
            System.out.println("Exception in setting self approval "+e);
        }
    }

    private void selectSelfAuthYes(String selfApprovalAmount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (selfAuthEnableYes.isDisplayed()){
                selfAuthEnableYes.click();
                if (selfAuthEnableValue.isDisplayed()){
                    System.out.println("Clearing the default CIM self auth value");
                    selfAuthEnableValue.clear();
                    base.clickAndEnter(selfAuthEnableValue, selfApprovalAmount);
                    System.out.println("Selected the self approval as Yes and entered the amount");
                }
            }
        }catch (Exception e){
            System.out.println("Exception in setting self approval value with Yes condition "+e);
        }
    }

    private void setapprovalAmountPerBulktxn(String approvalAmountPerBulktxn) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(approvalAmountPerBulktxn!=null){
                BrowserUtils.waitForVisibility(maxApprovalAmountPerTxnBulk,TIMEOUT_ELEMENT);
                if (maxApprovalAmountPerTxnBulk.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxApprovalAmountPerTxnBulk.clear();
                }
                base.clickAndEnter(maxApprovalAmountPerTxnBulk, approvalAmountPerBulktxn);
                System.out.println("Entered the maxApprovalAmountPerTxnBulk");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setapprovalAmountPerBulktxn "+e);
        }
    }

    private void setapprovalAmountPerSingletxn(String approvalAmountPerSingletxn) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(approvalAmountPerSingletxn!=null){
                BrowserUtils.waitForVisibility(maxApprovalAmountPerTxnSingle,TIMEOUT_ELEMENT);
                if (maxApprovalAmountPerTxnSingle.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxApprovalAmountPerTxnSingle.clear();
                }
                base.clickAndEnter(maxApprovalAmountPerTxnSingle, approvalAmountPerSingletxn);
                System.out.println("Entered the maxApprovalAmountPerTxnSingle");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setapprovalAmountPerSingletxn "+e);
        }
    }

    private void setdailyConsolidatedtxnApprovalAmount(String dailyConsolidatedtxnApprovalAmount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailyConsolidatedtxnApprovalAmount!=null){
                BrowserUtils.waitForVisibility(maxApprovalAmountPerDayConsolidated,TIMEOUT_ELEMENT);
                if (maxApprovalAmountPerDayConsolidated.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxApprovalAmountPerDayConsolidated.clear();
                }
                base.clickAndEnter(maxApprovalAmountPerDayConsolidated, dailyConsolidatedtxnApprovalAmount);
                System.out.println("Entered the maxApprovalAmountPerDayConsolidated");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailyConsolidatedtxnApprovalAmount "+e);
        }
    }

    private void setdailyBulktxnApprovalAmount(String dailyBulktxnApprovalAmount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailyBulktxnApprovalAmount!=null){
                BrowserUtils.waitForVisibility(maxApprovalAmountPerDayBulk,TIMEOUT_ELEMENT);
                if (maxApprovalAmountPerDayBulk.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxApprovalAmountPerDayBulk.clear();
                }
                base.clickAndEnter(maxApprovalAmountPerDayBulk, dailyBulktxnApprovalAmount);
                System.out.println("Entered the maxApprovalAmountPerDayBulk");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailyBulktxnApprovalAmount "+e);
        }
    }

    private void setdailySingletxnApprovalAmount(String dailySingletxnApprovalAmount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailySingletxnApprovalAmount!=null){
                BrowserUtils.waitForVisibility(maxApprovalAmountPerDaySingle,TIMEOUT_ELEMENT);
                if (maxApprovalAmountPerDaySingle.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxApprovalAmountPerDaySingle.clear();
                }
                base.clickAndEnter(maxApprovalAmountPerDaySingle, dailySingletxnApprovalAmount);
                System.out.println("Entered the maxApprovalAmountPerDaySingle");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailySingletxnApprovalAmount "+e);
        }
    }

    private void setdailyConsolidatedtxnInitiationAmount(String dailyConsolidatedtxnInitiationAmount) {WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailyConsolidatedtxnInitiationAmount!=null){
                BrowserUtils.waitForVisibility(maxInitiationAmountPerDayConsolidated,TIMEOUT_ELEMENT);
                if (maxInitiationAmountPerDayConsolidated.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxInitiationAmountPerDayConsolidated.clear();
                }
                base.clickAndEnter(maxInitiationAmountPerDayConsolidated, dailyConsolidatedtxnInitiationAmount);
                System.out.println("Entered the maxInitiationAmountPerDayConsolidated");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailyConsolidatedtxnInitiationAmount "+e);
        }
    }

    private void setdailyConsolidatedtxnInitiationCount(String dailyConsolidatedtxnInitiationCount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailyConsolidatedtxnInitiationCount!=null){
                BrowserUtils.waitForVisibility(maxInitiationNumberPerDayConsolidated,TIMEOUT_ELEMENT);
                if (maxInitiationNumberPerDayConsolidated.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxInitiationNumberPerDayConsolidated.clear();
                }
                base.clickAndEnter(maxInitiationNumberPerDayConsolidated, dailyConsolidatedtxnInitiationCount);
                System.out.println("Entered the dailyConsolidatedtxnInitiationCount");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailyConsolidatedtxnInitiationCount "+e);
        }
    }

    private void setdailyBulktxnInitiationAmount(String dailyBulktxnInitiationAmount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailyBulktxnInitiationAmount!=null){
                BrowserUtils.waitForVisibility(maxInitiationAmountPerDayBuld,TIMEOUT_ELEMENT);
                if (maxInitiationAmountPerDayBuld.isDisplayed()){
                    System.out.println("Defaulted value from CIM for maxInitiationAmountPerDayBuld is "+maxInitiationAmountPerDayBuld.getText());
                    System.out.println("Clearing the default value and entering user input");
                    maxInitiationAmountPerDayBuld.clear();
                }
                base.clickAndEnter(maxInitiationAmountPerDayBuld, dailyBulktxnInitiationAmount);
                System.out.println("Entered the maxInitiationAmountPerDayBuld");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailyBulktxnInitiationAmount "+e);
        }

    }

    private void setdailyBulktxnInitiationCount(String dailyBulktxnInitiationCount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailyBulktxnInitiationCount!=null){
                BrowserUtils.waitForVisibility(maxInitiationNumberPerDayBulk,TIMEOUT_ELEMENT);
                if (maxInitiationNumberPerDayBulk.isDisplayed()){
                    System.out.println("Defaulted value from CIM for maxInitiationNumberPerDayBulk is "+maxInitiationNumberPerDayBulk.getText());
                    System.out.println("Clearing the default value and entering user input");
                    maxInitiationNumberPerDayBulk.clear();
                }
                base.clickAndEnter(maxInitiationNumberPerDayBulk, dailyBulktxnInitiationCount);
                System.out.println("Entered the dailyBulktxnInitiationCount");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailySingletxnInitiationCount "+e);
        }
    }

    private void setdailySingletxnInitiationAmount(String dailySingletxnInitiationAmount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailySingletxnInitiationAmount!=null){
                BrowserUtils.waitForVisibility(maxInitiationAmountPerDaySingle,TIMEOUT_ELEMENT);
                if (maxInitiationAmountPerDaySingle.isDisplayed()){
                    System.out.println("Clearing the default value and entering user input");
                    maxInitiationAmountPerDaySingle.clear();
                }
                base.clickAndEnter(maxInitiationAmountPerDaySingle, dailySingletxnInitiationAmount);
                System.out.println("Entered the maxInitiationAmountPerDaySingle");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailySingletxnInitiationAmount "+e);
        }
    }

    private void setdailySingletxnInitiationCount(String dailySingletxnInitiationCount) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(dailySingletxnInitiationCount!=null){
                BrowserUtils.waitForVisibility(maxInitiationNumberPerDaySingle,TIMEOUT_ELEMENT);
                if (maxInitiationNumberPerDaySingle.isDisplayed()){
                   System.out.println("Clearing the default value and entering user input");
                    maxInitiationNumberPerDaySingle.clear();
                }
                base.clickAndEnter(maxInitiationNumberPerDaySingle, dailySingletxnInitiationCount);
                System.out.println("Entered the dailySingletxnInitiationCount");
            }
        }catch (Exception e){
            System.out.println("Exception in setting setdailySingletxnInitiationCount "+e);
        }
    }
}
