package pages.limits;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import java.util.List;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Review<M extends WebElement> extends Base {

    //local variables

    //constructor
    public Review() throws Exception{
        super();
    }

    // elements
    @FindBy(xpath = "//div/h1[contains(text(),'Review & submit')]")
    protected M pageTitle;

    @FindBy(xpath = "//limits-profile-review/div/div/div/div[2]/div[2]/div[1]")
    protected M domainName;

    @FindBy(xpath = "//limits-profile-review/div/div/div/div[2]/div[2]/div[2]")
    protected M profileName;

    @FindBy(xpath = "//entity-limits-view/div/div/div/div[2]/div/table/tbody/tr/td[1]/clr-icon")
    protected M expandLimitGrid;

    @FindBy(xpath = "//td/role-limits-view/div/div/div/div[1]/div[1]/div/h2")
    protected M gridHeader;

    @FindBy(xpath = "//div/button[contains(text(),'Cancel')]")
    protected M reviewCancel;

    @FindBy(xpath = "//div/button[contains(text(),'Save as draft')]")
    protected M reviewSaveAsDraft;

    @FindBy(xpath = "//div/button[contains(text(),'submit')]")
    protected M reviewSubmit;

    public void verifyLimits(String limitsProfileName, String targetStatus) throws Exception {

        verifyDefaults(limitsProfileName);
        verifyEnteredRoleLimits();
        verifyEnteredSubProductLimits();
        submitNewLimit(targetStatus);

    }

    private void submitNewLimit(String targetStatus) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", reviewSubmit);
            BrowserUtils.waitForClickable(reviewSubmit,TIMEOUT_MINUTE);
            if (targetStatus.contains("Pending approval")){
                if(reviewSubmit.isDisplayed()) {
                    reviewSubmit.click();
                    System.out.println("Clicked on the submit action for limit profile");
                }
            }else if (targetStatus.contains("Draft")){
                if(reviewSaveAsDraft.isDisplayed()) {
                    reviewSaveAsDraft.click();
                    System.out.println("Clicked on the draft action for limit profile");
                }
            }

        }catch(Exception e){
            System.out.println("Exception is submitting/ drafting limits "+e);
        }

    }

    private void verifyEnteredSubProductLimits() {
    }

    private void verifyEnteredRoleLimits() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(expandLimitGrid,TIMEOUT_MINUTE);
        try{
            if (expandLimitGrid.isDisplayed()){
                expandLimitGrid.click();
                BrowserUtils.waitForVisibility(gridHeader, TIMEOUT_MINUTE);
                getLimits();

            }
        }catch (Exception e){
            System.out.println("Exception in verifyEnteredRoleLimits "+e);
        }

    }

    private String[] getLimits() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(gridHeader, TIMEOUT_MINUTE);

        String[] limitDisplayValues = new String[0];
        try {
            List<WebElement> limitElementsInReview = driver.findElements(By.xpath("//tr/td/span"));
            limitDisplayValues = new String[limitElementsInReview.size()];
            for (WebElement limitElementValue : limitElementsInReview) {
                if (limitElementValue.getText().isEmpty()) {
                    System.out.println("Limits Value not displayed in Review");
                } else {
                   for (int i = 0; i < limitElementsInReview.size(); i++) {
                        limitDisplayValues[i] = limitElementValue.getText();
                    }
                   for (int i=0; i<limitDisplayValues.length; i++){
                       System.out.println(limitDisplayValues[i]);
                   }
                  //  return limitDisplayValues;
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in reading thee limits from review page " + e);
        } finally {
            System.out.println("Limit values from Review page are " + limitDisplayValues);
        }

        return new String[0];
    }

    private void verifyDefaults(String limitsProfileName) throws Exception {
        verifyPageTitle();
        verifyLimitDomainInputDetails(limitsProfileName);
    }

    private void verifyLimitDomainInputDetails(String limitsProfileName) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(domainName,TIMEOUT_MINUTE);
        try{
            if (!domainName.getText().isEmpty()){
                if (profileName.getText().contains(limitsProfileName)){
                    System.out.println("Created limits profile '"+profileName.getText()+"' for "+domainName.getText()+ " is displayed in Review page");
                }
            }
        }catch (Exception e){
            System.out.println("Error in Limit input details "+e);
        }
    }

    private boolean verifyPageTitle() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(pageTitle,TIMEOUT_MINUTE);
        try{
            boolean title = pageTitle.getText().contains("Review & submit");
            return title;

        }catch(Exception e){
            System.out.println("Error in page title "+e);
        }
        return false;

    }
}
