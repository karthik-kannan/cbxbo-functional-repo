package pages.servicerequests;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.DateUtils;
import framework.Global_VARS;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Summary<M extends WebElement> extends Base{

    // local variables
    Base base = new Base();

    // constructor
    public Summary() throws Exception{
        super();
    }

    // elements
    @FindBy(xpath = "//button[@title='Service requests']/span")
    protected M servicerequestIcon;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//button[@title='Accept']")
    protected M acceptButton;

    @FindBy(xpath = "//div[1]/div/h2")
    protected M acceptModal;

    @FindBy(xpath = "div[1]/div/h2")
    protected M rejectModal;

    @FindBy(xpath = "//button[contains(text(),'Accept')]")
    protected M acceptButtonInModal;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    protected M cancelButtonInModal;

    @FindBy(xpath = "//clr-date-container[1]/div/div/div/button/clr-icon")
    protected M acceptDateCalIcon;

    @FindBy(xpath = "//clr-daypicker/div[2]/div[1]/button[1]")
    protected M dateManager;

    @FindBy(xpath = "//button[@title='Reject']")
    protected M rejectButton;

    @FindBy(xpath = "//textarea[@name='reason']")
    protected M rejectReason;

    @FindBy(xpath = "//button[contains(text(),'Reject')]")
    protected M rejectSubmit;

    @FindBy(xpath = "//clr-dg-column[1]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M referenceColFilter;

    @FindBy(xpath = "//input[@name='search']")
    protected M search;

    @FindBy(xpath = "//div/div/button/clr-icon")
    protected M close;

    @FindBy(xpath = "//clr-dg-row/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M referenceElementGrid;

    @FindBy(xpath = "//button[@title='Go to service request detail']")
    protected M goToDetails;

    @FindBy(xpath = "//clr-dg-column[2]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M domainColFilter;

    @FindBy(xpath = "//clr-dg-cell[6]/div/div[1]/div[1]/button/span/span[contains(text(),'Complete')]")
    protected M completeSRQSummary;

    @FindBy(xpath = "//clr-dg-cell[6]/div/div[1]/div[2]/button/span/span[contains(text(),'Reject')]")
    protected M rejectSRQSummary;

    @FindBy(xpath = "//button/span[contains(text(),'Complete')]")
    protected M completeSRQDetails;

    @FindBy(xpath = "//button/span[contains(text(),'Reject')]")
    protected M rejectSRQDetails;

    @FindBy(xpath = "//button[contains(text(),'Complete')]")
    protected M completeFromModal;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    protected M cancelFromModal;

    @FindBy(xpath = "//div/div[2]/div/span")
    protected M statusElementGrid;

    String rejectionData = "Rejecting the record";

    @FindBy(xpath = "//div[1]/div/h2")
    protected M completeModal;

    @FindBy(xpath = "//clr-input-container/div/div/input")
    protected M hostRefNumber;

    @FindBy(xpath = "//clr-textarea-container/div/div/textarea")
    protected M notes;

    @FindBy(xpath = "//button[contains(text(),'Complete')]")
    protected M completeSRQ;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    protected M cancelSRQ;

    @FindBy(xpath = "//div[1]/nav/button/span[2]")
    protected M backToUserSummary;


    public void navigateToServiceRequest() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);

        try {
            BrowserUtils.waitForClickable(servicerequestIcon, Global_VARS.TIMEOUT_MINUTE);
            if (!servicerequestIcon.isSelected()) {
                base.clickTheMenu(servicerequestIcon);
                System.out.println("Clicked the Service Request menu");
            }
        } catch (Exception e) {
            System.out.println("Exception in navigating to Service Request  tab " + e);
        }



    }

    public void processServiceRequest(String entity, String event, String targetStatus, String action, String referenceId, JSONArray comments, String acceptanceStatus, String completionNotes) throws Exception {
        filterRecord(referenceId);
        acceptServiceRequest(action);
        checkAcceptanceRecord(referenceId, acceptanceStatus);
        doProcess(action, comments, referenceId, completionNotes);
    }

    private void doProcess(String action, JSONArray comments, String referenceId, String completionNotes) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] actionIn = action.split("_");
        BrowserUtils.waitForGridDataLoad(referenceElementGrid,referenceId,TIMEOUT_MINUTE);
        referenceElementGrid.click();

        System.out.println("Size of SRQ processing data is "+actionIn.length +". Values are: "+actionIn[0]+", "+actionIn[1]+", "+actionIn[2]+", "+actionIn[3]);
        System.out.println("Completion event for SRQ is identified as '"+actionIn[2]+", "+actionIn[3]);

        try{
            if (actionIn[2].contains("complete") && actionIn[3].contains("summary")){
                System.out.println(actionIn[2]+", "+actionIn[3]);
                completeServiceRequest(completeSRQSummary, referenceId, completionNotes, comments);
            } else if (actionIn[2].contains("complete") && actionIn[3].contains("details")){
                System.out.println(actionIn[2]+", "+actionIn[3]);
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                goToDetails.click();
                completeServiceRequest(completeSRQDetails, referenceId, completionNotes, comments);
            }else if (actionIn[2].contains("reject") && actionIn[3].contains("summary")){
                System.out.println(actionIn[2]+", "+actionIn[3]);
                reject(rejectSRQSummary);
            }else if (actionIn[2].contains("reject") && actionIn[3].contains("details")){
                System.out.println(actionIn[2]+", "+actionIn[3]);
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                goToDetails.click();
                reject(rejectSRQDetails);
            }

        }catch (Exception e){
            System.out.println("Exception in completing the service request processing the filtered record "+e);

        }
    }

    private void completeServiceRequest(WebElement element, String referenceId, String completionNotes, JSONArray comments) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try{
            System.out.println("The input comments are: "+comments);
            BrowserUtils.waitForClickable(element,TIMEOUT_MINUTE);
            System.out.println("Completing the SRQ with '"+referenceId+"'");
            ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",element);
            element.click();
            if (!completeModal.getText().isEmpty()){
                hostRefNumber.sendKeys(referenceId);
                notes.sendKeys(completionNotes);
                BrowserUtils.waitForClickable(completeFromModal, TIMEOUT_MINUTE);
                completeFromModal.click();
            }else {
                System.out.println("Modal dialog not displayed on completion process");
            }
        }catch (Exception e){
            System.out.println("Exception in completion of SRQ "+e);
        }
    }

    private void checkAcceptanceRecord(String referenceId, String acceptanceStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(rowCellData1,TIMEOUT_MINUTE);

        try{
            filterRecord(referenceId);
            if (!statusElementGrid.getText().isEmpty()){
                if (statusElementGrid.getText().contains(acceptanceStatus)){
                    System.out.println("Status of acceptance is same "+statusElementGrid.getText());
                }else {
                    System.out.println("Mismatch in acceptance status");
                }
            }
        }catch (Exception e){
            System.out.println("Excepting on checking the acceptance status of record "+e);
        }

    }

    private void acceptServiceRequest(String action) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] actionIn = action.split("_");
        System.out.println("Acceptance event for SRQ is identified as '"+actionIn[0]+", "+actionIn[1]);

        if (actionIn[0].contains("accept") && actionIn[1].contains("summary")){
            System.out.println("Processing of service request for '"+actionIn[0]+"' from "+actionIn[1]);
            acceptSRQ();
        }else if (actionIn[0].contains("accept") && actionIn[1].contains("details")){
            System.out.println("Processing of service request for '"+actionIn[0]+"' from "+actionIn[1]);
            navigateToSRQDetails(acceptButton);
            acceptSRQ();
        }else if (actionIn[0].contains("reject") && actionIn[1].contains("summary")){
            System.out.println("Processing of service request for '"+actionIn[0]+"' from "+actionIn[1]);
            reject(rejectButton);
        }else if (actionIn[0].contains("reject") && actionIn[1].contains("details")){
            System.out.println("Processing of service request for '"+actionIn[0]+"' from "+actionIn[1]);
            navigateToSRQDetails(rejectButton);
            reject(rejectButton);
        }
    }

    private void navigateToSRQDetails(M element) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(element,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(element, TIMEOUT_MINUTE);
            BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
            goToDetails.click();
            System.out.println("Clicked on go to service request details");

        }catch (Exception e){
            System.out.println("Exception in navigating to Service Request Details "+e);
        }
    }

    private void acceptSRQ() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(acceptButton,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(acceptButton, TIMEOUT_MINUTE);
            acceptButton.click();
            BrowserUtils.waitForVisibility(acceptModal, TIMEOUT_MINUTE);
            setAcceptanceDate();
            BrowserUtils.waitForClickable(acceptButtonInModal, TIMEOUT_MINUTE);
            acceptButtonInModal.click();
        }catch (Exception e){
            System.out.println("Exception in accepting the SRQ "+e);
        }
    }

    private void reject(WebElement element) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(element,TIMEOUT_MINUTE);
        try{
            System.out.println("Rejecting the SRQ");
            ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",element);
            element.click();
            rejectReason.sendKeys(rejectionData);
            rejectSubmit.click();
            return;
        }catch (Exception e){
            System.out.println("Exception while rejecting the record "+e);
        }
    }

    private void setAcceptanceDate() {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
                BrowserUtils.waitForClickable(acceptDateCalIcon,TIMEOUT_ELEMENT);
                acceptDateCalIcon.click();
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                String dateAccept = DateUtils.getCurrentDate();
                String lookupAcceptDateValue = "//clr-day/button[contains(text(),'"+dateAccept+"')]";
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(driver.findElement(By.xpath(lookupAcceptDateValue)),TIMEOUT_MINUTE);
                driver.findElement(By.xpath(lookupAcceptDateValue)).click();
        } catch (Exception e) {
            System.out.println("Exception in setting acceptance date "+e);
        }
    }

    private void filterRecord(String referenceId) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
        try {
            BrowserUtils.waitForClickable(referenceColFilter, TIMEOUT_MINUTE);
            referenceColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(referenceId);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(referenceElementGrid, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(referenceElementGrid, referenceId, TIMEOUT_MINUTE);
            if (referenceElementGrid.getText().contains(referenceId)) {
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                System.out.println("Filtered the record for message identifier '" + referenceId+"'");
            } else {
                System.out.println("Not filtered");
                System.out.println(referenceElementGrid.getText() + ", " + referenceId);
            }
        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }
    }

    public void assertProcessedRequest(String referenceId, String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitFor("Service request summary - CBX",TIMEOUT_MINUTE);
            filterRecord(referenceId);
            validateTargetStatus(targetStatus);
            return;
        }catch(Exception e){
            System.out.println("Exception in asserting the processing the SRQ "+e);
        }
    }

    private void validateTargetStatus(String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(referenceElementGrid, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(statusElementGrid,TIMEOUT_MINUTE);
            String identifier = referenceElementGrid.getText();
            String SRQActualStatus = statusElementGrid.getText();
            System.out.println("Actual SRQ status for "+identifier +" is '"+SRQActualStatus+"'");
            Assert.assertEquals(SRQActualStatus, targetStatus);
            navigateToUserSummary();
        }catch(Exception e){
            System.out.println("Exception in loading the role creation form");
        }
    }

    private void navigateToUserSummary() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(backToUserSummary,TIMEOUT_MINUTE);
            if(backToUserSummary.isDisplayed()) {
                backToUserSummary.click();
                BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
                System.out.println("Navigated to User summary");
            }
        }catch(Exception e){
            System.out.println("Exception is navigation to summary "+e);
        }
    }
}
