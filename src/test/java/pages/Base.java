package pages;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.util.List;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Base {

    public Base() throws Exception {
        PageFactory.initElements(CreateDriver.getInstance().getDriver(),this);
    }

    public static void cleanUpData() {
        File file = new File("test-output\\Logs\\");
        if (file.renameTo(new File("D:\\Downloads\\Archive\\CBX Back Office Regression_Test_Suite-05.30.21.20.20.09.log"))) {
//            file.delete();
            System.out.println("File moved successfully");
        } else {
            System.out.println("Failed to move the file");
        }

    }

    // common methods

    /**
     * clickAndEnter method to set focus on field and enter the keyword
     *
     * @param
     * @throws AssertionError
     */
    public void clickAndEnter(WebElement element, String data) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            element.click();
            //driver.findElement((By) element).click();
            //driver.findElement((By) element).sendKeys(data);
            element.sendKeys(data);
        }catch (Exception e){
            BrowserUtils.setFocusOnElement(element);

        }
    }

    /**
     * selectValue method to select the static drop down values
     *
     */
    public void selectValue(WebElement element, String data) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.setFocusOnElement(element);
            ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",element);
            Select roleType = new Select(element);
            roleType.selectByVisibleText(data);

        }catch (Exception e){
            System.out.println("Exception in selecting value "+e);
       }
    }

    /**
     * selectFilteredValue method to select the filtered value based on entered key
     *
     * @param element
     */
    public void selectFilteredValue(WebElement element) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            element.click();
        }catch (Exception e){

        }

    }

    /**
     * clickTheMenu method to select the filtered value based on entered key
     *
     * @param element
     */
    public void clickTheMenu(WebElement element) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            element.click();
        }catch (Exception e){
            System.out.println("Exception in element click from Base module "+e);
        }
    }

    public void selectOptions(WebElement element) throws Exception {
       WebDriver driver = CreateDriver.getInstance().getDriver();

        try{
            if (!element.isSelected()) {
                element.click();
            }
        }catch (Exception e){
            System.out.println("Exception in selecting the element");
        }

    }

    public void setFilter(WebElement element1, String string, WebElement element2, String data, WebElement element3) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForGridData(element1, TIMEOUT_MINUTE);
            driver.findElement(By.xpath(string)).click();
            BrowserUtils.waitForVisibility(element2, TIMEOUT_MINUTE);
            if (element2.isDisplayed()){
                element2.sendKeys(data);
                BrowserUtils.waitForGridData(element1, TIMEOUT_MINUTE);
            }
            element3.click();
            List<WebElement> filteredElements = driver.findElements(By.xpath("//clr-dg-row"));

            String[] elements = new String[filteredElements.size()];
        }catch(Exception e){
            System.out.println("Exception in setting the filter");
        }
    }
}
