package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SearchRole {
    private String menu, column, type, value;

    public SearchRole(JSONObject object) {
        setMenu(object.get("menu").toString());
        setColumn(object.get("column").toString());
        setType(object.get("type").toString());
        setValue(object.get("value").toString());
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
