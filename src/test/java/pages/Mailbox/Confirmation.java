package pages.Mailbox;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.json.simple.JSONArray;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base {

    Base base = new Base();
    pages.Mailbox.Summary mailboxSummary = new Summary();

    //local variables

    //constructor
    public Confirmation() throws Exception{
        super();
    }
    //elements
    @FindBy(xpath = "//div/h3")
    protected M modal;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div[1]")
    protected M confirmationMessage;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div/strong[1]")
    protected M generatedMailId;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a")
    protected M mailboxSummaryNav;


    public String checkMessageSubmission(String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(confirmationMessage,TIMEOUT_MINUTE);
            if (!confirmationMessage.getText().isEmpty()){
                System.out.println("Message creation to users is completed.");
                System.out.println("Confirmation Text: "+confirmationMessage.getText());
                String messageId = generatedMailId.getText();
                System.out.println("Sent a message with identifier "+messageId);
                System.out.println("Calling the assert functionality in Summary");
                mailboxSummaryNav.click();
                mailboxSummary.assertSentNewMessage(messageId, targetStatus);

            }else System.out.println("Create Error in Message");
        } catch (Exception e){
            System.out.println("Exception in creating the role "+e);
        }
        return null;

    }

    public void checkReplyMessageSubmission(String action, String targetStatus) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);
        String[] replyIn = action.split("_");

        try{
            BrowserUtils.waitForVisibility(confirmationMessage,TIMEOUT_MINUTE);
            if (!confirmationMessage.getText().isEmpty()){
                System.out.println("Message reply to users is completed.");
                System.out.println("Confirmation Text: "+confirmationMessage.getText());
                String messageId = generatedMailId.getText();
                System.out.println("Replied to the message with identifier "+messageId);
                System.out.println("Calling the assert functionality in Summary");
                mailboxSummaryNav.click();
                if (replyIn[1].contains("inbox")){
                    mailboxSummary.assertRepliedInboxMessage(messageId, targetStatus);
                }else if (replyIn[1].contains("sent")){
                    mailboxSummary.assertRepliedSentMessage(messageId, targetStatus);
                }else if (replyIn[1].contains("archive")){
                    mailboxSummary.assertRepliedArchiveMessage(messageId, targetStatus);
                }

            }else System.out.println("Replied Error in Message");
        } catch (Exception e){
            System.out.println("Exception in creating the role "+e);
        }
    }
}
