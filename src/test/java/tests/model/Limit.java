package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Limit {
    private String entity, event, targetStatus, domainNameID, limitsProfileName, limitsLevel, roleName;
    private JSONObject operationalLimits;
    private JSONObject subproductLimits;

    public Limit(JSONObject object) {
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setDomainNameID(object.get("domainNameID").toString());
        setLimitsProfileName(object.get("limitsProfileName").toString());
        setLimitsLevel(object.get("limitsLevel").toString());
        setRoleName(object.get("roleName").toString());
        setSubproductLimits((JSONObject) object.get("subproductLimits"));
        setOperationalLimits((JSONObject) object.get("operationalLimits"));
   }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getDomainNameID() {
        return domainNameID;
    }

    public void setDomainNameID(String domainNameID) {
        this.domainNameID = domainNameID;
    }

    public String getLimitsProfileName() {
        return limitsProfileName;
    }

    public void setLimitsProfileName(String limitsProfileName) {
        this.limitsProfileName = limitsProfileName;
    }

    public String getLimitsLevel() {
        return limitsLevel;
    }

    public void setLimitsLevel(String limitsLevel) {
        this.limitsLevel = limitsLevel;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public JSONObject getOperationalLimits() {
        return operationalLimits;
    }

    public void setOperationalLimits(JSONObject operationalLimits) {
        this.operationalLimits = operationalLimits;
    }

    public JSONObject getSubproductLimits() {
        return subproductLimits;
    }

    public void setSubproductLimits(JSONObject subproductLimits) {
        this.subproductLimits = subproductLimits;
    }
}
