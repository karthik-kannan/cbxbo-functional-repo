package pages.Mailbox;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Reply<M extends WebElement> extends Base {

    //local variables
    Base base = new Base();

    //constructor
    public Reply() throws Exception{
        super();
    }

    // elements
    @FindBy(xpath = "//div[1]/div[1]/label[contains(text(),'INBOX')]")
    protected M inboxTab;

    @FindBy(xpath = "//div[1]/div/label[contains(text(),'SENT')]")
    protected M sentTab;

    @FindBy(xpath = "//div[1]/div/label[contains(text(),'ARCHIVE')]")
    protected M archiveTab;

    @FindBy(xpath = "//button/span/span[contains(text(),'Reply')]")
    protected M replyButton;

    @FindBy(xpath = "//button/span[contains(text(),'Reply')]")
    protected M replyButtonDetails;


    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div")
    protected M inboxrowCellData1;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div")
    protected M sentrowCellData1;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div")
    protected M archiverowCellData1;

    @FindBy(xpath = "//clr-dg-column[2]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M refColFilter;

    @FindBy(xpath = "//div[2]/div/div/div[1]/div/div/div[1]/span[2]")
    protected M refIdInReplyDetails;

    @FindBy(xpath = "//div/input[@name='search']")
    protected M search;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//clr-dg-cell[2]/div")
    protected M refIdElement;

    @FindBy(xpath = "//clr-dg-cell[7]/div/div[2]/button/span")
    protected M goToDetails;

    @FindBy(xpath = "//div[2]/div/div/div[2]/div[1]/div[1]/div[3]")
    protected M domainEleInReply;

    @FindBy(xpath = "//div[2]/div/div/div[2]/div[2]/div[1]/span[3]")
    protected M topicsEleInReply;

    @FindBy(xpath = "//div[2]/div/div/div[2]/div[1]/div[2]/div[3]/div")
    protected M corporateUserEleInReply;

    @FindBy(xpath = "//div[2]/div/div/div[2]/div[2]/div[2]/span[3]")
    protected M subTopicEleInReply;

    @FindBy(xpath = "//div[2]/div/div/div[3]/div/div/div[1]/div/div/div[1]")
    protected M bankEleInReply;

    @FindBy(xpath = "//div[2]/div/div/div[3]/div/div/div[2]/div/textarea")
    protected M replyTextMessage;

    @FindBy(xpath = "//div[2]/div/div/div[3]/div/div/div[4]/label")
    protected M attachmentsEleInReply;

    @FindBy(xpath = "//button[contains(text(),'Send')]")
    protected M sendReplyButton;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    protected M cancelReplyButton;

    public void processReplyMessage(String entity, String event, String targetStatus, String referenceId, String message, String attachment, String action) throws Exception {
        navigateToTab(action, referenceId);
        performReply(action, referenceId, message);
    }

    private void navigateToTab(String action, String referenceId) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] replyIn = action.split("_");
        System.out.println("Navigation tab is identified as '"+replyIn[1]);

        if (replyIn[1].contains("inbox")){
            System.out.println("Default summary is "+replyIn[1]+". Calling the filter based on reference id");
            filterMessage(referenceId);
        }else if (replyIn[1].contains("sent")){
            System.out.println("Default summary is "+replyIn[1]+". To navigate to Sent summary");
            BrowserUtils.waitForVisibility(sentTab, TIMEOUT_MINUTE);
            sentTab.click();
            System.out.println("Clicked on Sent tab");
            BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
            System.out.println("Sent tab is loaded. Calling the filter based on reference id");
            filterMessage(referenceId);
        }else if (replyIn[1].contains("archive")){
            System.out.println("Default summary is "+replyIn[1]+". To navigate to Archive summary");
            BrowserUtils.waitForVisibility(archiveTab, TIMEOUT_MINUTE);
            archiveTab.click();
            System.out.println("Clicked on Archive tab");
            BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
            System.out.println("Archive tab is loaded. Calling the filter based on reference id");
            filterMessage(referenceId);


        }
    }

    private void performReply(String action, String referenceId, String message) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] replyIn = action.split("_");
        System.out.println("Size of reply data input is "+replyIn.length +", "+replyIn[0]+", "+replyIn[1] +", "+replyIn[2]);

        try{
            if (replyIn[1].contains("inbox") && replyIn[2].contains("summary")){
                rowCellData1.click();
                BrowserUtils.waitForClickable(replyButton,TIMEOUT_MINUTE);
                System.out.println("Performing '"+replyIn[0]+"' action for message with reference identifier '"+referenceId+"' from "+replyIn[1]+" "+replyIn[2]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",replyButton);
                replyButton.click();
                verifyProcess(referenceId, message);
            }else if (replyIn[1].contains("inbox") && replyIn[2].contains("details")){
                rowCellData1.click();
                BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
                goToDetails.click();
                System.out.println("Performing '"+replyIn[0]+"' action for message with reference identifier '"+referenceId+"' from "+replyIn[1]+" "+replyIn[2]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",replyButtonDetails);
                replyButtonDetails.click();
                verifyProcess(referenceId, message);

            } else if (replyIn[1].contains("sent") && replyIn[2].contains("summary")){
                rowCellData1.click();
                BrowserUtils.waitForVisibility(sentTab, TIMEOUT_MINUTE);
                sentTab.click();
                BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(replyButton, TIMEOUT_MINUTE);
                System.out.println("Performing '"+replyIn[0]+"' action for message with reference identifier '"+referenceId+"' from "+replyIn[1]+" "+replyIn[2]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",replyButton);
                replyButton.click();
                verifyProcess(referenceId, message);

            }else if (replyIn[1].contains("sent") && replyIn[2].contains("details")){
                rowCellData1.click();
                BrowserUtils.waitForVisibility(sentTab, TIMEOUT_MINUTE);
                sentTab.click();
                BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
                goToDetails.click();
                System.out.println("Performing '"+replyIn[0]+"' action for message with reference identifier '"+referenceId+"' from "+replyIn[1]+" "+replyIn[2]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",replyButtonDetails);
                replyButtonDetails.click();
                verifyProcess(referenceId, message);
            } else if (replyIn[1].contains("archive") && replyIn[2].contains("summary")){
                rowCellData1.click();
                BrowserUtils.waitForVisibility(archiveTab, TIMEOUT_MINUTE);
                archiveTab.click();
                BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(replyButton, TIMEOUT_MINUTE);
                System.out.println("Performing '"+replyIn[0]+"' action for message with reference identifier '"+referenceId+"' from "+replyIn[1]+" "+replyIn[2]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",replyButton);
                replyButton.click();
                verifyProcess(referenceId, message);
            } else if (replyIn[1].contains("archive") && replyIn[2].contains("details")){
                rowCellData1.click();
                BrowserUtils.waitForVisibility(archiveTab, TIMEOUT_MINUTE);
                archiveTab.click();
                BrowserUtils.waitForVisibility(archiverowCellData1, TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
                goToDetails.click();
                System.out.println("Performing '"+replyIn[0]+"' action for message with reference identifier '"+referenceId+"' from "+replyIn[1]+" "+replyIn[2]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",replyButtonDetails);
                replyButtonDetails.click();
                verifyProcess(referenceId, message);

            }

        }catch (Exception e){
            System.out.println("Exception in processing the filtered record "+e);

        }

    }

    private void verifyProcess(String referenceId, String message) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(refIdInReplyDetails, TIMEOUT_MINUTE);
        try{
            if (refIdInReplyDetails.getText().contains(referenceId)){
                String domain = domainEleInReply.getText();
                String topic = topicsEleInReply.getText();
                String corporateUser = corporateUserEleInReply.getText();
                String subTopic = subTopicEleInReply.getText();
                String replyDetails[] = {domain, topic, corporateUser, subTopic};
                System.out.println("Sending reply to the following user information. "+"Corporate User '"
                        +replyDetails[2]+" in Domain "+replyDetails[0]+" for the topic "+replyDetails[1]
                        +"and sub-topic "+replyDetails[3]);
                replyTextMessage.sendKeys(message);
                BrowserUtils.waitForVisibility(sendReplyButton, TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(sendReplyButton, TIMEOUT_MINUTE);
                sendReplyButton.click();
            }else {
                System.out.println("Reference Identifier not found in Details");
            }
        }catch (Exception e){
            System.out.println("Exception in replying to the inmail "+e);
        }
    }

    private void filterMessage(String referenceId) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
        try {
            BrowserUtils.waitForClickable(refColFilter, TIMEOUT_MINUTE);
            refColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(referenceId);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(refIdElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(refIdElement, referenceId, TIMEOUT_MINUTE);
            if (refIdElement.getText().contains(referenceId)) {
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                System.out.println("Filtered the record for reference identifier '" + referenceId+"'");
               } else {
                System.out.println("Not filtered");
                System.out.println(refIdElement.getText() + ", " + referenceId);
            }
        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }
    }
}
