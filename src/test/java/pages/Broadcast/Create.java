package pages.Broadcast;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.json.simple.JSONArray;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertNotNull;

public class Create<M extends WebElement> extends Base {
    Base base = new Base();
    // local variables

    // constructor
    public Create() throws Exception {
        super();

    }

    // elements
    @FindBy(xpath = "//button[contains(text(),'Create a new broadcast message')]")
    protected M createBroadcastMessage;

    @FindBy(xpath = "//div/h1")
    protected M formTitle;

    @FindBy(xpath = "//formly-field[1]/formly-wrapper-form-field/div/div/div/formly-field-datetimepicker/div[1]/div[1]/div/clr-date-container/div/div/div/input")
    protected M fromDate;

    @FindBy(xpath = "//formly-field[1]/formly-wrapper-form-field/div/div/div/formly-field-datetimepicker/div[1]/div[2]/cds-time/input")
    protected M fromTime;

    @FindBy(xpath = "//formly-field[2]/formly-wrapper-form-field/div/div/div/formly-field-datetimepicker/div[1]/div[1]/div/clr-date-container/div/div/div/input")
    protected M toDate;

    @FindBy(xpath = "//formly-field[2]/formly-wrapper-form-field/div/div/div/formly-field-datetimepicker/div[1]/div[2]/cds-time/input")
    protected M toTime;

    @FindBy(xpath = "//formly-field-select/select")
    protected M applicableFor;

    @FindBy(xpath = "//formly-field[4]/formly-wrapper-form-field/div/label")
    protected M formLabel;

    @FindBy(xpath = "//formly-field[4]/formly-wrapper-form-field/div/div/div/formly-field-broadcast-domain-grid/a")
    protected M formContextLinkDomain;

    @FindBy(xpath = "////formly-field[5]/formly-wrapper-form-field/div/div/div/formly-field-broadcast-entity-grid/a")
    protected M formContextLinkCustomer;

    @FindBy(xpath = "formly-field[6]/formly-wrapper-form-field/div/div/div/formly-field-broadcast-country-grid/a")
    protected M formContextLinkCountry;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/div")
    protected M modalTitle;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/div")
    protected M modalGridCol1;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/div")
    protected M modalGridCol2;

    @FindBy(xpath = "//clr-dg-column[3]/div/button")
    protected M modalGridCol3;

    @FindBy(xpath = "//clr-dg-column[1]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M modalGridColFilter1;

    @FindBy(xpath = "//clr-dg-column[2]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M modalGridColFilter2;

    @FindBy(xpath = "//clr-dg-column[3]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M modalGridColFilter3;

    @FindBy(xpath = "//input[@name='search']")
    protected M modalGridColSearch;

    @FindBy(xpath = "//clr-icon[@title='Close']")
    protected M modalGridColClose;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//clr-select-container/div/div/select")
    protected M selectLanguage;

    @FindBy(xpath = "//label[contains(text(),'Mark as primary')]")
    protected M languagePrimary;

    @FindBy(xpath = "//div/textarea")
    protected M actMessage;

    @FindBy(xpath = "//a[contains(text(),' Add message ')]")
    protected M addAdditionalMessage;

    @FindBy(xpath = "//a[contains(text(),' Delete message ')]")
    protected M deleteAdditionalMessage;

    @FindBy(xpath = "//button/span[contains(text(),'Review & submit')]")
    protected M reviewAndSubmit;

    @FindBy(xpath = "//button/span[contains(text(),'Cancel')]")
    protected M cancel;

    @FindBy(xpath = "//a[contains(text(),'Select domain')]")
    protected M selectDomain;

    @FindBy(xpath = "//a[contains(text(),'Select entity')]")
    protected M selectEntity;

    @FindBy(xpath = "//a[contains(text(),'Select country')]")
    protected M selectCountry;


    public void loadBroadcastForm() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(createBroadcastMessage,TIMEOUT_MINUTE);
            createBroadcastMessage.click();
            BrowserUtils.waitFor("Generate broadcast message",TIMEOUT_MINUTE);
            //  assertEquals(pageTitle, "Create role");
            return;
        }catch(Exception e){
            System.out.println("Exception in loading the broadcast message creation form");
        }

    }

    public void setbaseInfo(String validFromDate, String validFromTime, String validToDate, String validToTime, String type, String contextMap ) throws Exception {
        setFromDate(validFromDate);
        setFromTime(validFromTime);
        setToDate(validToDate);
        SetToTime(validToTime);
        setApplicableFor(type, contextMap);

    }

    private void setApplicableFor(String type, String contextMap) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(applicableFor, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(applicableFor, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", applicableFor);

            try {
                String s [] = contextMap.split("_");
                int contextSize =s.length;
                System.out.println("Size of applicable for type: "+contextSize);

                String data[] = new String[contextSize];
                for (int i=0; i<contextSize; i++){
                    data[i] = s[i];
                    System.out.println("Values in applicable for size is at index"+"["+i+"]: "+data[i]);
                }
                if ((type.equalsIgnoreCase("All domains") || (type.equalsIgnoreCase("All customers") || (type.equalsIgnoreCase("All countries"))))){
                    setAllContext(type);
                }else if (type.equalsIgnoreCase("Specific domain") || (type.equalsIgnoreCase("Specific customer") || (type.equalsIgnoreCase("Specific country")))){
                    System.out.println("Called the specific type");
                    setSpecificContext(type,s[0],s[1]);
                }
            }catch (Exception e){
                System.out.println("Exception in readng the applicable context"+e);
            }
            System.out.println("Selected the applicable for type: "+type);
        }catch (Exception e){
            System.out.println("Exception in setting applicable for type "+e);

        }

    }

    private void setSpecificContext(String type, String s1, String s2) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(applicableFor, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(applicableFor, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", applicableFor);

            if (applicableFor.isDisplayed()){
                System.out.println("Inside the if block");
                Select select = new Select(applicableFor);
                select.selectByVisibleText(type);
                BrowserUtils.waitForVisibility(applicableFor,TIMEOUT_ELEMENT);
                setContextualData(type,s1, s2);
            }
            System.out.println("Selected the applicable for type: "+type);
        }catch (Exception e){
            System.out.println("Exception in setting applicable for type "+e);

        }
    }

    private void setContextualData(String s0, String s1, String s2) {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        String context = null;
        String valueType = null;
        String contextLabel = null;
        String contextAction = null;
        String appendText = null;
        String filterCol1 = null;
        String filterCol2 = null;
        try {
            if (s0.contains("Specific domain")){
                context = "1";
            }else if (s0.contains("Specific customer")) {
                context = "2";
            }else if (s0.contains("Specific country")) {
                context = "3";
            }

            switch (context){
                case "1":
                    valueType= "Specific domain";
                    contextLabel= "Domain";
                    contextAction= "Select domain";
                    appendText = "domain";
                    filterCol1= "Domain";
                    filterCol2= "Organisation";
                    break;
                case "2":
                    valueType= "Specific customer";
                    contextLabel= "Entity";
                    contextAction= "Select entity";
                    appendText = "entity";
                    filterCol1= "Entity";
                    filterCol2= "Country";
                    break;
                case "3":
                    valueType= "Specific country";
                    contextLabel= "Country";
                    contextAction= "Select country";
                    appendText = "country";
                    filterCol1= "Country";
                    filterCol2= "";
                    break;
            }
            String lookupValue = "formly-field-broadcast-"+appendText+"-grid/a";

            System.out.println("Specific type applied as follows. "+valueType+", \n"
                    +"Running the selection logic based on filter parameters: "+filterCol1 +", "+filterCol2);

            checkData(contextLabel);
            checkContextAction(contextAction);
            filterRecords(s1, s2);
            validateSelectedCount();
            checkUpdatedContext();

        }catch (Exception e){
            System.out.println("Exception in setting the specific type "+e);
        }

    }

    private void checkUpdatedContext() {
    }

    private void validateSelectedCount() {
    }

    private void filterRecords(String s1, String s2) {
        // filter based on input columns and select based on condition evaluation
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            if (modalGridColFilter1.isDisplayed()){
                modalGridColFilter1.click();
                BrowserUtils.waitForClickable(modalGridColSearch,TIMEOUT_MINUTE);
                modalGridColSearch.sendKeys(s1);
                BrowserUtils.waitForClickable(modalGridColClose,TIMEOUT_MINUTE);
                modalGridColClose.click();
                BrowserUtils.waitForGridDataLoad(rowCellData1,s1,TIMEOUT_MINUTE);
                if (rowCellData1.getText().contains(s1)){
                    ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    modalGridCol2.click();
                    BrowserUtils.waitForClickable(modalGridColSearch,TIMEOUT_MINUTE);
                    modalGridColSearch.sendKeys(s2);
                    BrowserUtils.waitForClickable(modalGridColClose,TIMEOUT_MINUTE);
                    modalGridColClose.click();
                    BrowserUtils.waitForGridDataLoad(rowCellData1,s1,TIMEOUT_MINUTE);



                }
            }

        }catch (Exception e){
            System.out.println("Exception in filter modal "+e);
        }


    }

    private void checkContextAction(String contextAction) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(contextAction!=null){
                if (contextAction.equalsIgnoreCase("Select domain")){
                    BrowserUtils.waitForClickable(formContextLinkDomain,TIMEOUT_ELEMENT);
                    System.out.println(formContextLinkDomain.getText());
                    formContextLinkDomain.click();
                } else if (contextAction.equalsIgnoreCase("Select entity")){
                    BrowserUtils.waitForClickable(formContextLinkCustomer,TIMEOUT_ELEMENT);
                    System.out.println(formContextLinkCustomer.getText());
                    formContextLinkCustomer.click();
                } else if (contextAction.equalsIgnoreCase("Select country")){
                    BrowserUtils.waitForClickable(formContextLinkCountry,TIMEOUT_ELEMENT);
                    System.out.println(formContextLinkCountry.getText());
                    formContextLinkCountry.click();
                }
                System.out.println("The form context link is clicked to load the modal");
            }
        }catch(Exception e){
            System.out.println("Exception in loading the form context link "+e);
        }
    }

    private void checkData(String contextLabel) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(contextLabel!=null){
                BrowserUtils.waitForVisibility(formLabel,TIMEOUT_ELEMENT);
                assertNotNull(formLabel.getText().contains(contextLabel));
                System.out.println("The form label is "+formLabel.getText());
            }
        }catch(Exception e){
            System.out.println("Exception in loading the form label "+e);
        }

    }

    private void setAllContext(String s) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(applicableFor, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(applicableFor, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", applicableFor);

            if (applicableFor.isDisplayed()){
                Select select = new Select(applicableFor);
                select.selectByVisibleText(s);
            }
            System.out.println("Selected the applicable for type: "+s);
        }catch (Exception e){
            System.out.println("Exception in setting applicable for type "+e);

        }

    }

    private void SetToTime(String validToTime) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(validToTime!=null){
                BrowserUtils.waitForClickable(toTime,TIMEOUT_ELEMENT);
                setTimeViaKey(toTime);
            }
        }catch (Exception e){
            System.out.println("Exception in setting to time"+e);
        }
    }

    private void setToDate(String validToDate) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(validToDate!=null){
                BrowserUtils.waitForClickable(toDate,TIMEOUT_ELEMENT);
                base.clickAndEnter(toDate, validToDate);
                //assertEquals(roleName.getText(), data);
            }
        }catch (Exception e){
            System.out.println("Exception in setting to date"+e);
        }
    }

    private void setFromTime(String validFromTime) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(validFromTime!=null){
                BrowserUtils.waitForClickable(fromTime,TIMEOUT_ELEMENT);
                setTimeViaKey(fromTime);
            }
        }catch (Exception e){
            System.out.println("Exception in setting from time"+e);
        }
    }

    private void setTimeViaKey(M element) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(element.isEnabled()){
                BrowserUtils.waitForClickable(element,TIMEOUT_ELEMENT);
                element.sendKeys(Keys.ARROW_DOWN);
                element.sendKeys(Keys.TAB);
                element.sendKeys(Keys.ARROW_DOWN);
                element.sendKeys(Keys.RETURN);
            }
        }catch (Exception e){
            System.out.println("Exception in setting the time via Key:"+e);
        }

    }

    private void setFromDate(String validFromDate) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(validFromDate!=null){
                BrowserUtils.waitForClickable(fromDate,TIMEOUT_ELEMENT);
                base.clickAndEnter(fromDate, validFromDate);
                //assertEquals(roleName.getText(), data);
            }
        }catch (Exception e){
            System.out.println("Exception in setting from date"+e);
        }


    }

    public void setMessageInfo(String language, String messageValue, String isPrimary) throws Exception {
        setLanguage(language, isPrimary);
        setActualMessage(messageValue);
    }

    private void setActualMessage(String messageValue) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(messageValue!=null){
                BrowserUtils.waitForClickable(actMessage,TIMEOUT_ELEMENT);
                base.clickAndEnter(actMessage, messageValue);
                reviewSubmit();

            }
        }catch (Exception e){
            System.out.println("Exception in setting the message"+e);
        }



    }

    private void reviewSubmit() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", reviewAndSubmit);
        try{
            BrowserUtils.waitForClickable(reviewAndSubmit,TIMEOUT_MINUTE);
            if(reviewAndSubmit.isEnabled()) {
                reviewAndSubmit.click();
            }

        }catch(Exception e){
            System.out.println("Exception in saving the broadcast message info"+e);
        }


    }

    private void setLanguage(String language, String isPrimary) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(selectLanguage, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(selectLanguage, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", selectLanguage);


            if (selectLanguage.isDisplayed()){
                Select select = new Select(selectLanguage);
                select.selectByVisibleText(language);
            }
            if (isPrimary.contains("yes")){
                languagePrimary.click();
            }
            System.out.println("Selected the language: "+language);
        }catch (Exception e){
            System.out.println("Exception in setting language: "+e);

        }


    }

    private void processData(JSONArray language) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(selectLanguage, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            for (int i=0; i<language.size(); i++){
                System.out.println("Checking for data value from index: "+language.get(i).toString());

            }


            if (selectLanguage.isDisplayed()){
                Select select = new Select(selectLanguage);
                select.selectByVisibleText("language");
            }
            System.out.println("Selected the applicable for type: "+language);
        }catch (Exception e){
            System.out.println("Exception in setting applicable for type "+e);

        }



    }
}
