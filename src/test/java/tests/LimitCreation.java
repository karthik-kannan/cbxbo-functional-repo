package tests;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import framework.JSONDataProvider;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.Login;
import pages.limits.Confirmation;
import pages.limits.Create;
import pages.limits.Review;
import pages.limits.Summary;
import tests.model.Limit;


public class LimitCreation {
    // local vars

    private Login<WebElement> login = null;
    private Summary<WebElement> summary = null;
    private Create<WebElement> limitCreate = null;
    private Review<WebElement> limitReview = null;
    private Confirmation<WebElement> limitConfirm = null;

    private static final String DATA_FILE = "src\\data\\CreateLimit.json";


    // constructor
    public LimitCreation() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        login.logoutApplication(Global_VARS.TARGET_RUN);
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes
        login = new Login<WebElement>();
        summary = new pages.limits.Summary<WebElement>();
        limitCreate = new pages.limits.Create<WebElement>();
        limitReview = new pages.limits.Review<WebElement>();
        limitConfirm = new pages.limits.Confirmation<WebElement>();

        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;
    }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {

    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {
     }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);

    }

    // test methods

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_createRoleLimitNoSelfAuth(String rowID,
                                   String description,
                                   JSONObject testData) throws Exception {

        Limit limit = new Limit(testData);
        String entity = limit.getEntity();
        String event = limit.getEvent();
        String targetStatus = limit.getTargetStatus();
        String domainNameID = limit.getDomainNameID();
        String limitsProfileName = limit.getLimitsProfileName();
        String limitsLevel = limit.getLimitsLevel();
        String currency = limit.getOperationalLimits().get("currency").toString();
        String entityName = limit.getOperationalLimits().get("entityName").toString();
        String dailySingletxnInitiationCount = limit.getOperationalLimits().get("dailySingletxnInitiationCount").toString();
        String dailySingletxnInitiationAmount = limit.getOperationalLimits().get("dailySingletxnInitiationAmount").toString();
        String dailyBulktxnInitiationCount = limit.getOperationalLimits().get("dailyBulktxnInitiationCount").toString();
        String dailyBulktxnInitiationAmount = limit.getOperationalLimits().get("dailyBulktxnInitiationAmount").toString();
        String dailyConsolidatedtxnInitiationCount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationCount").toString();
        String dailyConsolidatedtxnInitiationAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationAmount").toString();
        String dailySingletxnApprovalAmount = limit.getOperationalLimits().get("dailySingletxnApprovalAmount").toString();
        String dailyBulktxnApprovalAmount = limit.getOperationalLimits().get("dailyBulktxnApprovalAmount").toString();
        String dailyConsolidatedtxnApprovalAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnApprovalAmount").toString();
        String approvalAmountPerSingletxn = limit.getOperationalLimits().get("approvalAmountPerSingletxn").toString();
        String approvalAmountPerBulktxn = limit.getOperationalLimits().get("approvalAmountPerBulktxn").toString();
        String selfApprovalAmount = limit.getOperationalLimits().get("selfApprovalAmount").toString();
        String applicableToSubProductLimits = limit.getSubproductLimits().get("applicableToSubProductLimits").toString();
        String subProductCurrency = limit.getSubproductLimits().get("currency").toString();
        JSONArray subProductLimits = (JSONArray) limit.getSubproductLimits().get("limits");
        String roleName = limit.getRoleName();
        String selfApprovalRequired = limit.getOperationalLimits().get("approvalRequired").toString();

        System.out.println(entity+ ", "+targetStatus+", "+domainNameID+ ", "+limitsProfileName+", "+limitsLevel+", "+currency+", "+entityName+", "+dailySingletxnInitiationCount+", "+dailySingletxnInitiationAmount+", "+dailyBulktxnInitiationCount
        +", "+dailyBulktxnInitiationAmount+", "+dailyConsolidatedtxnInitiationCount+", "+dailyConsolidatedtxnInitiationAmount
        +", "+dailySingletxnApprovalAmount+", "+dailyBulktxnApprovalAmount+", "+dailyConsolidatedtxnApprovalAmount
        +", "+approvalAmountPerSingletxn+", "+approvalAmountPerBulktxn+", "+selfApprovalRequired+", "+selfApprovalAmount+", "+applicableToSubProductLimits+", "+subProductCurrency+", "+subProductLimits+", "+roleName);

        login.doContextualLogin(entity, event);
        summary.navigateToLimits();
        summary.loadLimitForm();
        limitCreate.setDomainInfo(domainNameID, limitsProfileName, roleName);
        limitCreate.setLimitsInfo(dailySingletxnInitiationCount, dailySingletxnInitiationAmount, dailyBulktxnInitiationCount, dailyBulktxnInitiationAmount, dailyConsolidatedtxnInitiationCount, dailyConsolidatedtxnInitiationAmount, dailySingletxnApprovalAmount, dailyBulktxnApprovalAmount, dailyConsolidatedtxnApprovalAmount, approvalAmountPerSingletxn, approvalAmountPerBulktxn, selfApprovalAmount, selfApprovalRequired, subProductLimits, applicableToSubProductLimits);
        limitReview.verifyLimits(limitsProfileName, targetStatus);
        limitConfirm.checkLimitSubmission();
        summary.assertCreatedData(targetStatus,limitsProfileName);

    }

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc002_createRoleLimitYesSelfAuth(String rowID,
                                                String description,
                                                JSONObject testData) throws Exception {

        Limit limit = new Limit(testData);
        String entity = limit.getEntity();
        String event = limit.getEvent();
        String targetStatus = limit.getTargetStatus();
        String domainNameID = limit.getDomainNameID();
        String limitsProfileName = limit.getLimitsProfileName();
        String limitsLevel = limit.getLimitsLevel();
        String currency = limit.getOperationalLimits().get("currency").toString();
        String entityName = limit.getOperationalLimits().get("entityName").toString();
        String dailySingletxnInitiationCount = limit.getOperationalLimits().get("dailySingletxnInitiationCount").toString();
        String dailySingletxnInitiationAmount = limit.getOperationalLimits().get("dailySingletxnInitiationAmount").toString();
        String dailyBulktxnInitiationCount = limit.getOperationalLimits().get("dailyBulktxnInitiationCount").toString();
        String dailyBulktxnInitiationAmount = limit.getOperationalLimits().get("dailyBulktxnInitiationAmount").toString();
        String dailyConsolidatedtxnInitiationCount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationCount").toString();
        String dailyConsolidatedtxnInitiationAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationAmount").toString();
        String dailySingletxnApprovalAmount = limit.getOperationalLimits().get("dailySingletxnApprovalAmount").toString();
        String dailyBulktxnApprovalAmount = limit.getOperationalLimits().get("dailyBulktxnApprovalAmount").toString();
        String dailyConsolidatedtxnApprovalAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnApprovalAmount").toString();
        String approvalAmountPerSingletxn = limit.getOperationalLimits().get("approvalAmountPerSingletxn").toString();
        String approvalAmountPerBulktxn = limit.getOperationalLimits().get("approvalAmountPerBulktxn").toString();
        String selfApprovalAmount = limit.getOperationalLimits().get("selfApprovalAmount").toString();
        String applicableToSubProductLimits = limit.getSubproductLimits().get("applicableToSubProductLimits").toString();
        String subProductCurrency = limit.getSubproductLimits().get("currency").toString();
        JSONArray subProductLimits = (JSONArray) limit.getSubproductLimits().get("limits");
        String roleName = limit.getRoleName();
        String selfApprovalRequired = limit.getOperationalLimits().get("approvalRequired").toString();

        System.out.println(entity+ ", "+targetStatus+", "+domainNameID+ ", "+limitsProfileName+", "+limitsLevel+", "+currency+", "+entityName+", "+dailySingletxnInitiationCount+", "+dailySingletxnInitiationAmount+", "+dailyBulktxnInitiationCount
                +", "+dailyBulktxnInitiationAmount+", "+dailyConsolidatedtxnInitiationCount+", "+dailyConsolidatedtxnInitiationAmount
                +", "+dailySingletxnApprovalAmount+", "+dailyBulktxnApprovalAmount+", "+dailyConsolidatedtxnApprovalAmount
                +", "+approvalAmountPerSingletxn+", "+approvalAmountPerBulktxn+", "+selfApprovalRequired+", "+selfApprovalAmount+", "+applicableToSubProductLimits+", "+subProductCurrency+", "+subProductLimits+", "+roleName);

        login.doContextualLogin(entity, event);
        summary.navigateToLimits();
        summary.loadLimitForm();
        limitCreate.setDomainInfo(domainNameID, limitsProfileName, roleName);
        limitCreate.setLimitsInfo(dailySingletxnInitiationCount, dailySingletxnInitiationAmount, dailyBulktxnInitiationCount, dailyBulktxnInitiationAmount, dailyConsolidatedtxnInitiationCount, dailyConsolidatedtxnInitiationAmount, dailySingletxnApprovalAmount, dailyBulktxnApprovalAmount, dailyConsolidatedtxnApprovalAmount, approvalAmountPerSingletxn, approvalAmountPerBulktxn, selfApprovalAmount, selfApprovalRequired, subProductLimits, applicableToSubProductLimits);
        limitReview.verifyLimits(limitsProfileName, targetStatus);
        limitConfirm.checkLimitSubmission();
        summary.assertCreatedData(targetStatus,limitsProfileName);

    }

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc003_draftRoleLimitYesSelfAuth(String rowID,
                                                 String description,
                                                 JSONObject testData) throws Exception {

        Limit limit = new Limit(testData);
        String entity = limit.getEntity();
        String event = limit.getEvent();
        String targetStatus = limit.getTargetStatus();
        String domainNameID = limit.getDomainNameID();
        String limitsProfileName = limit.getLimitsProfileName();
        String limitsLevel = limit.getLimitsLevel();
        String currency = limit.getOperationalLimits().get("currency").toString();
        String entityName = limit.getOperationalLimits().get("entityName").toString();
        String dailySingletxnInitiationCount = limit.getOperationalLimits().get("dailySingletxnInitiationCount").toString();
        String dailySingletxnInitiationAmount = limit.getOperationalLimits().get("dailySingletxnInitiationAmount").toString();
        String dailyBulktxnInitiationCount = limit.getOperationalLimits().get("dailyBulktxnInitiationCount").toString();
        String dailyBulktxnInitiationAmount = limit.getOperationalLimits().get("dailyBulktxnInitiationAmount").toString();
        String dailyConsolidatedtxnInitiationCount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationCount").toString();
        String dailyConsolidatedtxnInitiationAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationAmount").toString();
        String dailySingletxnApprovalAmount = limit.getOperationalLimits().get("dailySingletxnApprovalAmount").toString();
        String dailyBulktxnApprovalAmount = limit.getOperationalLimits().get("dailyBulktxnApprovalAmount").toString();
        String dailyConsolidatedtxnApprovalAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnApprovalAmount").toString();
        String approvalAmountPerSingletxn = limit.getOperationalLimits().get("approvalAmountPerSingletxn").toString();
        String approvalAmountPerBulktxn = limit.getOperationalLimits().get("approvalAmountPerBulktxn").toString();
        String selfApprovalAmount = limit.getOperationalLimits().get("selfApprovalAmount").toString();
        String applicableToSubProductLimits = limit.getSubproductLimits().get("applicableToSubProductLimits").toString();
        String subProductCurrency = limit.getSubproductLimits().get("currency").toString();
        JSONArray subProductLimits = (JSONArray) limit.getSubproductLimits().get("limits");
        String roleName = limit.getRoleName();
        String selfApprovalRequired = limit.getOperationalLimits().get("approvalRequired").toString();

        System.out.println(entity+ ", "+targetStatus+", "+domainNameID+ ", "+limitsProfileName+", "+limitsLevel+", "+currency+", "+entityName+", "+dailySingletxnInitiationCount+", "+dailySingletxnInitiationAmount+", "+dailyBulktxnInitiationCount
                +", "+dailyBulktxnInitiationAmount+", "+dailyConsolidatedtxnInitiationCount+", "+dailyConsolidatedtxnInitiationAmount
                +", "+dailySingletxnApprovalAmount+", "+dailyBulktxnApprovalAmount+", "+dailyConsolidatedtxnApprovalAmount
                +", "+approvalAmountPerSingletxn+", "+approvalAmountPerBulktxn+", "+selfApprovalRequired+", "+selfApprovalAmount+", "+applicableToSubProductLimits+", "+subProductCurrency+", "+subProductLimits+", "+roleName);

        login.doContextualLogin(entity, event);
        summary.navigateToLimits();
        summary.loadLimitForm();
        limitCreate.setDomainInfo(domainNameID, limitsProfileName, roleName);
        limitCreate.setLimitsInfo(dailySingletxnInitiationCount, dailySingletxnInitiationAmount, dailyBulktxnInitiationCount, dailyBulktxnInitiationAmount, dailyConsolidatedtxnInitiationCount, dailyConsolidatedtxnInitiationAmount, dailySingletxnApprovalAmount, dailyBulktxnApprovalAmount, dailyConsolidatedtxnApprovalAmount, approvalAmountPerSingletxn, approvalAmountPerBulktxn, selfApprovalAmount, selfApprovalRequired, subProductLimits, applicableToSubProductLimits);
        limitReview.verifyLimits(limitsProfileName, targetStatus);
        limitConfirm.checkLimitSubmission();
        summary.assertCreatedData(targetStatus,limitsProfileName);

    }

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc004_createRoleSubProductLimitYesSelfAuth(String rowID,
                                                 String description,
                                                 JSONObject testData) throws Exception {

        Limit limit = new Limit(testData);
        String entity = limit.getEntity();
        String event = limit.getEvent();
        String targetStatus = limit.getTargetStatus();
        String domainNameID = limit.getDomainNameID();
        String limitsProfileName = limit.getLimitsProfileName();
        String limitsLevel = limit.getLimitsLevel();
        String currency = limit.getOperationalLimits().get("currency").toString();
        String entityName = limit.getOperationalLimits().get("entityName").toString();
        String dailySingletxnInitiationCount = limit.getOperationalLimits().get("dailySingletxnInitiationCount").toString();
        String dailySingletxnInitiationAmount = limit.getOperationalLimits().get("dailySingletxnInitiationAmount").toString();
        String dailyBulktxnInitiationCount = limit.getOperationalLimits().get("dailyBulktxnInitiationCount").toString();
        String dailyBulktxnInitiationAmount = limit.getOperationalLimits().get("dailyBulktxnInitiationAmount").toString();
        String dailyConsolidatedtxnInitiationCount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationCount").toString();
        String dailyConsolidatedtxnInitiationAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnInitiationAmount").toString();
        String dailySingletxnApprovalAmount = limit.getOperationalLimits().get("dailySingletxnApprovalAmount").toString();
        String dailyBulktxnApprovalAmount = limit.getOperationalLimits().get("dailyBulktxnApprovalAmount").toString();
        String dailyConsolidatedtxnApprovalAmount = limit.getOperationalLimits().get("dailyConsolidatedtxnApprovalAmount").toString();
        String approvalAmountPerSingletxn = limit.getOperationalLimits().get("approvalAmountPerSingletxn").toString();
        String approvalAmountPerBulktxn = limit.getOperationalLimits().get("approvalAmountPerBulktxn").toString();
        String selfApprovalAmount = limit.getOperationalLimits().get("selfApprovalAmount").toString();
        String applicableToSubProductLimits = limit.getSubproductLimits().get("applicableToSubProductLimits").toString();
        String subProductCurrency = limit.getSubproductLimits().get("currency").toString();
        JSONArray subProductLimits = (JSONArray) limit.getSubproductLimits().get("limits");
        String roleName = limit.getRoleName();
        String selfApprovalRequired = limit.getOperationalLimits().get("approvalRequired").toString();

        System.out.println(entity+ ", "+targetStatus+", "+domainNameID+ ", "+limitsProfileName+", "+limitsLevel+", "+currency+", "+entityName+", "+dailySingletxnInitiationCount+", "+dailySingletxnInitiationAmount+", "+dailyBulktxnInitiationCount
                +", "+dailyBulktxnInitiationAmount+", "+dailyConsolidatedtxnInitiationCount+", "+dailyConsolidatedtxnInitiationAmount
                +", "+dailySingletxnApprovalAmount+", "+dailyBulktxnApprovalAmount+", "+dailyConsolidatedtxnApprovalAmount
                +", "+approvalAmountPerSingletxn+", "+approvalAmountPerBulktxn+", "+selfApprovalRequired+", "+selfApprovalAmount+", "+applicableToSubProductLimits+", "+subProductCurrency+", "+subProductLimits+", "+roleName);

        login.doContextualLogin(entity, event);
        summary.navigateToLimits();
        summary.loadLimitForm();
        limitCreate.setDomainInfo(domainNameID, limitsProfileName, roleName);
        limitCreate.setLimitsInfo(dailySingletxnInitiationCount, dailySingletxnInitiationAmount, dailyBulktxnInitiationCount, dailyBulktxnInitiationAmount, dailyConsolidatedtxnInitiationCount, dailyConsolidatedtxnInitiationAmount, dailySingletxnApprovalAmount, dailyBulktxnApprovalAmount, dailyConsolidatedtxnApprovalAmount, approvalAmountPerSingletxn, approvalAmountPerBulktxn, selfApprovalAmount, selfApprovalRequired, subProductLimits, applicableToSubProductLimits);
        limitReview.verifyLimits(limitsProfileName, targetStatus);
        limitConfirm.checkLimitSubmission();
        summary.assertCreatedData(targetStatus,limitsProfileName);

    }

}
