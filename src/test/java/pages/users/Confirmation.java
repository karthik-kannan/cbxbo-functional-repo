package pages.users;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base{

    // local variables


    Base base = new Base();

    //constructor
    public Confirmation() throws Exception {
        super();

    }

    @FindBy(xpath = "//ul/li/a[contains(text(),'Roles')]")
    protected M userTab;

    @FindBy(xpath = "//div/h2")
    protected M modal;

    @FindBy(xpath = "//div[@class='ng-star-inserted']/span")
    protected M confirmationMessage;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a[contains(text(),'Go back to user summary')]")
    protected M userSummaryNav;

    public void checkUserSubmission(String orgName) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(confirmationMessage,TIMEOUT_MINUTE);
            if (!confirmationMessage.getText().isEmpty()){
                System.out.println("User creation flow is completed.");
                System.out.println("Confirmation Text: "+confirmationMessage.getText());
            }else System.out.println("Create Error in User");
            }catch (Exception e){
            System.out.println("Exception in creating the user "+e);
        }finally {
            BrowserUtils.waitForClickable(userSummaryNav,TIMEOUT_MINUTE);
            userSummaryNav.click();
        }

      //  try{
        //    if (modal.getText().contains("Success!")){
          //      System.out.println("Rule created successfully");
           // }if (confirmationMessage.getText().contains(orgName)){
             //   System.out.println("Created a user in the organisation "+orgName);
            //}
            //userSummaryNav.click();
        //}catch (Exception e){
          //  System.out.println("Exception in Confirmation Message "+e);
        //}



    }
}
