package tests.model;

import org.json.simple.JSONObject;

public class Deleteflow {
    private String entity;
    private String event;
    private String key;
    private String value;
    private String action;
    private String targetStatus;

    public Deleteflow(JSONObject object) {
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setKey(object.get("key").toString());
        setValue(object.get("value").toString());
        setAction(object.get("action").toString());
        setTargetStatus(object.get("targetStatus").toString());
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
