package pages.Broadcast;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Review<M extends WebElement> extends Base {
    Base base = new Base();
    // local variables

    // constructor
    public Review() throws Exception {
        super();

    }

    // elements
    @FindBy(xpath = "//button[@title='Edit']")
    protected M edit;

    @FindBy(xpath = "//button[contains(text(),'submit')]")
    protected M submit;

    @FindBy(xpath = "//button[contains(text(),'CANCEL')]")
    protected M cancel;

    @FindBy(xpath = "//broadcast-create-review/div/div/div/div[2]/div[2]/div[1]")
    protected M reviewDate;

    @FindBy(xpath = "//div/clr-dg-cell[1]/div")
    protected M reviewGridLang;

    @FindBy(xpath = "//div/clr-dg-cell[2]/div")
    protected M reviewGridMessage;

    @FindBy(xpath = "//broadcast-create-review/div/div/div/div[2]/div[2]/div[2]")
    protected M reviewApplicableContext;


    public void verifyBroadcastMessage(String type, String language, String messageValue) throws Exception {
        verifyDateIsDisplayed();
        verifyApplicableFor(type);
        verifyLanguage(language);
        verifyMessage(messageValue);
        submitNewBroadcastMessage();
    }

    private void verifyApplicableFor(String type) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(reviewApplicableContext,TIMEOUT_MINUTE);
        try{
            if (reviewApplicableContext.isDisplayed()){
                System.out.println(reviewApplicableContext.getText());
            }

        }catch (Exception e){
            System.out.println("Error in validating the applicable for information: "+e);
        }



    }

    private void submitNewBroadcastMessage() throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(submit,TIMEOUT_MINUTE);
            if(submit.isEnabled()) {
                submit.click();
            }

        }catch(Exception e){
            System.out.println("Exception is submitting broadcast message");
        }

    }

    private void verifyMessage(String messageValue) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(reviewGridMessage,TIMEOUT_MINUTE);
        try{
            if (reviewGridMessage.isDisplayed()){
                if (!reviewGridMessage.getText().isEmpty()){
                    System.out.println(reviewGridMessage.getText());
                }else {
                    System.out.println("Message is not displayed in Grid");
                }
            }

        }catch (Exception e){
            System.out.println("Error in validating the language information in grid: "+e);
        }




    }

    private void verifyLanguage(String language) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(reviewGridLang,TIMEOUT_MINUTE);
        try{
            if (reviewGridLang.isDisplayed()){
                if (!reviewGridLang.getText().isEmpty()){
                    System.out.println(reviewGridLang.getText());
                }else {
                    System.out.println("Language is not displayed in Grid");
                }
            }

        }catch (Exception e){
            System.out.println("Error in validating the language information in grid: "+e);
        }



    }

    private void verifyDateIsDisplayed() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(reviewDate,TIMEOUT_MINUTE);
        try{
            if (reviewDate.isDisplayed()){
                System.out.println(reviewDate.getText());
            }

        }catch (Exception e){
            System.out.println("Error in validating the date information: "+e);
        }


    }
}
