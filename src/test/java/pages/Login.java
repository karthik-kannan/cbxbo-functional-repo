package pages;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import pages.roles.Summary;

import javax.xml.bind.SchemaOutputResolver;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static framework.Global_VARS.*;

public class Login<M extends WebElement> extends Base{
    Base base = new Base();
    // local variables

    // constructor
    public Login() throws Exception {
        super();

    }

    // elements
    @FindBy(xpath = "//input[@name='email']")
    protected M email;

    @FindBy(xpath = "//input[@name='authValue']")
    protected M password;

    @FindBy(xpath = "//button[@type='submit']")
    protected M submit;

    @FindBy(xpath = "//app-header/div/div[3]/clr-dropdown/button/span[2]")
    protected M usersProfile;


    @FindBy(xpath = "//app-header/div/div[3]/clr-dropdown/button")
    protected M exitMenu;

    @FindBy(xpath = "//app-header/div/div[3]/clr-dropdown/clr-dropdown-menu/button")
    protected M logout;

    @FindBy(xpath = "//button[@id='killSessionSubmit']")
    protected M ARXCheckExistingSession;

    @FindBy(xpath = "//div[3]/div[3]/div")
    protected M ARXTargetApp;



    @FindBy(xpath = "//li/a[@title='Logout']")
    protected M ARXLogoutButton;

    @FindBy(xpath = "//h8[@id='logout_modal_title']")
    protected M ARXLogoutModalTitle;

    @FindBy(xpath = "//div/p[@id='logout_modal_body']")
    protected M ARXLogoutModalText;

    @FindBy(xpath = "//button[@id='cancelLogoutButton']")
    protected M ARXLogoutModalCancel;

    @FindBy(xpath = "//button[@id='confirmLogoutButton']")
    protected M ARXLogoutModalLogout;

    @FindBy(xpath = "//input[@id='loginid']")
    protected M arxLoginUserId;

    @FindBy(xpath = "//input[@id='password']")
    protected M arxLoginPassword;

    @FindBy(xpath = "//button[@id='loginSubmit']")
    protected M arxLoginSubmit;

    @FindBy(xpath = "//div/div[2]/strong")
    protected M loggedInEntity;

    @FindBy(xpath = "//div[@title='IGTB-CBXBO']")
    protected M appCBXBO;


    String cbxBOApp = "IGTB-CBXBO";

  //common methods
    /**
     * loadPage method to navigate to Target URL
     *
     * @param url
     * @param timeout
     * @throws Exception
     */
    public void loadPage(String url, int timeout) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        driver.manage().deleteAllCookies();
        driver.navigate().to(url);

        // wait for page URL
        BrowserUtils.waitForURL(Global_VARS.TARGET_URL, timeout);
    }

    /**
     * loadPage method to navigate to Target URL
     *
     * @throws Exception
     */
    public void loadPage(int timeout) throws Exception {
        if (Global_VARS.TARGET_RUN.equalsIgnoreCase("DIT")){
            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserEmail);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);
            launchApplication(userEmail,userPassword);
        } else if (Global_VARS.TARGET_RUN.equalsIgnoreCase("PAM")){
            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserEmail);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);
            launchApplicationViaArx(userEmail,userPassword);
        } else if(Global_VARS.TARGET_RUN.equalsIgnoreCase("SIT")){
            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserEmail);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);
            launchApplicationViaArx(userEmail,userPassword);
        }
    }

   private void launchApplicationViaArx(String userEmail, String userPassword) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(arxLoginUserId, Global_VARS.TIMEOUT_MINUTE);
        try{
            arxLoginUserId.sendKeys(userEmail);
            arxLoginPassword.sendKeys(userPassword);
            arxLoginSubmit.click();
            if (!loggedInEntity.getText().isEmpty()){
                System.out.println("Logged into ARX application");
                loadDeployedApp(cbxBOApp);
            }else //if (ARXCheckExistingSession.isEnabled())
                 {
                     BrowserUtils.waitForClickable(ARXCheckExistingSession,TIMEOUT_MINUTE);
                ARXCheckExistingSession.click();
                loadDeployedApp(cbxBOApp);

            }
        }catch (Exception e){
            System.out.println("Exception in logging to ARX application "+e);
        }

    }

    private void loadDeployedApp(String s) throws Exception {
        checkLoggedInEntity();
        checkAppsDeployed(s);
    }

    private void checkLoggedInEntity() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(loggedInEntity, Global_VARS.TIMEOUT_MINUTE);
        try{
            if (loggedInEntity.getText() != null){
                System.out.println("User is now logged in to the entity "+loggedInEntity.getText());
          }
        }catch (Exception e){
            System.out.println("Exception in retrieving the Entity "+e);
        }
    }

    private void checkAppsDeployed(String s) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(loggedInEntity, Global_VARS.TIMEOUT_MINUTE);
        try{
            List<WebElement> deployedApps = driver.findElements(By.xpath("//div[2]/p/a"));
            for (WebElement apps: deployedApps){
                System.out.println("Available application in environment: "+apps.getText());
              }
            String mainWindowHandle = driver.getWindowHandle();
            Set<String> handles = driver.getWindowHandles();

            System.out.println("Total windows before click: " +handles.size());
            System.out.println("Window handle before click is "+mainWindowHandle);

            appCBXBO.click();

            ARXLogoutButton.click();
            BrowserUtils.waitForVisibility(ARXLogoutModalTitle, Global_VARS.TIMEOUT_MINUTE);


            if (ARXLogoutModalTitle.getText().equalsIgnoreCase("Log Out")){
                String logoutText = ARXLogoutModalText.getText();
                System.out.println(logoutText);
                ARXLogoutModalCancel.click();
            }

            handles = driver.getWindowHandles();
            System.out.println("Total windows after click: " +handles.size());

            System.out.println("Switching to new window tab");

            for (String h:handles){
                System.out.println(h);
                if (!h.equalsIgnoreCase(mainWindowHandle)){
                    driver.switchTo().window(h);
                    BrowserUtils.waitForClickable(usersProfile, Global_VARS.TIMEOUT_MINUTE);
                    System.out.println("Window title is: "+driver.getTitle());
                    System.out.println("Window URL is: "+driver.getCurrentUrl());
                }
            }
            System.out.println(driver.findElement(By.xpath("//div/div[1]/div/div/div/div/ul/li[2]/a")).getText());


         //  transitionToTargetApp();

        }catch (Exception e){
            System.out.println("Exception in loading the target application. "+e);

        }



    }

    private void transitionToTargetApp() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(loggedInEntity, Global_VARS.TIMEOUT_MINUTE);
        try {
            String mainWindowHandle = driver.getWindowHandle();
            System.out.println("After click Main window is "+mainWindowHandle);
            System.out.println(driver.getTitle());
            driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
            System.out.println("After 1 control tab"+driver.getTitle());
            driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
            System.out.println("After 2 control tab"+driver.getTitle());

            String afterCTRLTWindowHandle = driver.getWindowHandle();
            System.out.println("After control tab Main window is "+afterCTRLTWindowHandle);

            Set<String> allWindowHandles = driver.getWindowHandles();
            int count = allWindowHandles.size();
            System.out.println("Total windows "+count);
            driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");

            String SIT_HOST="e7b7d6a4-us-south.lb.appdomain.cloud/";
            String SUITE_TARGET_APP="cbxbo/";
            String HSBC_client_id="131804060198305";
            String SIT_REDIRECT_URL =
                    "http://"+SIT_HOST+"/ARXSSO/oauth/authorize?"+"client_id="+HSBC_client_id+"&redirect_uri=http://"+SIT_HOST+SUITE_TARGET_APP+"& scope=offline_access&response_type=code&lang=en_US";

            String SIT_REDIRECT_APP_URL = "http://"+SIT_HOST+"ARXSSO/oauth/authorize?client_id="+HSBC_client_id+"&redirect_uri=http://"+SIT_HOST+SUITE_TARGET_APP+"&scope=offline_access&response_type=code&lang=en_US";

            driver.get("http://e7b7d6a4-us-south.lb.appdomain.cloud/ARXSSO/oauth/authorize?client_id=131804060198305&redirect_uri=http://e7b7d6a4-us-south.lb.appdomain.cloud/cbxbo/&scope=offline_access&response_type=code&lang=en_US");
            System.out.println(SIT_REDIRECT_APP_URL);
 //           driver.get(SIT_REDIRECT_URL);



            Iterator<String> iterator = allWindowHandles.iterator();
            while(iterator.hasNext()){
                String ChildWindow = iterator.next();
                System.out.println("After click Child window is "+ChildWindow);
                System.out.println("After click Child window"+driver.getTitle());
            }


        }catch (Exception e){
            System.out.println("Exception in navigating to the target application"+e);
        }

    }


    /**
     * launchApplication method to login to Back Office application
     *
     *
     */
    public Summary launchApplication(String userEmail, String userPassword) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(email, Global_VARS.TIMEOUT_MINUTE);
        email.sendKeys(userEmail);
        password.sendKeys(userPassword);
        submit.click();
        BrowserUtils.waitForClickable(usersProfile, Global_VARS.TIMEOUT_MINUTE);
        return new Summary();
    }


    public void doContextualLogin(String entity, String event) throws Exception {

        if (Global_VARS.TARGET_RUN.equalsIgnoreCase("DIT")){
            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserMakerEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserCheckerEmail = TARGET_RUN + "_USER_CHECKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);

            if (!event.equalsIgnoreCase("authorize")){
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserMakerEmail);
                launchApplication(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            } else {
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserCheckerEmail);
                launchApplication(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            }
        }else if (Global_VARS.TARGET_RUN.equalsIgnoreCase("PAM")){

            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserMakerEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserCheckerEmail = TARGET_RUN + "_USER_CHECKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);

            if (!event.equalsIgnoreCase("authorize")){
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserMakerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            } else {
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserCheckerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            }
        }else if (Global_VARS.TARGET_RUN.equalsIgnoreCase("iPAM")){

            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserMakerEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserCheckerEmail = TARGET_RUN + "_USER_CHECKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);

            if (!event.equalsIgnoreCase("authorize")){
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserMakerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            } else {
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserCheckerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            }
        }else if (Global_VARS.TARGET_RUN.equalsIgnoreCase("SIT")){

            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserMakerEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserCheckerEmail = TARGET_RUN + "_USER_CHECKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);

            if (!event.equalsIgnoreCase("authorize")){
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserMakerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            } else {
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserCheckerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            }


        }else if (Global_VARS.TARGET_RUN.equalsIgnoreCase("NEW")){

            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserMakerEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserCheckerEmail = TARGET_RUN + "_USER_CHECKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);

            if (!event.equalsIgnoreCase("authorize")){
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserMakerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            } else {
                String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserCheckerEmail);
                launchApplicationViaArx(userEmail,userPassword);
                System.out.println("Logged into "+Global_VARS.ENVIRONMENT+"/ "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);
            }
        }

    }


    public void logoutApplication(String environment) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(exitMenu, Global_VARS.TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        if (environment.equalsIgnoreCase("iPAM") ||
                (environment.equalsIgnoreCase("PAM") ||
                        (environment.equalsIgnoreCase("SIT")))) {
            //Implementing the integration logout logic

            try {
                exitMenu.click();
                BrowserUtils.waitForClickable(logout, Global_VARS.TIMEOUT_MINUTE);
                js.executeScript("arguments[0].scrollIntoView();", logout);
                logout.click();
                Set<String> handles = driver.getWindowHandles();
                for (String h: handles){
                    System.out.println(h);
                    driver.switchTo().window(h);
                    ARXLogoutButton.click();
                    BrowserUtils.waitForVisibility(ARXLogoutModalLogout,TIMEOUT_MINUTE);
                    ARXLogoutModalLogout.click();
                }
            } catch (Exception e){
                System.out.println("Exception while logging out from ARX. "+e);
            } finally {
                CreateDriver.getInstance().closeDriver();
            }

        } else if (environment.equalsIgnoreCase("DIT")) {
            try {
                exitMenu.click();
                BrowserUtils.waitForClickable(logout, Global_VARS.TIMEOUT_MINUTE);
                js.executeScript("arguments[0].scrollIntoView();", logout);
                logout.click();
            } catch (Exception e) {
                System.out.println("Exception in logging out of application " + e);
            } finally {
                CreateDriver.getInstance().closeDriver();
            }
        }
    }
}