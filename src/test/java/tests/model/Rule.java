package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Rule {

    private String entity;
    private String event;
    private String domain;
    private String ruleName;
    private String channels;
    private String from;
    private String to;
    private String workFlowType;
    private String product;
    private String targetStatus;

    private JSONArray accounts;
    private JSONArray virtualAccounts;
    private JSONObject authInfo;

    public Rule(JSONObject object){
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setDomain(object.get("domain").toString());
        setRuleName(object.get("ruleName").toString());
        setChannels(object.get("channels").toString());
        setFrom(object.get("from").toString());
        setTo(object.get("to").toString());
        setWorkFlowType(object.get("workFlowType").toString());
        setProduct(object.get("product").toString());
        setAccounts((JSONArray) object.get("accounts"));
        setVirtualAccounts((JSONArray) object.get("virtualAccounts"));
        setAuthInfo((JSONObject) object.get("authInfo"));

    }

    public String getWorkFlowType() {
        return workFlowType;
    }

    public void setWorkFlowType(String workFlowType) {
        this.workFlowType = workFlowType;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getChannels() {
        return channels;
    }

    public void setChannels(String channels) {
        this.channels = channels;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public JSONArray getAccounts() {
        return accounts;
    }

    public void setAccounts(JSONArray accounts) {
        this.accounts = accounts;
    }

    public JSONArray getVirtualAccounts() {
        return virtualAccounts;
    }

    public void setVirtualAccounts(JSONArray virtualAccounts) {
        this.virtualAccounts = virtualAccounts;
    }

    public JSONObject getAuthInfo() {
        return authInfo;
    }

    public void setAuthInfo(JSONObject authInfo) {
        this.authInfo = authInfo;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }
}
