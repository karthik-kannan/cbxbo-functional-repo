package pages.reports;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Summary<M extends WebElement> extends Base {

    // local variables
    Base base = new Base();

    //constructor
    public Summary() throws Exception {
        super();

    }

    @FindBy(xpath = "//button[@title='Generate a new report']")
    protected M generateReport;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//button/span/span[contains(text(),'DOWNLOAD')]")
    protected M downloadReport;

    @FindBy(xpath = "//clr-dg-column[1]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M reportColFilter;

    @FindBy(xpath = "//div/input[@name='search']")
    protected M searchKey;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M filteredReportNameElement;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[2]")
    protected M filteredReportIdElement;

    @FindBy(xpath = "//ul/li/a[contains(text(),'Reports')]")
    protected M reportTab;

    @FindBy(xpath = "//clr-dg-cell[5]/record-status-component/div/div[2]/div/span")
    protected M statusColAssert;


    public void navigateToReports() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
        try {
            if (!reportTab.isSelected()) {
                BrowserUtils.waitForClickable(reportTab, Global_VARS.TIMEOUT_MINUTE);
                base.clickTheMenu(reportTab);
            }
        } catch (Exception e) {
            System.out.println("Exception in navigating to reports tab " + e);
        }

    }

    public void assertGeneratedReport(String reportId, String targetStatus) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterGeneratedRecord(reportId);
        try {
            if (!statusColAssert.getText().isEmpty()) {
                BrowserUtils.waitForVisibility(statusColAssert, TIMEOUT_MINUTE);
                Assert.assertEquals(statusColAssert.getText(), targetStatus);
                waitForPageLoad();
            }
        } catch (Exception e) {
            System.out.println("Exception in asserting the value " + e);
        }

    }

    public void waitForPageLoad() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
        BrowserUtils.waitForClickable(rowCellData1, TIMEOUT_MINUTE);

    }

    private void filterGeneratedRecord(String reportId) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            if (!reportTab.isSelected()) {
                navigateToReports();
                BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            }
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            reportColFilter.click();
            BrowserUtils.waitForClickable(searchKey, TIMEOUT_MINUTE);
            searchKey.sendKeys(reportId);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(filteredReportIdElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(filteredReportIdElement, reportId, TIMEOUT_MINUTE);
            if (filteredReportIdElement.getText().contains(reportId)) {
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(downloadReport,TIMEOUT_MINUTE);
                System.out.println("Filtered the record with report identifier: " + reportId);
            } else {
                System.out.println("Not filtered");
                System.out.println(filteredReportIdElement.getText() + ", " + reportId);
            }

        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }



    }

    public void downloadGeneratedReport() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(downloadReport, TIMEOUT_MINUTE);
        try {
            if (downloadReport.isEnabled()) {
                downloadReport.click();
                BrowserUtils.waitForClickable(downloadReport, TIMEOUT_MINUTE);
                System.out.println("Clicked on Download for the filtered report");
            }else {
                System.out.println("Unable to Download from Report Summary");
            }
        } catch (Exception e) {
            System.out.println("Exception in downloading the generated report " + e);
        }


    }
}
