package tests;

import com.google.sitebricks.client.Web;
import framework.*;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.Base;
import pages.LoginEnhance;
import tests.model.Mailbox;
import tests.model.ServiceRequestflow;

@Listeners(TestNG_ConsoleRunner.class)
public class ServiceRequestflows {
    Logger log = Logger.getLogger(ServiceRequestflows.class);

    // local vars

    private LoginEnhance<WebElement> login = null;

    private pages.users.Summary<WebElement> userSummary = null;
    private pages.roles.Summary<WebElement> roleSummary = null;
    private pages.rules.Summary<WebElement> ruleSummary = null;

   private pages.servicerequests.Complete<WebElement> completeSRQ = null;
   private pages.servicerequests.Details<WebElement> detailsSRQ = null;
   private pages.servicerequests.Summary summarySRQ = null;
   private pages.servicerequests.Confirmation confirmSRQ = null;

    private static final String DATA_FILE = "src\\data\\ServiceRequestflow.json";

    // constructor
    public ServiceRequestflows() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {
        Base.cleanUpData();

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes

        login = new LoginEnhance<>();
        userSummary = new pages.users.Summary<>();
        completeSRQ = new pages.servicerequests.Complete<WebElement>();
        detailsSRQ = new pages.servicerequests.Details<WebElement>();
        summarySRQ = new pages.servicerequests.Summary<WebElement>();
        confirmSRQ = new pages.servicerequests.Confirmation<WebElement>();

        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;
    }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {
    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {


    }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);
        userSummary.navigateToUser();
        login.logoutTargetApp();

    }

    // test methods

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_serviceRequestProcess(String rowID,
                                   String description,
                                   JSONObject testData) throws Exception {

        ServiceRequestflow serviceRequests = new ServiceRequestflow(testData);

        String entity = serviceRequests.getEntity();
        String event = serviceRequests.getEvent();
        String targetStatus = serviceRequests.getTargetStatus();
        String action = serviceRequests.getAction();
        String referenceId = serviceRequests.getReferenceId();
        String acceptanceStatus = serviceRequests.getAcceptanceStatus();
        String completionNotes = serviceRequests.getCompletionNotes();
        JSONArray comments = serviceRequests.getComments();

        System.out.println(entity+", "+event+", "+targetStatus+", "+action+", "+referenceId+", "+comments+", "+acceptanceStatus+", "+completionNotes);

        login.doContextualLogin(entity, event);
        summarySRQ.navigateToServiceRequest();
        summarySRQ.processServiceRequest(entity, event, targetStatus, action, referenceId, comments, acceptanceStatus, completionNotes);
        confirmSRQ.checkServiceRequestSubmission(targetStatus);

    }


    }

