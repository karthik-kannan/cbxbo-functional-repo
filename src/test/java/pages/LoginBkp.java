package pages;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.roles.Summary;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static framework.Global_VARS.TARGET_RUN;

public class LoginBkp<M extends WebElement> extends Base{
    Base base = new Base();
    // local variables

    // constructor
    public LoginBkp() throws Exception {
        super();

    }

    // elements
    @FindBy(xpath = "//input[@name='email']")
    protected M email;

    @FindBy(xpath = "//input[@name='authValue']")
    protected M password;

    @FindBy(xpath = "//button[@type='submit']")
    protected M submit;

    @FindBy(xpath = "//input[@id='loginid']")
    protected M arxLoginUserId;

    @FindBy(xpath = "//input[@id='password']")
    protected M arxLoginPassword;

    @FindBy(xpath = "//button[@id='loginSubmit']")
    protected M arxLoginSubmit;

    @FindBy(xpath = "//div/div[2]/strong")
    protected M loggedInEntity;

    @FindBy(xpath = "//div[@title='IGTB-CBXBO']")
    protected M appCBXBO;


    String cbxBOApp = "IGTB-CBXBO";


    //common methods
    /**
     * loadPage method to navigate to Target URL
     *
     * @param url
     * @param timeout
     * @throws Exception
     */
    public void loadPage(String url, int timeout) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        driver.navigate().to(url);

        // wait for page URL
        BrowserUtils.waitForURL(Global_VARS.TARGET_URL, timeout);
    }

    /**
     * loadPage method to navigate to Target URL
     *
     * @throws Exception
     */
    public void loadPage(int timeout) throws Exception {
        if (Global_VARS.TARGET_RUN.equalsIgnoreCase("DIT")){
            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserEmail);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            System.out.println("Logged into "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);
            launchApplication(userEmail,userPassword);
        } else if (Global_VARS.TARGET_RUN.equalsIgnoreCase("PAM")){
            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserEmail);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            System.out.println("Logged into "+Global_VARS.TARGET_RUN+" environment" +"as "+userEmail);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);
            launchApplicationViaArx(userEmail,userPassword);
        } else if(Global_VARS.TARGET_RUN.equalsIgnoreCase("SIT")){
            String concatUrl = TARGET_RUN + "_TARGET_URL";
            String concatUserEmail = TARGET_RUN + "_USER_MAKER_EMAIL";
            String concatUserPassword = TARGET_RUN + "_USER_PASSWORD";
            String url = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUrl);
            String userEmail = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserEmail);
            String userPassword = BrowserUtils.lookupMessage(Global_VARS.ENVIRONMENT_CONFIG_FILE, concatUserPassword);

            System.out.println("Logged into "+Global_VARS.TARGET_RUN+" environment" +" as "+userEmail);

            WebDriver driver = CreateDriver.getInstance().getDriver();
            driver.navigate().to(url);
            launchApplicationViaArx(userEmail,userPassword);
        }
    }

   private void launchApplicationViaArx(String userEmail, String userPassword) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(arxLoginUserId, Global_VARS.TIMEOUT_MINUTE);
        try{
            arxLoginUserId.sendKeys(userEmail);
            arxLoginPassword.sendKeys(userPassword);
            arxLoginSubmit.click();
            System.out.println("Logged into ARX application");

            loadDeployedApp(cbxBOApp);

        }catch (Exception e){
            System.out.println("Exception in logging to ARX application "+e);
        }

    }

    private void loadDeployedApp(String s) throws Exception {
        checkLoggedInEntity();
        checkAppsDeployed(s);
    }

    private void checkLoggedInEntity() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(loggedInEntity, Global_VARS.TIMEOUT_MINUTE);
        try{
            if (loggedInEntity.getText() != null){
                System.out.println("User is now logged in to the entity "+loggedInEntity.getText());
          }
        }catch (Exception e){
            System.out.println("Exception in retrieving the Entity "+e);
        }
    }

    private void checkAppsDeployed(String s) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(loggedInEntity, Global_VARS.TIMEOUT_MINUTE);
        try{
            List<WebElement> deployedApps = driver.findElements(By.xpath("//div[2]/p/a"));
            for (WebElement apps: deployedApps){
                System.out.println("Available application in environment: "+apps.getText());
              }
            appCBXBO.click();
            String arxWindowHandle = driver.getWindowHandle();
            System.out.println("Window handle before click is "+arxWindowHandle);
            System.out.println(driver.getTitle());
           transitionToTargetApp();

        }catch (Exception e){
            System.out.println("Exception in loading the target application");

        }



    }

    private void transitionToTargetApp() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(loggedInEntity, Global_VARS.TIMEOUT_MINUTE);
        try {
            String mainWindowHandle = driver.getWindowHandle();
            System.out.println("After click Main window is "+mainWindowHandle);
            System.out.println(driver.getTitle());
            driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
            System.out.println("After 1 control tab"+driver.getTitle());
            driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
            System.out.println("After 2 control tab"+driver.getTitle());

            String afterCTRLTWindowHandle = driver.getWindowHandle();
            System.out.println("After control tab Main window is "+afterCTRLTWindowHandle);

            Set<String> allWindowHandles = driver.getWindowHandles();
            int count = allWindowHandles.size();
            System.out.println("Total windows "+count);
            driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
            driver.get("http://e7b7d6a4-us-south.lb.appdomain.cloud/ARXSSO/oauth/authorize?client_id=131804060198305&redirect_uri=http://e7b7d6a4-us-south.lb.appdomain.cloud/cbxbo/&scope=offline_access&response_type=code&lang=en_US");



            Iterator<String> iterator = allWindowHandles.iterator();
            while(iterator.hasNext()){
                String ChildWindow = iterator.next();
                System.out.println("After click Child window is "+ChildWindow);
                System.out.println("After click Child window"+driver.getTitle());
            }


        }catch (Exception e){
            System.out.println("Exception in navigating to the target application"+e);
        }

    }


    /**
     * launchApplication method to login to Back Office application
     *
     *
     */
    public Summary launchApplication(String userEmail, String userPassword) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(email, Global_VARS.TIMEOUT_MINUTE);
        email.sendKeys(userEmail);
        password.sendKeys(userPassword);
        submit.click();
        return new Summary();
    }



}