package pages.users;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

public class Details<M extends WebElement> extends Base {

    // local variables


    Base base = new Base();

    //constructor
    public Details() throws Exception {
        super();

    }

    @FindBy(xpath = "//ul/li/a[contains(text(),'Roles')]")
    protected M roleTab;

    @FindBy(xpath = "//ul/li/a[contains(text(),'Users')]")
    protected M userTab;

    public void navigateToUser() throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(userTab, Global_VARS.TIMEOUT_MINUTE);
        base.clickTheMenu(userTab);
    }
}
