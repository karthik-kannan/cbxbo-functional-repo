package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class BroadcastMessage {
    private String entity;
    private String event;
    private String targetStatus;
    private String key;
    private String value;
    private String validFromDate;
    private String validFromTime;
    private String validToDate;
    private String validToTime;
    private JSONObject applicableFor;
    private JSONObject message;

    public BroadcastMessage(JSONObject object) {
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setKey(object.get("key").toString());
        setValue(object.get("value").toString());
        setValidFromDate(object.get("validFromDate").toString());
        setValidFromTime(object.get("validFromTime").toString());
        setValidToDate(object.get("validToDate").toString());
        setValidToTime(object.get("validToTime").toString());
        setApplicableFor((JSONObject) object.get("applicableFor"));
        setMessage((JSONObject) object.get("message"));
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValidFromDate() {
        return validFromDate;
    }

    public void setValidFromDate(String validFromDate) {
        this.validFromDate = validFromDate;
    }

    public String getValidFromTime() {
        return validFromTime;
    }

    public void setValidFromTime(String validFromTime) {
        this.validFromTime = validFromTime;
    }

    public String getValidToDate() {
        return validToDate;
    }

    public void setValidToDate(String validToDate) {
        this.validToDate = validToDate;
    }

    public String getValidToTime() {
        return validToTime;
    }

    public void setValidToTime(String validToTime) {
        this.validToTime = validToTime;
    }

    public JSONObject getApplicableFor() {
        return applicableFor;
    }

    public void setApplicableFor(JSONObject applicableFor) {
        this.applicableFor = applicableFor;
    }

    public JSONObject getMessage() {
        return message;
    }

    public void setMessage(JSONObject message) {
        this.message = message;
    }
}
