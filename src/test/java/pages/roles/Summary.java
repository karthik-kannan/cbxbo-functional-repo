package pages.roles;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import java.util.List;

import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertEquals;

public class Summary<M extends WebElement> extends Base {

    // local variables
    Base base = new Base();

    //constructor
    public Summary() throws Exception {
        super();

    }

    String rejectionData = "Rejecting the record";

    @FindBy(xpath = "//ul/li/a[contains(text(),'Roles')]")
    protected M roleTab;

    @FindBy(xpath = "//clr-dg-column[1]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M roleColFilter;

    @FindBy(xpath = "//div[@class='datagrid-scrolling-cells']/clr-dg-cell/div/div")
    protected M cellData;

    @FindBy(xpath = "//div/input[@name='search']")
    protected M searchKey;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//clr-dg-row[\"+i+\"]/div/div[2]/div/clr-dg-cell[4]/record-status-component/div/div[2]/div[1]/span")
    protected M status;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//input[@name='search']")
    protected M search;

    @FindBy(xpath = "//div/div/button/clr-icon")
    protected M closeIcon;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M roleNameElement;

    @FindBy(xpath = "//button[@title='Approve']")
    protected M approveSummary;

    @FindBy(xpath = "//button[@title='Reject']")
    protected M rejectSummary;

    @FindBy(xpath = "//span[@title='Approve']")
    protected M approveDetails;

    @FindBy(xpath = "//span[@title='Reject']")
    protected M rejectDetails;

    @FindBy(xpath = "//button[@title='Delete']")
    protected M deleteSummary;

    @FindBy(xpath = "//button[2]/span[contains(text(),'Delete')]")
    protected M deleteDetails;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/h2")
    protected M deletePrompt;

    @FindBy(xpath = "//div[@class='modal-body']/div")
    protected M deletePromptMessage;

    @FindBy(xpath = "//button[contains(text(),' Yes, delete ')]")
    protected M deletePromptYes;

    @FindBy(xpath = "//button[contains(text(),'No, don')]")
    protected M deletePromptNo;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/h2")
    protected M deleteConfirmationModal;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div")
    protected M deleteConfirmationText;

    @FindBy(xpath = "//button[@title='Go to role detail']")
    protected M goToDetails;

    @FindBy(xpath = "//div/h2")
    protected M authConfirmationTitle;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]")
    protected M authConfirmationText;

    @FindBy(xpath = "//textarea[@name='reason']")
    protected M rejectReason;

    @FindBy(xpath = "//button/span[contains(text(),'Reject')]")
    protected M rejectSubmit;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a[contains(text(),'Go back to role summary')]")
    protected M goToRoleSummary;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a[contains(text(),'Go back to role summary')]")
    protected M goToSummary;

    @FindBy(xpath = "//clr-dg-cell[4]/record-status-component/div/div[2]/div[1]/span")
    protected M statusColAssert;

    public void authFilter(String key, String value, String action, String targetStatus) throws Exception {
        filterRecord(key, value);
        authenticate(key, value, action);
        verifyAuthentication(value);
        asssertValue(key, value, targetStatus);
    }

    private void asssertValue(String key, String value, String targetStatus) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterRecord(key, value);
        try {
            if (!statusColAssert.getText().isEmpty()) {
                BrowserUtils.waitForVisibility(statusColAssert, TIMEOUT_MINUTE);
                Assert.assertEquals(statusColAssert.getText(), targetStatus);
                System.out.println("Asserting the filtered record is completed");
                waitForPageLoad();
            }
        } catch (Exception e) {
            System.out.println("Exception in asserting the value " + e);

        }

    }

    private void verifyAuthentication(String value) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(authConfirmationTitle, TIMEOUT_MINUTE);

        try {
            BrowserUtils.waitForVisibility(authConfirmationText, TIMEOUT_MINUTE);
            if (!authConfirmationText.getText().isEmpty()) {
                System.out.println("Role authentication flow is completed.");
                System.out.println("Confirmation Text: " + authConfirmationText.getText());
            } else System.out.println("Authentication Error in Role");

        } catch (Exception e) {
            System.out.println("Exception in verifying the authentication " + e);
        }finally {
            BrowserUtils.waitForClickable(goToSummary, TIMEOUT_MINUTE);
            goToSummary.click();
            waitForPageLoad();
        }

    }

    private void authenticate(String key, String value, String action) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] authIn = action.split("_");
        System.out.println("Size of authentication data is " + authIn.length + ", " + authIn[0] + ", " + authIn[1]);

        try {
            if (authIn[0].contains("authorize") && authIn[1].contains("summary")) {
                System.out.println(authIn[0] + ", " + authIn[1]);
                BrowserUtils.waitForGridDataLoad(roleNameElement, value, TIMEOUT_MINUTE);
                rowCellData1.click();
                BrowserUtils.waitForClickable(approveSummary, TIMEOUT_MINUTE);
                System.out.println("Authorising the role with role name " + value + " from Summary");
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", approveSummary);
                approveSummary.click();
            } else if (authIn[0].contains("reject") && authIn[1].contains("summary")) {
                System.out.println(authIn[0] + ", " + authIn[1]);
                BrowserUtils.waitForGridDataLoad(roleNameElement, value, TIMEOUT_MINUTE);
                rowCellData1.click();
                BrowserUtils.waitForClickable(rejectSummary, TIMEOUT_MINUTE);
                System.out.println("Rejecting the role with role name " + value + " from Summary");
                reject(rejectSummary);
            } else if (authIn[0].contains("authorize") && authIn[1].contains("details")) {
                System.out.println(authIn[0] + ", " + authIn[1]);
                BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(approveDetails, TIMEOUT_MINUTE);
                System.out.println("Authorising the role with role name " + value + " from Details");
                approveDetails.click();
            } else if (authIn[0].contains("reject") && authIn[1].contains("details")) {
                System.out.println(authIn[0] + ", " + authIn[1]);
                BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(rejectDetails, TIMEOUT_MINUTE);
                System.out.println("Rejecting the role with role name " + value + " from Details");
                reject(rejectDetails);
            }

        } catch (Exception e) {
            System.out.println("Exception in authenticating the filtered record " + e);

        }

    }

    private void reject(M element) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(element, TIMEOUT_MINUTE);
        try {
            element.click();
            rejectReason.sendKeys(rejectionData);
            rejectSubmit.click();
            return;
        } catch (Exception e) {
            System.out.println("Exception while rejecting the record " + e);
        }


    }

    private void filterRecord(String key, String value) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            if (!roleTab.isSelected()) {
                navigateToRole();
                BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            }
            roleColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(value);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            //  List<WebElement> userElements = driver.findElements(By.xpath("//div/div[2]/div/clr-dg-cell[1]"));
            // for (WebElement userElement: userElements){
            BrowserUtils.waitForVisibility(roleNameElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(roleNameElement, value, TIMEOUT_MINUTE);
            if (roleNameElement.getText().contains(value)) {
                System.out.println("Filtered the record for authentication " + value);
            } else {
                System.out.println("Not filtered");
                System.out.println(roleNameElement.getText() + ", " + value);
            }

            //  }

        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }

    }

    public void navigateToRole() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
        //BrowserUtils.waitForClickable(roleTab, Global_VARS.TIMEOUT_MINUTE);

       // BrowserUtils.waitForClickable(roleTab, Global_VARS.TIMEOUT_MINUTE);
        try {
            if (!roleTab.isSelected()) {
                BrowserUtils.waitForClickable(roleTab, Global_VARS.TIMEOUT_MINUTE);
                base.clickTheMenu(roleTab);
            }
        } catch (Exception e) {
            System.out.println("Exception in navigating to roles tab " + e);
        }
    }

    public void checkCreatedRole() throws Exception {

    }

    public void checkForGridLoad() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForGridData(cellData, TIMEOUT_MINUTE);
    }

    public String[] filterRoleDetails(Details.ROLE_COLUMN_TYPE role_column_type, String value) throws Exception {
        String indexQuery = null;
        String resultType = null;
        WebDriver driver = CreateDriver.getInstance().getDriver();

        switch (role_column_type) {
            case roleName:
                indexQuery = "1";
                resultType = "roleName";
                break;
            case roleType:
                indexQuery = "2";
                resultType = "roleType";
                break;
            case domain:
                indexQuery = "4";
                resultType = "domain";
                break;
            case status:
                indexQuery = "4";
                resultType = "status";
                break;
        }

        String customElement = "//clr-dg-column[" + indexQuery + "1]/div/clr-dg-string-filter/clr-dg-filter/button";

        try {
            // To customize >>> base.setFilter(cellData, customElement, searchKey, value, close);
            BrowserUtils.waitForGridData(cellData, TIMEOUT_MINUTE);
            driver.findElement(By.xpath(customElement)).click();
            BrowserUtils.waitForVisibility(searchKey, TIMEOUT_MINUTE);
            if (searchKey.isDisplayed()) {
                searchKey.sendKeys(value);
                BrowserUtils.waitForGridData(cellData, TIMEOUT_MINUTE);
            }
            close.click();
            List<WebElement> filteredElements = driver.findElements(By.xpath("//clr-dg-row"));

            String[] elements = new String[filteredElements.size()];

            for (int i = 0; i < filteredElements.size(); i++) {
                if (resultType == "roleName") {
                    String filteredRecords = "//clr-dg-row[" + i + "]/div/div[2]/div/clr-dg-cell[1]/div/div[1]";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                }
                if (resultType == "roleType") {
                    String filteredRecords = "//clr-dg-row[" + i + "]/div/div[2]/div/clr-dg-cell[1]/div/div[2]";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                }
                if (resultType == "domain") {
                    String filteredRecords = "//clr-dg-row[" + i + "]/div/div[2]/div/clr-dg-cell[2]/div";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                }
                if (resultType == "status") {
                    String filteredRecords = "//clr-dg-row[" + i + "]/div/div[2]/div/clr-dg-cell[4]/record-status-component/div/div[2]/div[1]/span";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                }
                Assert.assertTrue(elements[i].contains(value));
            }
            System.out.println("Filtered results from Role is/ are " + elements);
        } catch (TimeoutException e) {
            System.out.println("Exception in filter by roles");
        }
        return new String[0];
    }

    public void deleteFilter(String key, String value, String action, String targetStatus) throws Exception {
        filterRecord(key, value);
        delete(key, value, action);
        verifyDeletion(value);
        asssertValue(key, value, targetStatus);
    }

    private void verifyDeletion(String value) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(deleteConfirmationModal, TIMEOUT_MINUTE);

        try {
            BrowserUtils.waitForVisibility(deleteConfirmationModal, TIMEOUT_MINUTE);
            if (deleteConfirmationText.getText().contains(value)) {
                System.out.println("Role deletion flow is completed " + deleteConfirmationText.getText());
            } else System.out.println("Role value not in confirmation text");

        } catch (Exception e) {
            System.out.println("Exception in verifying the deletion " + e);
        }finally {
            goToRoleSummary.click();
        }

    }

    private void delete(String key, String value, String action) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] deleteIn = action.split("_");
        System.out.println("Size of deletion data is " + deleteIn.length + ", " + deleteIn[0] + ", " + deleteIn[1]);

        try {
            if (deleteIn[0].contains("delete") && deleteIn[1].contains("summary")) {
                System.out.println(deleteIn[0] + ", " + deleteIn[1]);
                rowCellData1.click();
                BrowserUtils.waitForClickable(deleteSummary, TIMEOUT_MINUTE);
                System.out.println("Deleting the user with login id " + value + " from Summary");
                ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", deleteSummary);
                deleteSummary.click();
                proceedWithDeletion(deletePromptYes);
            } else if (deleteIn[0].contains("delete") && deleteIn[1].contains("details")) {
                BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(deleteDetails, TIMEOUT_MINUTE);
                System.out.println("Deleting the user with login id " + value + " from Details");
                deleteDetails.click();
                proceedWithDeletion(deletePromptYes);
            }

        } catch (Exception e) {
            System.out.println("Exception in deleting the filtered record " + e);

        }


    }

    private void proceedWithDeletion(M deletePromptYes) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(deletePromptYes, TIMEOUT_MINUTE);
        try {
            if (deletePrompt.getText().contains("Please confirm")) {
                String text = deletePromptMessage.getText();
                System.out.println(text);
                BrowserUtils.waitForClickable(deletePromptYes, TIMEOUT_MINUTE);
                deletePromptYes.click();
            }
        } catch (Exception e) {
            System.out.println("Exception in deleting the role " + e.getMessage());
        }
    }

    public void waitForPageLoad() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
        BrowserUtils.waitForClickable(rowCellData1, TIMEOUT_MINUTE);

    }

    public void assertCreatedData(String targetStatus, String roleName) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterCreatedRecord(roleName);
        try {
            if (!statusColAssert.getText().isEmpty()) {
                BrowserUtils.waitForVisibility(statusColAssert, TIMEOUT_MINUTE);
                Assert.assertEquals(statusColAssert.getText(), targetStatus);
                waitForPageLoad();
            }
        } catch (Exception e) {
            System.out.println("Exception in asserting the value " + e);
        }
    }

    private void filterCreatedRecord(String value) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            if (!roleTab.isSelected()) {
                navigateToRole();
                BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            }
            roleColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(value);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            //  List<WebElement> userElements = driver.findElements(By.xpath("//div/div[2]/div/clr-dg-cell[1]"));
            // for (WebElement userElement: userElements){
            BrowserUtils.waitForVisibility(roleNameElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(roleNameElement, value, TIMEOUT_MINUTE);
            if (roleNameElement.getText().contains(value)) {
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                System.out.println("Filtered the record " + value);
            } else {
                System.out.println("Not filtered");
                System.out.println(roleNameElement.getText() + ", " + value);
            }

            //  }

        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }


    }}
