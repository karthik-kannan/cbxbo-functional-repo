package pages.users;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.Base;

import java.util.List;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Create<M extends WebElement> extends Base {
    // local variables


    Base base = new Base();

    //constructor
    public Create() throws Exception {
        super();

    }

    @FindBy(xpath = "//button[@title='Create new user(s)']")
    protected M createUser;


    @FindBy(xpath = "//formly-field-typeahead-custom/ng-select/div/div/div[@role='combobox']/input")
    // > //div[@role='combobox']/input/preceding::div[contains(text(),'organisation')]
    // > //div[contains(text(),'org')]
    protected M organisation;

    @FindBy(xpath = "//div/div/div[1]/span[1]")
    protected M organisationValue;

    @FindBy(xpath = "//formly-wrapper-form-field/div/div/span")
    protected M orgId;

    @FindBy(xpath = "//formly-field/formly-group/formly-field[1]/domain-control/div/div[1]/div/ng-select/div")
    // > //div[@role='combobox']/input/preceding::div[contains(text(),'domain')]
    // > //div/ng-select/div/div/div[@role='combobox']/input
    // > //div/ng-select/div/div/div[2]
    protected M domain;

    @FindBy(xpath = "//div[@role='option']")
    protected M domainFilteredValues;

    @FindBy(xpath = "//domain-control/div/div[1]/span")
    protected M domainId;

    @FindBy(xpath = "//formly-field/formly-group/formly-field[2]/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div/div")
    // >//formly-field/formly-group/formly-field[2]/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div
    // > //div[@role='combobox']/input/preceding::div[contains(text(),'entity')]
    protected M primaryEntity;

    @FindBy(xpath = "//formly-group/formly-field[1]/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div/div")
    // > //formly-field[1]/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div
    protected M primaryAuth;

    @FindBy(xpath = "//div[@role='option']")
    protected M primaryAuthValues;

    @FindBy(xpath = "//fieldset/formly-group/formly-field[2]/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div/div")
    // > //formly-group/formly-field[2]/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div
    protected M secondaryAuth;

    @FindBy(xpath = "//div[@role='option']")
    protected M secondaryAuthValues;

    //td[2]/formly-field/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div/div/div[2]
    @FindBy(xpath = "//td[2]/formly-field/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div")
    protected M txnAuthEle;

    @FindBy(xpath = "//div[@role='option']")
    protected M txnAuthValues;

    @FindBy(xpath = "//formly-field[6]/formly-wrapper-form-field/div/div/div/formly-field-user-pref-combo/div/div/clr-select-container/div/div/select")
    protected M langRegion;

    @FindBy(xpath = "//formly-field[7]/formly-wrapper-form-field/div/div/div/formly-field-user-pref-combo/div/div/clr-select-container/div/div/select")
    protected M amountFormat;

    //formly-field[8]/formly-wrapper-form-field/div/div/div/formly-field-user-pref-combo/div/div[1]/clr-select-container/div/div/select
    @FindBy(xpath = "//formly-field[7]/formly-wrapper-form-field/div/div/div/formly-field-user-pref-combo/div/div[1]/clr-select-container/div/div/select")
    protected M dateTimeFormat;

    //formly-field[9]/formly-wrapper-form-field/div/div/div/formly-field-user-pref-combo/div/div[1]/clr-select-container/div/div/select
    @FindBy(xpath = "//formly-field[8]/formly-wrapper-form-field/div/div/div/formly-field-user-pref-combo/div/div[1]/clr-select-container/div/div/select")
    protected M refCurrency;

    @FindBy(xpath = "//span[contains(text(),'Next')]")
    protected M settingsNav;

    @FindBy(xpath = "//input[@placeholder='Enter name']")
    protected M username;

    @FindBy(xpath = "//input[@placeholder='Enter surname']")
    protected M surname;

    @FindBy(xpath = "//input[@placeholder='Enter login ID']")
    protected M loginId;

    @FindBy(xpath = "//formly-field-country-dialcode-control/ng-select/div")
    protected M mobileCountryCode;

    @FindBy(xpath = "//div[@role='option']")
    //> //ng-dropdown-panel/div/div[2]/div
    protected M mobileCountryValues;

    @FindBy(xpath = "//input[@placeholder='Enter mobile no']")
    protected M mobileValue;

    @FindBy(xpath = "//input[@placeholder='Enter email']")
    protected M emailId;

    @FindBy(xpath = "//domain-entity-role-form/div/div[2]/div/div[2]/div/div/ng-select/div/div")
    protected M role;

    @FindBy(xpath = "//div[@role='option']")
    protected M roleValues;

    @FindBy(xpath = "//formly-field[3]/formly-wrapper-form-field/div/div/div/formly-field-typeahead/ng-select/div/div")
    protected M userSecondaryAuth;

    @FindBy(xpath = "//fieldset/formly-group/formly-field[2]/formly-wrapper-form-field/div/div/div/formly-field-input/input")
    protected M primaryTokenValue;

    @FindBy(xpath = "//formly-field[4]/formly-wrapper-form-field/div/div/div/formly-field-input/input")
    protected M secondaryTokenValue;

    @FindBy(xpath = "//span[contains(text(),'Review & submit')]")
    protected M formNav;

    @FindBy(xpath = "//span[contains(text(),'Cancel')]")
    protected M cancel;

    @FindBy(xpath = "//formly-field-checkbox/div/label[contains(text(),'Add authentication for transactions')]")
    protected M txnAuthCheck;

    @FindBy(xpath = "//formly-field-checkbox/div/input")
    protected M txnAuthCheckBox;

    @FindBy(xpath = "//formly-field-authentication-country/span[3]")
    protected M authGridCountry;

    @FindBy(xpath = "//td[3]/formly-field/formly-wrapper-form-field/div/div/div/formly-field-input/input[@placeholder='Enter token Id']")
    protected M txnTokenIdEle;

    @FindBy(xpath = "//td[4]/formly-field/formly-wrapper-form-field/div/div/div/formly-wrapper-addons/formly-field-input/input[@placeholder='Enter amount']")
    protected M txnThresholdAmountEle;

    public void loadUserForm() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(createUser,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", createUser);
           // BrowserUtils.waitForClickable(createUser,TIMEOUT_MINUTE);
            createUser.click();
            BrowserUtils.waitFor("Create user",TIMEOUT_MINUTE);
            //  assertEquals(pageTitle, "Create user(s)");
            return;
        }catch(Exception e){
            System.out.println("Exception in loading the user creation form "+e);
        }
    }

    public void setGeneralSettings(String data1, String data2, String data3, String data4, String data5, String data6, String data7, String data8, String data9, String data10, String data11) throws Exception {
        setOrganisation(data1, data2);
        setDomain(data3, data4);
        setPrimaryEntity(data5);
        setPrimaryAuth(data6);
        setSecondaryAuth(data7);
        setPreferences(data8, data9, data10, data11);
    }

    private void setPreferences(String data8, String data9, String data10, String data11) throws Exception {
        setLangRegion(data8);
       // setAmountFormat(data9);
        setDateTimeFormat(data10);
        setRefCurrency(data11);
    }

    private void setRefCurrency(String data11) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(refCurrency, TIMEOUT_MINUTE);

        try{
            if (refCurrency.isDisplayed()){
                Select select = new Select(refCurrency);
                select.selectByVisibleText(data11);
            }
            System.out.println("Selected the reference currency: "+data11);
            navigateToUserInfo();

        }catch(Exception e){
            System.out.println("Exception in setting reference currency "+e);
        }
    }

    private void navigateToUserInfo() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (settingsNav.isEnabled()){
                settingsNav.click();
            }

        }catch(Exception e){
            System.out.println("Exception while navigating from General Settings "+e);
        }

        //span[contains(text(),'Next')]
    }

    private void setDateTimeFormat(String data10) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(dateTimeFormat, TIMEOUT_MINUTE);

        try{
            if (dateTimeFormat.isDisplayed()){

                Select select = new Select(dateTimeFormat);
                select.selectByVisibleText(data10);
            }
            System.out.println("Selected the Date and Time format: "+data10);

        }catch(Exception e){
            System.out.println("Exception in setting date time format "+e);
        }

    }

    private void setAmountFormat(String data9) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(amountFormat, TIMEOUT_MINUTE);

        try{
            if (amountFormat.isDisplayed()){
                Select select = new Select(amountFormat);
                select.selectByVisibleText(data9);
            }
            System.out.println("Selected the language and region: "+data9);

        }catch(Exception e){
            System.out.println("Exception in setting amount format "+e);
        }
    }

    private void setLangRegion(String data8) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(langRegion, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            js.executeScript("arguments[0].scrollIntoView();", langRegion);
            if (langRegion.isDisplayed()){
                Select select = new Select(langRegion);
                select.selectByVisibleText(data8);
             }
            System.out.println("Selected the language and region: "+data8);
        }catch(Exception e){
            System.out.println("Exception in setting language and region "+e);
        }
    }

    public void setOrganisation(String data1, String data2) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
            if (data1 != null) {
                BrowserUtils.waitForClickable(organisation, TIMEOUT_MINUTE);
                base.clickAndEnter(organisation, data1);
                BrowserUtils.waitForClickable(organisationValue, TIMEOUT_ELEMENT);
                base.selectFilteredValue(organisationValue);
                assertNotNull(orgId.getText());

                System.out.println("The Organisation ID for the selected Organisation is " + orgId.getText());
                Actions builder = new Actions(driver);
                builder.sendKeys(Keys.TAB).build().perform();
                builder.sendKeys(Keys.TAB).build().perform();

            }
        } catch (Exception e) {
            System.out.println("Exception in setting organisation "+e);
        }
    }

    public void setDomain(String data3, String data4) {
            WebDriver driver = CreateDriver.getInstance().getDriver();
        List<WebElement> domainValues = driver.findElements(By.xpath("//ng-dropdown-panel/div/div[2]/div/div/div[1]"));
        JavascriptExecutor js = (JavascriptExecutor) driver;

            try{
                if (data3 != null){
                    BrowserUtils.waitForClickable(domain, TIMEOUT_MINUTE);
                    js.executeScript("arguments[0].click();",domain);

                    domain.click();
                    for (WebElement domainValue : domainValues){

                        if (domainValue.getText().contains(data3)){
                            domainValue.sendKeys(Keys.ENTER);
                        }
                    }

                    BrowserUtils.waitForVisibility(domainFilteredValues, TIMEOUT_MINUTE);
                    List<WebElement> elements = driver.findElements(By.xpath("//div[@role='option']"));
                    System.out.println(elements.size());
                    for (int i=0;i<elements.size();i++){
                        if (elements.get(i).getText().contains(data3)){
                            elements.get(i).click();
                        }
                    }
                    base.selectFilteredValue(domainFilteredValues);
                    assertNotNull(domainId.getText());
                    }
            }catch (Exception e){
                System.out.println("Exception in setting domain "+e);
            }

        }

    public void setPrimaryEntity(String data5) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(primaryEntity, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", primaryEntity);
            if (primaryEntity.isDisplayed()){
                primaryEntity.click();
                List<WebElement> elements = driver.findElements(By.xpath("//div[@class='ng-dropdown-panel-items scroll-host']/div/div[@role='option']"));

                for (int i=0;i<elements.size();i++){
                    if (elements.get(i).getText().contains(data5)){
                        elements.get(i).click();
                    }
                    System.out.println("Selected the primary entity: "+data5);
                }
            }
         }catch(Exception e){
            System.out.println("Exception in setting primary entity "+e);
        }


    }

    public void setPrimaryAuth(String data6) throws Exception{

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(primaryAuth, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", primaryAuth);
            if (primaryAuth.isDisplayed()){
                primaryAuth.click();
                BrowserUtils.waitForVisibility(primaryAuthValues, TIMEOUT_MINUTE);
                List<WebElement> elements = driver.findElements(By.xpath("//div[@role='option']"));
                for (int i=0;i<elements.size();i++){
                    if (elements.get(i).getText().contains(data6)){
                        elements.get(i).click();
                    }
                    System.out.println("Selected the primary authentication: "+data6);
                }
            }
        }catch(Exception e){
            System.out.println("Exception in setting primary authentication "+e);
        }
    }

    private void setSecondaryAuth(String data7) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(secondaryAuth, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", secondaryAuth);
            if (secondaryAuth.isDisplayed()){
                secondaryAuth.click();
                BrowserUtils.waitForVisibility(secondaryAuthValues, TIMEOUT_MINUTE);
                List<WebElement> elements = driver.findElements(By.xpath("//div[@role='option']"));
                for (int i=0;i<elements.size();i++){
                    if (elements.get(i).getText().contains(data7)){
                        elements.get(i).click();
                    }
                    System.out.println("Selected the secondary authentication: "+data7);
                }
            }
        }catch(Exception e){
            System.out.println("Exception in setting secondary authentication "+e);
        }
    }

    public void setUserInfo(String data12, String data13, String data14, String data15, String data16, String data17, String data18, String data19, String data20, String data21) throws Exception {
        setUsername(data12);
        setSurname(data13);
        setLoginId(data14);
        setMobileNumber(data15, data16);
        setEmail(data17);
        setRole(data18);
        setSignInValues(data19, data20, data21);
    }

    private void setSignInValues(String data19, String data20, String data21) throws Exception {
        setUserPrimaryToken(data19);
        setUserSecondaryToken(data20, data21);
        
    }

    private void setUserSecondaryToken(String data20, String data21) throws Exception {
        changeSecondaryAuth(data20);

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(secondaryTokenValue, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",secondaryTokenValue);
            if (secondaryTokenValue.isDisplayed()){
                secondaryTokenValue.sendKeys(data21);
                //navigateToReview();
             }
            }catch(Exception e){
            System.out.println("Exception in setting the secondary token "+e);
        }

    }

    private void navigateToReview() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(formNav,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView(true);",formNav);
            if (formNav.isEnabled()){
                formNav.click();
                System.out.println("Entered the user details and navigated to Review");
            }

        }catch(Exception e){
            System.out.println("Exception while navigating from User form "+e);
        }



    }

    private void changeSecondaryAuth(String data20) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(userSecondaryAuth, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", userSecondaryAuth);
            if (userSecondaryAuth.isDisplayed()){
                userSecondaryAuth.click();
                BrowserUtils.waitForVisibility(secondaryAuthValues, TIMEOUT_MINUTE);
                List<WebElement> elements = driver.findElements(By.xpath("//div[@role='option']"));
                System.out.println("Count of Secondary Authentication values: "+elements.size());
                for (int i=0;i<elements.size();i++){
                    if (elements.get(i).getText().contains(data20)){
                        elements.get(i).click();
                    }
                    System.out.println("Selected the secondary authentication: "+data20);
                }
            }
        }catch(Exception e){
            System.out.println("Exception in setting secondary authentication "+e);
        }

    }

    private void setUserPrimaryToken(String data19) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(primaryTokenValue, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",primaryTokenValue);
            if (primaryTokenValue.isDisplayed()){
                primaryTokenValue.sendKeys(data19);
            }
            System.out.println("Entered the value in primary token: "+data19);
        }catch(Exception e){
            System.out.println("Exception in setting the primary token "+e);
        }

}

    private void setRole(String data18) throws Exception {


        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(role, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", role);
            if (role.isDisplayed()){
                role.click();
                BrowserUtils.waitForVisibility(roleValues, TIMEOUT_MINUTE);
                List<WebElement> elements = driver.findElements(By.xpath("//div[@role='option']"));
                for (int i=0;i<elements.size();i++){
                    if (elements.get(i).getText().contains(data18)){
                        elements.get(i).click();
                    }
                    System.out.println("Selected the primary authentication: "+data18);
                }
            }
        }catch(Exception e){
            System.out.println("Exception in setting primary authentication "+e);
        }

    }

    private void setEmail(String data17) throws Exception {


        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(emailId, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",emailId);
            if (emailId.isDisplayed()){
                emailId.sendKeys(data17);
            }
            System.out.println("Entered the value in email: "+data17);

        }catch(Exception e){
            System.out.println("Exception in setting the email "+e);
        }




    }

    private void setMobileNumber(String data15, String data16) throws Exception {
        setMobileCode(data15);
        setMobileValue(data16);
    }

    private void setMobileValue(String data16) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(mobileValue, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",mobileValue);
            if (mobileValue.isDisplayed()){
                mobileValue.sendKeys(data16);
            }
            System.out.println("Entered the value in mobile number: "+data16);

        }catch(Exception e){
            System.out.println("Exception in setting the mobile number "+e);
        }
    }

    private void setMobileCode(String data15) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(mobileCountryCode, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", mobileCountryCode);
            if (mobileCountryCode.isDisplayed()){
                mobileCountryCode.click();
                BrowserUtils.waitForVisibility(mobileCountryValues, TIMEOUT_MINUTE);
                List<WebElement> elements = driver.findElements(By.xpath("//div[@role='option']"));

                for (int i=0;i<elements.size();i++){
                     if (elements.get(i).getText().contains(data15)){
                        elements.get(i).click();
                    }
                    System.out.println("Selected the country code: "+data15);
                }
            }
        }catch(Exception e){
            System.out.println("Exception in setting primary authentication "+e);
        }

    }

    private void setLoginId(String data14) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(loginId, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",loginId);
            if (loginId.isDisplayed()){
                loginId.sendKeys(data14);
            }
            System.out.println("Entered the value in loginId: "+data14);

        }catch(Exception e){
            System.out.println("Exception in setting the loginId "+e);
        }


    }

    private void setSurname(String data13) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(surname, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",surname);
            if (surname.isDisplayed()){
                surname.sendKeys(data13);
            }
            System.out.println("Entered the value in surname: "+data13);

        }catch(Exception e){
            System.out.println("Exception in setting the surname "+e);
        }

    }

    public void setUsername(String data12) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(username, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",username);
            if (username.isDisplayed()){
                username.sendKeys(data12);
            }
            System.out.println("Entered the value in username: "+data12);

        }catch(Exception e){
            System.out.println("Exception in setting the username "+e);
}
    }

    public void setTransactionAuthentication(String txnAuthType, String txnAuthToken, String txnAuthThresholdAmount) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(txnAuthCheck, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();",txnAuthCheck);
            if (txnAuthCheck.isEnabled()){
                System.out.println("The element txnAuthCheck is displayed and available for click");
                txnAuthCheck.click();
                if (txnAuthCheckBox.isSelected()){
                    System.out.println("The element txnAuthCheck is selected");
                }else
                    System.out.println("The element txnAuthCheck is not selected");
            }else {
                System.out.println("The element txnAuthCheck is not enabled to be clicked");
            }
            System.out.println("Selected for transaction authentication");

        }catch(Exception e){
            System.out.println("Exception in selecting the authentication grid "+e);
        }finally {
            BrowserUtils.waitForVisibility(authGridCountry, TIMEOUT_MINUTE);
            setAuthDetails(txnAuthType, txnAuthToken, txnAuthThresholdAmount);
        }

    }

    private void setAuthDetails(String txnAuthType, String txnAuthToken, String txnAuthThresholdAmount) throws Exception {
       setAuthenticationType(txnAuthType);
       setAuthenticationTokenId(txnAuthToken);
       setAuthenticationThresholdamount(txnAuthThresholdAmount);
    }

    private void setAuthenticationThresholdamount(String txnAuthThresholdAmount) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(txnThresholdAmountEle, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", txnThresholdAmountEle);
            if (txnThresholdAmountEle.isDisplayed()){
                txnThresholdAmountEle.sendKeys(txnAuthThresholdAmount);
                System.out.println("Entered the Threshold Amount for authentication");
                System.out.println("Navigating to the user details page");
                navigateToReview();
            }
        }catch(Exception e){
            System.out.println("Exception in setting authentication threshold amount "+e);
        }

    }

    private void setAuthenticationTokenId(String txnAuthToken) throws Exception {


        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(txnTokenIdEle, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            js.executeScript("arguments[0].scrollIntoView();", txnTokenIdEle);
            if (txnTokenIdEle.isDisplayed()){
                txnTokenIdEle.sendKeys(txnAuthToken);
                System.out.println("Entered the Token Id for authentication");
            }
        }catch(Exception e){
            System.out.println("Exception in setting authentication token id "+e);
        }



    }

    private void setAuthenticationType(String txnAuthType) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(authGridCountry, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try{
            if (!authGridCountry.getText().isEmpty()){
                System.out.println("The country is listed in authentication grid. Value is: "+authGridCountry.getText());

                js.executeScript("arguments[0].scrollIntoView();", txnAuthEle);
                if (txnAuthEle.isDisplayed()){
                    System.out.println("The element txnAuthEle is displayed and available for selection");
                    txnAuthEle.click();
                    BrowserUtils.waitForVisibility(txnAuthValues, TIMEOUT_MINUTE);
                    List<WebElement> elements = driver.findElements(By.xpath("//div[@role='option']"));
                    System.out.println("Count of Transaction Authentication values: "+elements.size());
                    for (int i=0;i<elements.size();i++){
                        if (elements.get(i).getText().contains(txnAuthType)){
                            System.out.println("The element txnAuthValues is loaded and identified target value: "+txnAuthType);
                            elements.get(i).click();
                        }
                        System.out.println("Selected the transaction authentication type: "+txnAuthType);
                    }
                }else {
                    System.out.println("The txnAuthEle element is not displayed");
                }

            }
        }catch(Exception e){
            System.out.println("Exception in setting authentication type"+e);
        }


    }
}
