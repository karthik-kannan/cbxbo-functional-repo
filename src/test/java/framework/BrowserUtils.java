package framework;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.*;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BrowserUtils {

    /**
     * waitFor method to poll page title
     * @param title
     * @param timer
     * @throws Exception
     */
    public static void waitFor(String title, int timer) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebDriverWait exists = new WebDriverWait(driver, timer);

        exists.until(ExpectedConditions.refreshed(ExpectedConditions.titleContains(title)));

    }

    /**
     * waitFor method to poll page title
     * @param element
     * @param timer
     * @throws Exception
     */
    public static void waitForGridData(WebElement element,int timer) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebDriverWait exists = new WebDriverWait(driver, timer);

        exists.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
       }

    /**
     * waitForGridDataLoad method to poll grid data is loaded based on filter
     * @param element
     * @param timer
     * @throws Exception
     */
    public static void waitForGridDataLoad(WebElement element, String value, int timer) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebDriverWait exists = new WebDriverWait(driver, timer);

        exists.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.textToBePresentInElement(element,value)));
        exists.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));

    }

    /**
     * waitFor method to poll page URL
     * @param url
     * @param timer
     * @throws Exception
     */
    public static void waitForURL(String url, int timer) throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebDriverWait exists = new WebDriverWait(driver, timer);

        exists.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.urlContains(url)));
    }

    /**
     * waitForClickable method to poll for clickable
     *
     * @param element
     * @param timer
     * @throws Exception
     * @return
     */
    public static WebElement waitForClickable(WebElement element, int timer) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebDriverWait exists = new WebDriverWait(driver, timer);

      try{
          exists.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
          exists.ignoring(NoSuchElementException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
          //exists.ignoring(TimeoutException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));

          //exists.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
          return element;
      }catch (Exception e){
          System.out.println("Exception in waitForClickable: "+e);
          System.out.println(e.getMessage());
          System.out.println("Cause: "+e.getCause());
      }
        return element;
    }

    /**
     * waitForClickable method to poll for clickable
     *
     * @param element
     * @param timer
     * @throws Exception
     * @return
     */
    public static WebElement waitForFluentClickable(WebElement element, int timer) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
           Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                   .withTimeout(Global_VARS.TIMEOUT_MINUTE, TimeUnit.SECONDS)
                   .pollingEvery(Global_VARS.TIMEOUT_ELEMENT,TimeUnit.SECONDS)
                   .ignoring(NoSuchElementException.class);

            return element;
        }catch (Exception e){
            System.out.println("Exception in waitForClickable: "+e);
            System.out.println(e.getMessage());
            System.out.println("Cause: "+e.getCause());
        }
        return element;
    }

    public static void waitAndClick(WebElement element, int timer) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebDriverWait wait = new WebDriverWait(driver, timer);
        ExpectedCondition<Boolean> elementIsClickable = arg0 -> {
            try {
                element.click();
                return true;
            } catch (Exception e) {
                return false;
            }
        };
        wait.until(elementIsClickable);
    }

    /**
     * waitForVisibility method to poll for visibility
     *
     * @param element
     * @param timer
     * @throws Exception
     */
    public static void  waitForVisibility(WebElement element, int timer) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebDriverWait exists = new WebDriverWait(driver, timer);
        exists.ignoring(TimeoutException.class).until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
        exists.ignoring(NoSuchElementException.class).until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
        exists.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));
    }

    /**
     * click method using JavaScript API click
     *
     * @param by
     * @throws Exception
     */
    public static void click(By by) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        WebElement element = driver.findElement(by);

        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", element);
    }

    /**
     * setFocusOnElement method using JavaScript API click
     *
     * @param element
     * @throws Exception
     */
    public static void setFocusOnElement(WebElement element) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        //  WebElement element = driver.findElement(element);

        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", element);
    }

    /**
     *  lookupMessage method toretrieve confirmation/ error message using the code
     *
     * @param propFilePath
     * @param code
     * @return String
     * @throws Exception
     */
    public static String lookupMessage(String propFilePath, String code) throws Exception{
        Properties props = new Properties();
        props.load(new FileInputStream(propFilePath));
        String expectedMessage = props.getProperty(code, null);

        if (expectedMessage != null ){
            return expectedMessage;
        } else {
            throw new Exception("ERROR: The code '" +code + "' is not availabe");
        }
    }

    public static String screenShot(ITestResult result) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        DateFormat stamp = new SimpleDateFormat("MM.dd.yy.HH.mm.ss");
        Date date = new Date();
        ITestNGMethod method = result.getMethod();
        String testName = method.getMethodName();

        return captureScreen(testName+"_"+stamp.format(date)+".png");
    }


    private static String captureScreen(String fileName) throws Exception {
        String bitmapPath = "test-output\\screens\\";
        WebDriver driver = CreateDriver.getInstance().getDriver();
        File screen = null;

        if (Global_VARS.DEF_ENVIRONMENT.equalsIgnoreCase("remote")) {
            //Casting to Augmenter class for RemoteWebDriver
            screen = ((TakesScreenshot) new Augmenter().augment(driver)).getScreenshotAs(OutputType.FILE);
        }else {
            screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        }
        FileUtils.copyFile(screen, new File(bitmapPath+fileName));
        return fileName;

    }
}