package pages.limits;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

public class Details<M extends WebElement> extends Base {
    // local variables

    // constructor
    public Details() throws Exception {
        super();
    }

    // elements
    @FindBy(xpath = "//button[contains(text(),'Create a new limit')]")
    protected M createLimits;
}
