package pages.roles;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.json.simple.JSONArray;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.Base;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Create<M extends WebElement> extends Base {

    Base base = new Base();
    // local variables

    // constructor
    public Create() throws Exception {
        super();

    }

    // elements
    @FindBy(xpath = "//button[contains(text(),'Create a new role')]")
    protected M createRole;

    @FindBy(xpath = "//div/h1[contains(text(),'Create role')]")
    protected M pageTitle;

    ////div[contains(text(),'Select domain')]
    ////ng-select/div/div/div[1]
    @FindBy(xpath = "//div[@role='combobox']/input")
    protected M domainKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]/span")
    protected M domainValue;

    @FindBy(xpath = "//domain-control/div/div[1]/span")
    protected M domainId;

    @FindBy(xpath = "//input[@placeholder='Enter role name']")
    protected M roleName;

    //formly-field[3]/formly-wrapper-form-field/div/div/div/formly-field-select/select
    @FindBy(xpath = "//select")
    protected M roleTypes;

    @FindBy(xpath = "//label[contains(text(),'Web')]")
    protected M channelWeb;

    @FindBy(xpath = "//label[contains(text(),'Mobile')]")
    protected M channelMobile;

    @FindBy(xpath = "//button[@name='next']")
    protected M domainNext;

    @FindBy(xpath = "//span[contains(text(),'cancel')]")
    protected M productCancel;

    @FindBy(xpath = "//button[@name='next']")
    protected M productNext;

    @FindBy(xpath = "//button[@name='next']")
    protected M accountNext;

    @FindBy(xpath = "//clr-dg-cell/div")
    protected M linkAccountCellData;

    @FindBy(xpath = "//div[@class='ui-criteria-footer ng-star-inserted']")
    protected M accountGridSummary;

    @FindBy(xpath = "//div[@class='ui-criteria-footer ng-star-inserted']")
    protected M virtualaccountGridSummary;

    @FindBy(xpath = "//div[@class='ui-criteria-footer ng-star-inserted']")
    protected M entityGridSummary;

    @FindBy(xpath = "//button/span[contains(text(),'Review & Submit')]")
    protected M reviewSubmit;

  //abstract methods implementation

    //common methods
    public void loadRoleForm() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(createRole,TIMEOUT_MINUTE);
            createRole.click();
            BrowserUtils.waitFor("Create role",TIMEOUT_MINUTE);
          //  assertEquals(pageTitle, "Create role");
            return;
        }catch(Exception e){
            System.out.println("Exception in loading the role creation form");
        }
    }


    public void setDomain(String data) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(data!=null){
                BrowserUtils.waitForClickable(domainKey,TIMEOUT_MINUTE);
                base.clickAndEnter(domainKey, data);
                BrowserUtils.waitForClickable(domainValue,TIMEOUT_ELEMENT);
                base.selectFilteredValue(domainValue);
                assertNotNull(domainId.getText());
                System.out.println("The Domain ID for the selected Domain is "+domainId.getText());
            }
        }catch(Exception e){
            System.out.println("Exception in setting domain"+e);
        }
  }

    public void setRoleName(String data) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(data!=null){
                BrowserUtils.waitForClickable(roleName,TIMEOUT_ELEMENT);
                base.clickAndEnter(roleName, data);
                //assertEquals(roleName.getText(), data);
            }
        }catch (Exception e){
            System.out.println("Exception in setting role name");
        }

    }

    public void setRoleType(String data) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(roleTypes, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(roleTypes, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", roleTypes);

           if (roleTypes.isDisplayed()){
               Select select = new Select(roleTypes);
               select.selectByVisibleText(data);
            }
            System.out.println("Selected the role type: "+data);
        }catch (Exception e){
            System.out.println("Exception in setting role type "+e);

        }
    }

    public void setChannels(JSONArray data) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            for (int i=0;i<data.size();i++){
                String channelQuery = "//label[contains(text(),'\"+ data.get(i) +\"')]";
                WebElement channelElement = driver.findElement(By.xpath(channelQuery));
                base.selectOptions(channelElement);
            }

        }catch(Exception e){

        }


    }


    /**
     * selectValue method to select the filtered value based on entered key
     *
     */
    public void selectValue(String domain) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            driver.findElement((By) domainValue);
        }catch (Exception e){

        }
    }

    public void setDomainInfo(String domain, String roleName, String roleType, JSONArray data4) throws Exception {
        setDomain(domain);
        setRoleName(roleName);
        setRoleType(roleType);
        setChannels(data4);
        saveDomainNav();
    }


    public void saveDomainNav() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(domainNext,TIMEOUT_MINUTE);
            if(domainNext.isDisplayed()) {
                domainNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception is saving the domain info");
        }


    }

    public void setProductInfoTemp(JSONArray data) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        List<WebElement> prodelements = driver.findElements(By.xpath("//clr-tree-node/div[@role='treeitem']"));

        try{
            for (int i=0; i<data.size(); i++){
                System.out.println("Checking for data value from index "+data.get(i).toString());
                for (WebElement prodelement : prodelements){
                    System.out.println(prodelement.getText());
                    if (prodelement.getText().contains(data.get(i).toString())){
                        prodelement.sendKeys(Keys.ENTER);
                    }
                }
            }
            saveProductNav();
        }catch(Exception e){
            System.out.println("Exception in product selection "+e);
        }
    }

    public void setProductInfo(JSONArray data) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            for (int i=0;i<data.size();i++){
                System.out.println(data.get(i).toString());
                }
            List<WebElement> prodelements = driver.findElements(By.xpath("//clr-tree-node/div[@role='treeitem']"));

            for (WebElement prodelement : prodelements){


                    for (int i=0;i<data.size();i++){
                        if(!data.get(i).toString().equalsIgnoreCase(prodelement.getText())){
                            System.out.println("Found match");
                        }
                                          }
                prodelement.sendKeys(Keys.ENTER);

                System.out.println("value from prodelement is "+prodelement.getText());

            }

            BrowserUtils.waitForClickable(productCancel,TIMEOUT_MINUTE);
        }catch (Exception e){
            System.out.println("Exception in setting product info");

        }
        saveProductNav();
    }

    public void saveProductNav() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(productNext,TIMEOUT_MINUTE);
            if(productNext.isDisplayed()) {
                productNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception is saving the domain info");
        }

    }

    public void setAccountInfo() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForGridData(accountGridSummary,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try {
            js.executeScript("arguments[0].scrollIntoView();", accountGridSummary);
            if (accountGridSummary.isDisplayed()) {
                String accountSelected = accountGridSummary.getText();
                if (accountSelected.contains("accounts selected")) {
                    saveAccountNav();
                }
            }

        } catch (Exception e) {
            System.out.println("Exception in setting account information "+e);

        }

    }

    public void saveAccountNav() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", accountNext);
        try{

            BrowserUtils.waitForGridData(linkAccountCellData,TIMEOUT_MINUTE);
            BrowserUtils.waitForClickable(accountNext,TIMEOUT_MINUTE);
            if(accountNext.isDisplayed()) {
                accountNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception in saving the account info"+e);
        }

    }

    public void setVirtualAccountInfo() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForGridData(virtualaccountGridSummary,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try {
            js.executeScript("arguments[0].scrollIntoView();", virtualaccountGridSummary);
            if (virtualaccountGridSummary.isDisplayed()) {
                String accountSelected = virtualaccountGridSummary.getText();
                if (accountSelected.contains("virtual accounts selected")) {
                    saveVirtualAccountNav();
                }
            }

        } catch (Exception e) {
            System.out.println("Exception in setting virtual account information "+e);

        }
}

    public void saveVirtualAccountNav() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", accountNext);
        try{

            BrowserUtils.waitForGridData(linkAccountCellData,TIMEOUT_MINUTE);
            BrowserUtils.waitForClickable(accountNext,TIMEOUT_MINUTE);
            if(accountNext.isDisplayed()) {
                accountNext.click();
            }

        }catch(Exception e){
            System.out.println("Exception in saving the account info"+e);
        }

    }

    public void setEntitiesInfo() throws Exception{

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForGridData(entityGridSummary,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        try {
            js.executeScript("arguments[0].scrollIntoView();", entityGridSummary);
            if (entityGridSummary.isDisplayed()) {
                String accountSelected = entityGridSummary.getText();
                if (accountSelected.contains("entities selected")) {
                    reviewSubmit();
                }
            }

        } catch (Exception e) {
            System.out.println("Exception in setting virtual account information "+e);

        }

    }

    private void reviewSubmit() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", reviewSubmit);
        try{

            BrowserUtils.waitForGridData(linkAccountCellData,TIMEOUT_MINUTE);
            BrowserUtils.waitForClickable(reviewSubmit,TIMEOUT_MINUTE);
            if(reviewSubmit.isDisplayed()) {
                reviewSubmit.click();
            }

        }catch(Exception e){
            System.out.println("Exception in saving the account info"+e);
        }

    }
}
