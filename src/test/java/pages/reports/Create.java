package pages.reports;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.DateUtils;
import org.json.simple.JSONArray;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.Base;

import java.util.List;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertNotNull;

public class Create<M extends WebElement> extends Base {

    // local variables
    Base base = new Base();

    //constructor
    public Create() throws Exception {
        super();

    }

    @FindBy(xpath = "//div/h1[contains(text(),'Generate report')]")
    protected M pageTitle;

    @FindBy(xpath = "//select[@placeholder='Select report']")
    protected M selectReport;

    @FindBy(xpath = "//select[@placeholder='Select format']")
    protected M selectFormat;

    @FindBy(xpath = "//formly-field[4]/formly-wrapper-form-field/div/div/div/formly-field-typeahead-custom/ng-select/div/div/div[2]/input")
    protected M organisationKey;

    @FindBy(xpath = "//div/div/div[1]/span[1]")
    protected M organisationValue;

    @FindBy(xpath = "//formly-field[12]/formly-wrapper-form-field/div/div/div/formly-field-typeahead-custom/ng-select/div/div/div[3]/input")
    protected M entityKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div")
    protected M entityValue;

    @FindBy(xpath = "//formly-field[13]/formly-wrapper-form-field/div/div/div/formly-field-typeahead-custom/ng-select/div/div/div[3]/input")
    protected M userKey;

    @FindBy(xpath = "formly-field[13]/formly-wrapper-form-field/div/div/div/formly-field-typeahead-custom/ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]")
    protected M userValue;


    @FindBy(xpath = "//formly-field[6]/formly-wrapper-form-field/div/div/div/formly-field-typeahead-custom/ng-select/div/div/div[2]/input")
    protected M domainKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]/span")
    protected M domainValue;

    @FindBy(xpath = "//select[@placeholder='Type']")
    protected M entitlementCriteriaType;

    @FindBy(xpath = "//formly-field[11]/formly-wrapper-form-field/div/div/div/formly-field-typeahead-custom/ng-select/div/div/div[2]/input")
    protected M entitlementCriteriaValueKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]")
    protected M entitlementCriteriaValueValue;

    @FindBy(xpath = "//button/span[contains(text(),'generate')]")
    protected M generateReportData;

    @FindBy(xpath = "//button[@title='Generate a new report']")
    protected M generateReport;

    @FindBy(xpath = "//clr-date-container[1]/div/div/div/button/clr-icon")
    protected M fromDateCalIcon;

    @FindBy(xpath = "//clr-date-container[2]/div/div/div/button/clr-icon")
    protected M toDateCalIcon;

    @FindBy(xpath = "//clr-daypicker/div[2]/div[1]/button[1]")
    protected M dateManager;

    @FindBy(xpath = "//div/a[contains(text(),'Edit actions')]")
    protected M editAction;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/div")
    protected M editActionModal;

    @FindBy(xpath = "//clr-datagrid/div[1]/div/div/div/div/div/div/div/div[1]/div[1]/div[1]/label")
    protected M selectAllService;

    @FindBy(xpath = "//clr-dg-row/clr-expandable-animation/div/div[2]/div/clr-dg-cell[1]")
    protected M serviceElements;

    @FindBy(xpath = "//clr-dg-footer/div[2]/div[contains(text(),'services selected')]")
    protected M serviceSelected;

    @FindBy(xpath = "//button[contains(text(),' CANCEL ')]")
    protected M actionCancel;

    @FindBy(xpath = "//button[contains(text(),' SAVE ')]")
    protected M actionSave;

    @FindBy(xpath = "//div/strong[1]")
    protected M numOfActions;

    @FindBy(xpath = "//div/strong[2]")
    protected M numOfServices;


    public void loadReportForm() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(generateReport,TIMEOUT_MINUTE);
            generateReport.click();
            BrowserUtils.waitFor("Generate report",TIMEOUT_MINUTE);
            return;
        }catch(Exception e){
            System.out.println("Exception in loading the report generation form "+e);
        }

    }

    public void generateRoleEntitlementReport(String entity, String event, String targetStatus, String reportName, String format, String organisation, String domain, String criteria, String value) throws Exception {

        selectReportToGenerate(reportName);
        setFormat(format);
        selectOrganisation(organisation);
        selectDomain(domain);
        selectCriteria(criteria);
        setCriteriaValue(value);
        generateAndVerifyReportGeneration();
    }

    private void generateAndVerifyReportGeneration() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(generateReportData,TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            js.executeScript("arguments[0].scrollIntoView(true);", generateReportData);
            if (generateReportData.isDisplayed()){
                generateReportData.click();
                System.out.println("Generated report");

            }else{
                System.out.println("Exception in report generation ");
            }

        }catch(Exception e){
            System.out.println("Exception while generating report "+e);
        }




    }



    private void setCriteriaValue(String value) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(entitlementCriteriaValueKey, TIMEOUT_MINUTE);

        try {
            if (value != null) {
                BrowserUtils.waitForClickable(entitlementCriteriaValueKey, TIMEOUT_MINUTE);
                base.clickAndEnter(entitlementCriteriaValueKey, value);
                BrowserUtils.waitForClickable(entitlementCriteriaValueValue, TIMEOUT_ELEMENT);
                base.selectFilteredValue(entitlementCriteriaValueValue);

                Actions builder = new Actions(driver);
                builder.sendKeys(Keys.TAB).build().perform();
                builder.sendKeys(Keys.TAB).build().perform();

            }
        } catch (Exception e) {
            System.out.println("Exception in entitlement criteria value "+e);
        }



    }

    private void selectCriteria(String criteria) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(entitlementCriteriaType, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(entitlementCriteriaType, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", entitlementCriteriaType);

            if (entitlementCriteriaType.isDisplayed()){
                Select select = new Select(entitlementCriteriaType);
                select.selectByVisibleText(criteria);
            }
            System.out.println("Selected the entitlement report type: "+criteria);
        }catch (Exception e){
            System.out.println("Exception in setting report type "+e);

        }


    }

    private void selectDomain(String domain) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
            if (domain != null) {
                BrowserUtils.waitForClickable(domainKey, TIMEOUT_MINUTE);
                base.clickAndEnter(domainKey, domain);
                BrowserUtils.waitForClickable(domainValue, TIMEOUT_ELEMENT);
                base.selectFilteredValue(domainValue);

                Actions builder = new Actions(driver);
                builder.sendKeys(Keys.TAB).build().perform();
                builder.sendKeys(Keys.TAB).build().perform();

            }
        } catch (Exception e) {
            System.out.println("Exception in setting domain "+e);
        }


    }

    private void selectOrganisation(String organisation) {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
            if (organisation != null) {
                BrowserUtils.waitForClickable(organisationKey, TIMEOUT_MINUTE);
                base.clickAndEnter(organisationKey, organisation);
                BrowserUtils.waitForClickable(organisationValue, TIMEOUT_ELEMENT);
                base.selectFilteredValue(organisationValue);

                Actions builder = new Actions(driver);
                builder.sendKeys(Keys.TAB).build().perform();
                builder.sendKeys(Keys.TAB).build().perform();

            }
        } catch (Exception e) {
            System.out.println("Exception in setting organisation "+e);
        }

    }

    private void setFormat(String format) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(selectFormat, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(selectFormat, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", selectFormat);

            if (selectFormat.isDisplayed()){
                Select select = new Select(selectFormat);
                select.selectByVisibleText(format);
            }
            System.out.println("Selected the "+format+" format");
        }catch (Exception e){
            System.out.println("Exception in setting report format "+e);

        }


    }

    private void selectReportToGenerate(String reportName) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(selectReport, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(selectReport, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", selectReport);

            if (selectReport.isDisplayed()){
                Select select = new Select(selectReport);
                select.selectByVisibleText(reportName);
            }
            System.out.println("Selected the report type: "+reportName);
        }catch (Exception e){
            System.out.println("Exception in setting report type "+e);

        }

    }

    public void generateUserEntitlementReport(String entity, String event, String targetStatus, String reportName, String format, String organisation, String domain, String criteria, String value) throws Exception {
        selectReportToGenerate(reportName);
        setFormat(format);
        selectOrganisation(organisation);
        selectDomain(domain);
        selectCriteria(criteria);
        setCriteriaValue(value);
        generateAndVerifyReportGeneration();
    }

    public void generateUserListReport(String entity, String event, String targetStatus, String reportName, String format, String organisation, String domain, JSONArray statuses) throws Exception {
        selectReportToGenerate(reportName);
        setFormat(format);
        selectOrganisation(organisation);
        selectDomain(domain);
        selectStatus(statuses);
        generateAndVerifyReportGeneration();
    }

    private void selectStatus(JSONArray statuses) {
        System.out.println("To implement");
    }

    public void generateCustomerProfileReport(String entity, String event, String targetStatus, String reportName, String format, String organisation, String domain) throws Exception {
        selectReportToGenerate(reportName);
        setFormat(format);
        selectOrganisation(organisation);
        selectDomain(domain);
        generateAndVerifyReportGeneration();
    }

    public void generateAuditReport(String entity, String event, String targetStatus, String reportName, String format, String user, String organisation, String domain, String fromDate, String toDate, JSONArray actions) throws Exception {
        selectReportToGenerate(reportName);
        setFormat(format);
        selectOrganisation(organisation);
        selectDomain(domain);
        selectFromDate(fromDate);
        selectToDate(toDate);
        setActions(actions);
        selectUser(user);
        generateAndVerifyReportGeneration();
    }

    private void selectUser(String user) {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
            if (user != null) {
                BrowserUtils.waitForClickable(userKey, TIMEOUT_MINUTE);
                base.clickAndEnter(userKey, user);
                BrowserUtils.waitForClickable(userValue, TIMEOUT_ELEMENT);
                base.selectFilteredValue(userValue);

                Actions builder = new Actions(driver);
                builder.sendKeys(Keys.TAB).build().perform();
                builder.sendKeys(Keys.TAB).build().perform();

            }
        } catch (Exception e) {
            System.out.println("Exception in setting organisation "+e);
        }

    }

    private void setActions(JSONArray actions) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(editAction, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try {
            js.executeScript("arguments[0].scrollIntoView(true);", editAction);
            if (editAction.isDisplayed()){
                editAction.click();
                System.out.println("Clicked on edit action to set the service for audit report");
                processService(actions);

            }else{
                System.out.println("Exception in report generation ");
            }

        } catch (Exception e) {
            System.out.println("Exception in setting to date "+e);
        }



    }

    private void processService(JSONArray actions) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (editActionModal.getText().equalsIgnoreCase("Edit actions")){
                selectAllService.click();
                List<WebElement> serviceElements = driver.findElements(By.xpath("//clr-dg-row/clr-expandable-animation/div/div[2]/div/clr-dg-cell[1]"));

                for (int i=0; i<actions.size(); i++){
                    System.out.println("Checking the input service data "+actions.get(i).toString());
                    for (WebElement service:serviceElements){
                        System.out.println("Services listed in application"+service.getText());
                        if (service.getText().contains(actions.get(i).toString())){
                            driver.findElement(By.xpath("//clr-dg-row["+i+"]/clr-expandable-animation/div/div[1]/div[1]/div/label")).click();
                        }
                    }
                }
            }

        }catch (Exception e){
            System.out.println("Excepting in setting the service "+e);
        }finally {
            if (serviceSelected.isDisplayed()){
                BrowserUtils.waitForClickable(actionSave,TIMEOUT_MINUTE);
                actionSave.click();
                BrowserUtils.waitForVisibility(numOfActions,TIMEOUT_MINUTE);
                System.out.println("Selection actions for Audit report: "+numOfActions.getText());
                System.out.println("Selection services for Audit report: "+numOfServices.getText());

            }
        }
    }

    private void selectToDate(String toDate) {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
            if (toDate != null) {
                BrowserUtils.waitForClickable(toDateCalIcon,TIMEOUT_ELEMENT);
                toDateCalIcon.click();
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                String dateTo = DateUtils.getCurrentDate();
               // String lookupToDateValue = "//tr/td/clr-day/button[@aria-label='"+dateTo+"']";
                String lookupToDateValue = "//clr-day/button[contains(text(),'"+dateTo+"')]";
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(driver.findElement(By.xpath(lookupToDateValue)),TIMEOUT_MINUTE);
                driver.findElement(By.xpath(lookupToDateValue)).click();
            }
        } catch (Exception e) {
            System.out.println("Exception in setting to date "+e);
        }



    }

    private void selectFromDate(String fromDate) {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
            if (fromDate != null) {
                BrowserUtils.waitForClickable(fromDateCalIcon,TIMEOUT_ELEMENT);
                fromDateCalIcon.click();
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                String dateFrom = DateUtils.getCurrentDate();
                //String lookupFromDateValue = "//tr/td/clr-day/button[@aria-label='"+dateFrom+"']";
                String lookupFromDateValue = "//clr-day/button[contains(text(),'"+dateFrom+"')]";
                BrowserUtils.waitForClickable(dateManager,TIMEOUT_MINUTE);
                BrowserUtils.waitForClickable(driver.findElement(By.xpath(lookupFromDateValue)),TIMEOUT_MINUTE);
                driver.findElement(By.xpath(lookupFromDateValue)).click();
            }
        } catch (Exception e) {
            System.out.println("Exception in setting from date "+e);
        }



    }

    public void generateCustomerUserAuditReport(String entity, String event, String targetStatus, String reportName, String format, String organisation, String domain, String customerEntity, String user, String fromDate, String toDate, JSONArray functions, JSONArray transactionStatus) throws Exception {
        selectReportToGenerate(reportName);
        setFormat(format);
        selectOrganisation(organisation);
        selectDomain(domain);
        selectCustomerEntity(entity);
        selectUser(user);
        selectFromDate(fromDate);
        selectToDate(toDate);
        setFunctions(functions);
        setTransactionStatus(transactionStatus);
        generateAndVerifyReportGeneration();
    }

    private void setTransactionStatus(JSONArray transactionStatus) {
        System.out.println("To implement this optional step setTransactionStatus");
    }

    private void setFunctions(JSONArray functions) {
        System.out.println("To implement this default selected step");
    }

    private void selectCustomerEntity(String entity) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();

        try {
            if (entity != null) {
                BrowserUtils.waitForClickable(entityKey, TIMEOUT_MINUTE);
                base.clickAndEnter(entityKey, entity);
                BrowserUtils.waitForClickable(entityValue, TIMEOUT_ELEMENT);
                base.selectFilteredValue(entityValue);
                doProcess(entity);

                Actions builder = new Actions(driver);
                builder.sendKeys(Keys.TAB).build().perform();
                builder.sendKeys(Keys.TAB).build().perform();

            }
        } catch (Exception e) {
            System.out.println("Exception in setting customer entity "+e);
        }


    }

    private void doProcess(String entity) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(entityValue,TIMEOUT_ELEMENT);

        try{
            List<WebElement> entityelements = driver.findElements(By.xpath("//ng-select/ng-dropdown-panel/div/div[2]/div"));

                System.out.println("Checking for entity value from input "+entity);
                for (WebElement entityelement : entityelements){
                    System.out.println(entityelement.getText());
                    if (entityelement.getText().contains(entity)){
                        entityelement.sendKeys(Keys.ENTER);
                    }
                }
        }catch (Exception e){
            System.out.println("Exception in processing the customer entity "+e);
        }

    }
    }
