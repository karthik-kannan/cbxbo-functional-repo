package pages.roles;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base {

    Base base = new Base();

    // constructor
    public Confirmation() throws Exception {
        super();

    }
    // elements
    @FindBy(xpath = "//div/h3")
    protected M modal;

    @FindBy(xpath = "//div[@class='ng-star-inserted']/span")
    protected M confirmationMessage;

    @FindBy(xpath = "//div[@class='modal-footer']/a[@class='go-back-to-roles']")
    protected M roleSummaryNav;

    @FindBy(xpath = "//div[@class='modal-footer']/button")
    protected M createAnotherRole;


    public void checkRoleSubmissionTemp(String data1, String data2) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);
        try{
            if (modal.getText().contains("Success!")){
                System.out.println("Role created successfully");
            }if (confirmationMessage.getText().contains(data1)){
                System.out.println("Created a role with name "+data1);
            }if (confirmationMessage.getText().contains(data2)){
                System.out.println("Created a role in the domain "+data2);
            }if (confirmationMessage.getText().contains("sent for approval")){
                System.out.println("Created a role and pending for approval "+"\n"+confirmationMessage.getText());
            }
            roleSummaryNav.click();



        }catch (Exception e){
            System.out.println("Exception in Confirmation Message "+e);
        }

    }

    public void checkRoleSubmission(String roleName, String domain, String targetStatus) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(confirmationMessage,TIMEOUT_MINUTE);
            if (!confirmationMessage.getText().isEmpty()){
                System.out.println("Role creation flow is completed.");
                System.out.println("Confirmation Text: "+confirmationMessage.getText());
            }else System.out.println("Create Error in Role");
            BrowserUtils.waitForClickable(roleSummaryNav,TIMEOUT_MINUTE);
            roleSummaryNav.click();

        } catch (Exception e){
            System.out.println("Exception in creating the role "+e);
        }
    }
}
