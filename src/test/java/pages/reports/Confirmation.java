package pages.reports;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;
import pages.reports.Summary;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base{

    // local variables
    Base base = new Base();
    Summary reportSummary = new Summary();

    //constructor
    public Confirmation() throws Exception {
        super();

    }

    @FindBy(xpath = "//div/h3")
    protected M confirmationModalTitle;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div")
    protected M confirmationModalText;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a[contains(text(),'Go back to reports')]")
    protected M goBackToReports;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div/strong[1]")
    protected M confirmationModalReportName;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div/strong[2]")
    protected M confirmationModalReportId;


    public void checkReportGeneration(String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(confirmationModalTitle,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(confirmationModalText,TIMEOUT_MINUTE);
            if (!confirmationModalText.getText().isEmpty()){
                System.out.println("Report generation flow is completed.");
                System.out.println("Confirmation Text: "+confirmationModalText.getText());
                String reportId = processReportData();
                BrowserUtils.waitForClickable(goBackToReports,TIMEOUT_MINUTE);
                goBackToReports.click();
                reportSummary.assertGeneratedReport(reportId, targetStatus);
            }else System.out.println("Report generation Error");


        } catch (Exception e){
            System.out.println("Exception in generating the report "+e);
        }finally {
            System.out.println("checkReportGeneration is successful");
        }

    }

    private String processReportData() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(goBackToReports, TIMEOUT_MINUTE);

        try {
            BrowserUtils.waitForVisibility(confirmationModalText, TIMEOUT_MINUTE);
            if (!confirmationModalText.getText().isEmpty()) {

                String generatedReportNameInModal = confirmationModalReportName.getText();
                String generatedReportIdInModal = confirmationModalReportId.getText();
                if (!generatedReportNameInModal.isEmpty() && !generatedReportIdInModal.isEmpty()) {
                    System.out.println("Report is generated for '" + generatedReportNameInModal + "' and the identifier is '" + generatedReportIdInModal + "'");
                    return generatedReportIdInModal;
                } else {
                    System.out.println("Could not find the report name and identifier in confirmation modal");
                }
            } else System.out.println("Report generation Error");
        } catch (Exception e) {
            System.out.println("Exception in generating the report " + e);
        }


        return null;
    }
}
