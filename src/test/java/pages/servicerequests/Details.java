package pages.servicerequests;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

public class Details<M extends WebElement> extends Base {
    // local variables
    Base base = new Base();

    // constructor
    public Details() throws Exception{
        super();
    }

    //elements
    @FindBy(xpath = "//button[@title='Complete']/span[contains(text(),'Complete')]")
    protected M completeFromDetails;

    @FindBy(xpath = "//button[@title='Reject']/span[contains(text(),'Reject')]")
    protected M rejectFromDetails;

    @FindBy(xpath = "//button[@title='Add comment']/span[contains(text(),'Add comment')]")
    protected M addComment;

    @FindBy(xpath = "//clr-textarea-container/div/div/textarea")
    protected M commentsText;

    @FindBy(xpath = "//button[contains(text(),'Add')]")
    protected M addCommentButton;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    protected M cancelCommentButton;

    @FindBy(xpath = "//service-requests-detail/div/div/div/div[1]/div[1]/span[2]")
    protected M referenceIdInDetail;

    @FindBy(xpath = "//service-requests-detail/div/div/div/div[1]/div[1]/div/span")
    protected M srqTypeInDetail;

    @FindBy(xpath = "//service-requests-detail/div/div/div/div[2]/div[3]/div[2]")
    protected M domainNameInDetail;

    @FindBy(xpath = "//service-requests-detail/div/div/div/div[2]/div[4]/div[2]")
    protected M domainIdInDetail;

    @FindBy(xpath = "service-requests-detail/div/div/div/div[2]/div[1]/div[2]")
    protected M statusInDetail;


}
