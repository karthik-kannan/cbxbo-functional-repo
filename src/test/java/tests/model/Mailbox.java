package tests.model;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Mailbox {
    private String entity, event, targetStatus, domainName, domainId, topics, subTopic, message, attachment;
    private JSONArray usersLoginId;

    public Mailbox(JSONObject object) {
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setDomainName(object.get("domainName").toString());
        setDomainId(object.get("domainId").toString());
        setTopics(object.get("topics").toString());
        setSubTopic(object.get("subTopic").toString());
        setMessage(object.get("message").toString());
        setAttachment(object.get("attachment").toString());
        setUsersLoginId((JSONArray) object.get("usersLoginId"));
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getSubTopic() {
        return subTopic;
    }

    public void setSubTopic(String subTopic) {
        this.subTopic = subTopic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public JSONArray getUsersLoginId() {
        return usersLoginId;
    }

    public void setUsersLoginId(JSONArray usersLoginId) {
        this.usersLoginId = usersLoginId;
    }

    public void assertSentNewMessage(String targetStatus) {
    }
}
