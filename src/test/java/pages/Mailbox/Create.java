package pages.Mailbox;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.json.simple.JSONArray;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_ELEMENT;
import static framework.Global_VARS.TIMEOUT_MINUTE;
import static org.junit.Assert.assertNotNull;

public class Create<M extends WebElement> extends Base {

    // local variables
    Base base = new Base();

    // constructor
    public Create() throws Exception {
        super();


    }

    // elements
    @FindBy(xpath = "//button[contains(text(),'Create a new Message')]")
    protected M createNewMailMessage;

    @FindBy(xpath = "//div[@role='combobox']/input")
    protected M domainKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]/span")
    protected M domainValue;

    @FindBy(xpath = "//domain-control/div/div[1]/span")
    protected M domainId;

    @FindBy(xpath = "//formly-field-mailbox-user-grid/a[contains(text(),' Select user(s) ')]")
    protected M selectUserLink;

    @FindBy(xpath = "//clr-dg-column[2]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M searchLoginId;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//clr-dg-row/div/div[1]/div/div/label")
    protected M rowCellData1Select;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/div")
    protected M modalTitle;

    @FindBy(xpath = "//div/input[@name='search']")
    protected M searchKey;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//clr-datagrid/div[1]/div/clr-dg-footer/div[2]/div[contains(text(),'users selected')]")
    protected M usersSelectedYes;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/button[contains(text(),'SAVE')]")
    protected M saveUsersFromModal;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/button[contains(text(),'CANCEL')]")
    protected M cancelUsersFromModal;

    @FindBy(xpath = "//formly-field-mailbox-user-grid/div")
    protected M selectedUsersInForm;

    @FindBy(xpath = "//formly-field-input/input[@placeholder='Common topics']")
    protected M commonTopics;

    @FindBy(xpath = "//select[@placeholder='Select sub-topic']")
    protected M subTopics;

    @FindBy(xpath = "//div/textarea")
    protected M messageText;

    @FindBy(xpath = "//label[contains(text(),' Add attachment ')]")
    protected M attachmentLink;

    @FindBy(xpath = "//div/h1[contains(text(),'Create message')]")
    protected M pageTitle;

    @FindBy(xpath = "//formly-field-file/div/div/div")
    protected M attachedFileRow;

    @FindBy(xpath = "//formly-field-file/div/div/div/div[1]")
    protected M attachedFileIcon;

    @FindBy(xpath = "//formly-field-file/div/div/div/div[2]/div[1]")
    protected M attachedFileName;

    @FindBy(xpath = "//formly-field-file/div/div/div/div[2]/div[2]")
    protected M attachedFileSize;

    @FindBy(xpath = "//formly-field-file/div/div/div/div[3]")
    protected M attachedFileDelete;

    @FindBy(xpath = "//button[@name='send']")
    protected M send;

    @FindBy(xpath = "//button[@name='cancel']")
    protected M cancel;


    public void loadMailMessageForm() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(createNewMailMessage,TIMEOUT_MINUTE);
            createNewMailMessage.click();
            BrowserUtils.waitFor("Messagecenter - CBX",TIMEOUT_MINUTE);
            //  assertEquals(pageTitle, "Create role");
            return;
        }catch(Exception e){
            System.out.println("Exception in loading the new message creation form");
        }


    }

    public void processNewMailCreation(String entity, String event, String targetStatus, String domainName, String domainId, String topics, String subTopic, String message, String attachment, JSONArray users) throws Exception {
        selectDomain(domainName);
        selectUsers(users);
        selectSubTopic(subTopic);
        setMessage(message);
        addAttachment(attachment);
        submitNewMessage();
    }

    private void submitNewMessage() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(send,TIMEOUT_MINUTE);
            if(send.isDisplayed()) {
                send.click();
            }

        }catch(Exception e){
            System.out.println("Exception is sending the mail to users "+e);
        }

    }

    private void addAttachment(String attachment) {
        System.out.println("To implement adding attachments");
    }

    private void setMessage(String message) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(message!=null){
                BrowserUtils.waitForVisibility(messageText,TIMEOUT_ELEMENT);
                base.clickAndEnter(messageText, message);
            }
        }catch (Exception e){
            System.out.println("Exception in setting message content to users "+e);
        }


    }

    private void selectSubTopic(String subTopic) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(subTopics, TIMEOUT_MINUTE);
        JavascriptExecutor js = (JavascriptExecutor) driver;

        try{
            BrowserUtils.waitForClickable(subTopics, TIMEOUT_MINUTE);
            js.executeScript("arguments[0].scrollIntoView(true);", subTopics);

            if (subTopics.isDisplayed()){
                Select select = new Select(subTopics);
                select.selectByVisibleText(subTopic);
            }
            System.out.println("Selected the sub topic: "+subTopic);
        }catch (Exception e){
            System.out.println("Exception in setting sub topic "+e);

        }

    }

    private void selectUsers(JSONArray users) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(users!=null && users.size() > 0){
                System.out.println("Users available for sending message. Total of "+users.size()+ " users set in data");
                BrowserUtils.waitForVisibility(selectUserLink,TIMEOUT_MINUTE);
                if (selectUserLink.isDisplayed()){
                    selectUserLink.click();
                }
                boolean userSelection = filterUsersRecord(users);
                if (userSelection){
                    System.out.println("The users are selected. Message will be sent to these users");
                }else {
                    System.out.println("Exception in selecting the users for sending the message");
                }
            }
        }catch(Exception e){
            System.out.println("Exception in selecting user "+e);
        }


    }

    private boolean filterUsersRecord(JSONArray users) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            BrowserUtils.waitForClickable(searchLoginId, TIMEOUT_MINUTE);
            if (!modalTitle.getText().isEmpty()) {
                BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
                for (int i = 0; i < users.size(); i++) {
                    searchLoginId.click();
                    BrowserUtils.waitForVisibility(searchKey, TIMEOUT_MINUTE);
                    searchKey.sendKeys(users.get(i).toString());
                    BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
                    close.click();
                    BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
                    rowCellData1Select.click();
                    BrowserUtils.waitForClickable(searchLoginId, TIMEOUT_MINUTE);
                    searchLoginId.click();
                    BrowserUtils.waitForVisibility(searchKey, TIMEOUT_MINUTE);
                    searchKey.clear();
                    BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
                    close.click();
                }
                if (usersSelectedYes.isDisplayed()) {
                    saveUsersFromModal.click();
                    if (selectedUsersInForm.isDisplayed()) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in filtering the user records " + e);

        }
        return false;
    }

    private void selectDomain(String domainName) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if(domainName!=null){
                BrowserUtils.waitForClickable(domainKey,TIMEOUT_MINUTE);
                base.clickAndEnter(domainKey, domainName);
                BrowserUtils.waitForClickable(domainValue,TIMEOUT_ELEMENT);
                base.selectFilteredValue(domainValue);
                assertNotNull(domainId.getText());
                System.out.println("The Domain ID for the selected Domain is "+domainId.getText());
            }
        }catch(Exception e){
            System.out.println("Exception in setting domain"+e);
        }

    }
}
