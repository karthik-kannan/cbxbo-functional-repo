package pages.rules;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import java.util.List;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Review<M extends WebElement> extends Base {
    // local variables
    Base base = new Base();

    // constructor
    public Review() throws Exception {
        super();

    }

    // elements
    @FindBy(xpath = "//div/h1[contains(text(),'Review & submit')]")
    protected M pageTitle;

    @FindBy(xpath = "//div[@class='ui-edit-criteria-info clr-row']/div[2]/div[1]")
    protected M ruleName;

    @FindBy(xpath = "//div[@class='ui-edit-criteria-info clr-row']/div[2]/div[2]")
    protected M product;

    @FindBy(xpath = "//div[@class='ui-edit-criteria-info clr-row']/div[4]/div[1]")
    protected M domain;

    @FindBy(xpath = "//div[@class='ui-edit-criteria-info clr-row']/div[4]/div[2]")
    protected M channels;

    @FindBy(xpath = "//div[@class='ui-edit-criteria-info clr-row']/div[2]/div[3]")
    protected M validity;

    @FindBy(xpath = "")
    protected M authRequired;

    @FindBy(xpath = "")
    protected M roleDefinition;

    @FindBy(xpath = "//div/button[contains(text(),'Account')]")
    protected M accountGridHeader;

    @FindBy(xpath = "//div[contains(text(), 'Virtual account')]")
    protected M virtualAccountGridHeader;

    @FindBy(xpath = "//button[@type='submit']")
    protected M submit;

    public void verifyRuleWithVAInfo(String ruleName, String domain, String product, String channel, String from, String to, String authRequired) throws Exception{
        verifyDefaults(ruleName, domain, product, channel, from, to, authRequired);
        verifyAuthDetails();
        verifyAccountList();
      //  verifyVirtualAccountList();
        submitNewRule();
    }

    private void submitNewRule() {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(submit,TIMEOUT_MINUTE);
            if(submit.isDisplayed()) {
                submit.click();
            }

        }catch(Exception e){
            System.out.println("Exception is submitting role");
        }

    }

    private void verifyVirtualAccountList() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(virtualAccountGridHeader,TIMEOUT_MINUTE);

        try{
            List<WebElement> accVA = driver.findElements(By.xpath("//clr-dg-footer/div"));
            for (WebElement element: accVA){
                if (element.getText().contains("virtual accounts")){
                    System.out.println("Virtual Accounts grid is loaded");
                }
                return;
            }
            }catch (Exception e){
            System.out.println("Exception "+e);
        }}

    private void verifyAccountList() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(accountGridHeader,TIMEOUT_MINUTE);

        try{
            List<WebElement> accVA = driver.findElements(By.xpath("//clr-dg-footer/div"));
            for (WebElement element: accVA){
                if (element.getText().contains("accounts")){
                    System.out.println("Accounts grid is loaded");
                }
                return;
            }
            }catch (Exception e){
            System.out.println("Exception "+e);
        }
    }

    private void verifyAuthDetails() {
    }


    private void verifyDefaults(String ruleName, String domain, String product, String channel, String from, String to, String authRequired) throws Exception {
        verifyPageTitle();
        verifyRuleInputInfo(ruleName, domain, product, channel, from, to, authRequired);
    }

    private boolean verifyRuleInputInfo(String rule, String domainData, String productData, String channelData, String from, String to, String authRequiredData) throws Exception{

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(ruleName,TIMEOUT_MINUTE);
        try{
            if (ruleName.isDisplayed()){
                boolean ruleDetail = this.ruleName.getText().contains(rule);
                return ruleDetail;
            } if (domain.isDisplayed()){
                boolean domainDetail = this.domain.getText().contains(domainData);
                return domainDetail;
            } if (product.isDisplayed()){
                boolean productDetail = product.getText().contains(productData);
                return productDetail;
            } if (channels.isDisplayed()) {
                boolean channelDetail = channels.getText().contains(channelData);
                return channelDetail;
            } if (validity.isDisplayed()) {
                boolean validityDetail = validity.getText().contains(from);
                return validityDetail;
            } if (authRequired.isDisplayed()){
                boolean authRequiredDetail = authRequired.getText().contains(authRequiredData);
                return authRequiredDetail;
            }

        }catch (Exception e){
            System.out.println("Error in Rule input details "+e);
        }
        return false;

    }


    private boolean verifyPageTitle() throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(pageTitle,TIMEOUT_MINUTE);
        try{
            boolean title = pageTitle.getText().contains("Review & Submit");
            return title;
           }catch(Exception e){
            System.out.println("Error in page title "+e);
        }
        return false;

    }
}
