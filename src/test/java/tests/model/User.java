package tests.model;

import org.json.simple.JSONObject;

public class User {

    private String entity;
    private String targetStatus;
    private String event;
    private JSONObject organisation;
    private JSONObject userPreference;
    private JSONObject userInfo;

    public JSONObject getOrganisation() {
        return organisation;
    }

    public JSONObject getUserPreference() {
        return userPreference;
    }

    public JSONObject getUserInfo() {
        return userInfo;
    }

   public User(JSONObject object) throws Exception {
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setOrganisation((JSONObject) object.get("organisation"));
        setUserPreferences((JSONObject) object.get("userPreference"));
        setUserInfo((JSONObject) object.get("userInfo"));

}

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public void setUserPreference(JSONObject userPreference) {
        this.userPreference = userPreference;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public void setUserPreferences(JSONObject userPreference) {
        this.userPreference = userPreference;
    }

    public void setOrganisation(JSONObject organisation) {
        this.organisation = organisation;
    }

    public void setUserInfo(JSONObject userInfo) {
        this.userInfo = userInfo;
    }


    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}
