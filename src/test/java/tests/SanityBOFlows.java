package tests;

import framework.*;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.Login;
import pages.users.Confirmation;
import pages.users.Create;
import pages.users.Review;
import tests.model.*;

@Listeners(TestNG_ConsoleRunner.class)
public class SanityBOFlows {
    // local vars

    private Login<WebElement> login = null;

    private pages.users.Summary<WebElement> userSummary = null;
    private pages.roles.Summary<WebElement> roleSummary = null;
    private pages.rules.Summary<WebElement> ruleSummary = null;
    private Create<WebElement> userCreate = null;
    private Review<WebElement> userReview = null;
    private Confirmation<WebElement> userConfirm = null;
    private pages.roles.Create<WebElement> roleCreate = null;
    private pages.roles.Review<WebElement> roleReview = null;
    private pages.roles.Confirmation<WebElement> roleConfirm = null;
    private pages.rules.Create<WebElement> ruleCreate = null;
    private pages.rules.Review<WebElement> ruleReview = null;
    private pages.rules.Confirmation<WebElement> ruleConfirm = null;

    private static final String DATA_FILE = "src\\data\\SanityBO.json";


    // constructor
    public SanityBOFlows() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        login.logoutApplication(Global_VARS.TARGET_RUN);
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes

        login = new Login<WebElement>();
        userSummary = new pages.users.Summary<>();
        roleSummary = new pages.roles.Summary<>();
        ruleSummary = new pages.rules.Summary<>();
        userCreate = new Create<WebElement>();
        userReview = new Review<WebElement>();
        userConfirm = new Confirmation<WebElement>();
        roleCreate = new pages.roles.Create<WebElement>();
        roleReview = new pages.roles.Review<WebElement>();
        roleConfirm = new pages.roles.Confirmation<WebElement>();
        ruleCreate = new pages.rules.Create<WebElement>();
        ruleReview = new pages.rules.Review<WebElement>();
        ruleConfirm = new pages.rules.Confirmation<WebElement>();

        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;
    }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {
    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {
    }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_roleCreate(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Role role = new Role(testData);
        String entity = role.getEntity();
        String event = role.getEvent();
        String targetStatus = role.getTargetStatus();
        String domain = role.getDomain();
        String roleName = role.getRoleName();
        String roleType = role.getRoleType();
        JSONArray channel = role.getChannel();
        JSONArray subProducts = role.getSubProduct();


        login.doContextualLogin(entity, event);
        roleSummary.navigateToRole();
        roleCreate.loadRoleForm();
        roleCreate.setDomainInfo(domain,roleName,roleType,channel);
        roleCreate.setProductInfoTemp(subProducts);
        roleCreate.setAccountInfo();
       // roleCreate.setVirtualAccountInfo();
       // roleCreate.setEntitiesInfo();
        roleReview.verifyRoleWithVAInfo(roleName,roleType,domain);
        roleConfirm.checkRoleSubmission(roleName,domain,targetStatus);
        roleSummary.assertCreatedData(targetStatus,roleName);
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc002_roleCreateApprove(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc003_userCreate(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {

        User user = new User(testData);
        String targetStatus = user.getTargetStatus();
        String entity = user.getEntity();
        String event = user.getEvent();
        String orgName = user.getOrganisation().get("Name").toString();
        String orgAlias = user.getOrganisation().get("Alias").toString();
        String domain = user.getOrganisation().get("domain").toString();
        String domainId = user.getOrganisation().get("domainId").toString();
        String entityOrg = user.getOrganisation().get("entityOrg").toString();
        String primarySignIn = user.getOrganisation().get("primarySignIn").toString();
        String secondarySignIn = user.getOrganisation().get("secondarySignIn").toString();
        String languageRegion = user.getUserPreference().get("languageRegion").toString();
        String amountFormat = user.getUserPreference().get("amountFormat").toString();
        String dateFormat = user.getUserPreference().get("dateFormat").toString();
        String timeFormat = user.getUserPreference().get("timeFormat").toString();
        String referenceCurrency = user.getUserPreference().get("referenceCurrency").toString();
        String currencyDefault = user.getUserPreference().get("currencyDefault").toString();
        String userName = user.getUserInfo().get("userName").toString();
        String surName = user.getUserInfo().get("surName").toString();
        String loginId = user.getUserInfo().get("loginId").toString();
        String loginIdOrgAlias = user.getUserInfo().get("loginIdOrgAlias").toString();
        String mobileCode = user.getUserInfo().get("mobileCode").toString();
        String mobileValue = user.getUserInfo().get("mobileValue").toString();
        String email = user.getUserInfo().get("email").toString();
        String udomain = user.getUserInfo().get("domain").toString();
        String udomainId = user.getUserInfo().get("domainId").toString();
        String udomainRole = user.getUserInfo().get("domainRole").toString();
        String uprimaryEntity = user.getUserInfo().get("primaryEntity").toString();
        String uprimarySignIn = user.getUserInfo().get("primarySignIn").toString();
        String uprimaryTokenId = user.getUserInfo().get("primaryTokenId").toString();
        String usecondarySignIn = user.getUserInfo().get("secondarySignIn").toString();
        String usecondaryTokenId = user.getUserInfo().get("secondaryTokenId").toString();


        System.out.println(
                entity+", "+targetStatus+", "+
                        event+", "+
                        orgName +", "
                        +orgAlias+", "
                        +domain +", "
                        +domainId+", "
                        +entityOrg+", "
                        +primarySignIn+", "
                        +secondarySignIn+", "
                        +languageRegion+", "
                        +amountFormat+", "+dateFormat+", "+timeFormat+", "+referenceCurrency+", "
                        +currencyDefault+", "+userName+", "+surName+", "+loginId+", "+loginIdOrgAlias+", "+mobileCode+", "
                        +mobileValue+", "+email+", "+udomain+", "+udomainId+", "+udomainRole+", "+uprimaryEntity+", "
                        +uprimarySignIn+", "+uprimaryTokenId+", "+usecondarySignIn+", "+usecondaryTokenId);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();
        userCreate.loadUserForm();
        userCreate.setGeneralSettings(orgName, orgAlias, domain, domainId, entityOrg, primarySignIn, secondarySignIn, languageRegion, amountFormat, dateFormat, referenceCurrency);
        userCreate.setUserInfo(userName, surName, loginId, mobileCode, mobileValue, email, udomainRole, uprimaryTokenId, usecondarySignIn, usecondaryTokenId);
        userReview.verifyUserDetails();
        userConfirm.checkUserSubmission(orgName);
        userSummary.assertCreatedData(targetStatus,loginId);
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc004_userCreateApprove(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {

        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc005_ruleCreate(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {

        Rule rule = new Rule(testData);
        String entity = rule.getEntity();
        String event = rule.getEvent();
        String domain = rule.getDomain();
        String ruleName = rule.getRuleName();
        String channels = rule.getChannels();
        String from = rule.getFrom();
        String to = rule.getTo();
        String workFlowType = rule.getWorkFlowType();
        String product = rule.getProduct();
        JSONArray accounts = rule.getAccounts();
        JSONArray virtualAccounts = rule.getVirtualAccounts();
        String authRequired = rule.getAuthInfo().get("required").toString();
        String roleMap = rule.getAuthInfo().get("role").toString();
        String operator = rule.getAuthInfo().get("operator").toString();
        String group = rule.getAuthInfo().get("group").toString();
        String targetStatus = rule.getTargetStatus();

        login.doContextualLogin(entity, event);
        ruleSummary.navigateToRule();
        ruleCreate.loadRuleForm();
        ruleCreate.setDomainInfo(domain,ruleName,channels,from,to,product,workFlowType);
        ruleCreate.setAccountInfo();//to add test data for selection of accounts
    //  ruleCreate.setVirtualAccountInfo(); //to add test data for selection of accounts
        ruleCreate.setAuthorisation(authRequired,roleMap,operator,group);
        ruleReview.verifyRuleWithVAInfo(ruleName,domain, product, channels, from, to, authRequired);
        ruleConfirm.checkRuleSubmission(ruleName,domain,targetStatus);
        ruleSummary.assertCreatedData(targetStatus,ruleName);
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc006_ruleCreateApprove(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc007_userDelete(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Deleteflow deleteflow = new Deleteflow(testData);

        String entity = deleteflow.getEntity();
        String event = deleteflow.getEvent();
        String key = deleteflow.getKey();
        String value = deleteflow.getValue();
        String action = deleteflow.getAction();
        String targetStatus = deleteflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.deleteFilter(key,value,action,targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc008_userDeleteApprove(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc009_roleDelete(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Deleteflow deleteflow = new Deleteflow(testData);

        String entity = deleteflow.getEntity();
        String event = deleteflow.getEvent();
        String key = deleteflow.getKey();
        String value = deleteflow.getValue();
        String action = deleteflow.getAction();
        String targetStatus = deleteflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.deleteFilter(key,value,action,targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc010_roleDeleteApprove(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc011_ruleDelete(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Deleteflow deleteflow = new Deleteflow(testData);

        String entity = deleteflow.getEntity();
        String event = deleteflow.getEvent();
        String key = deleteflow.getKey();
        String value = deleteflow.getValue();
        String action = deleteflow.getAction();
        String targetStatus = deleteflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.deleteFilter(key,value,action,targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.deleteFilter(key,value,action,targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc012_ruleDeleteApprove(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc013_disableUser(String rowID,
                                  String description,
                                  JSONObject testData) throws Exception {
        UAMflow uaMflow = new UAMflow(testData);

        String entity = uaMflow.getEntity();
        String event = uaMflow.getEvent();
        String targetStatus = uaMflow.getTargetStatus();
        String key = uaMflow.getKey();
        String value = uaMflow.getValue();
        String action = uaMflow.getAction();
        String code = uaMflow.getCode();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        String expectedText = BrowserUtils.lookupMessage(Global_VARS.CONFIRMATION_MESSAGE_CONFIG_FILE, code);
        userSummary.processUserAccessManagement(key, value, action, expectedText, targetStatus, code);
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc014_disableUserApprove(String rowID,
                                         String description,
                                         JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc015_enableUser(String rowID,
                                 String description,
                                 JSONObject testData) throws Exception {
        UAMflow uaMflow = new UAMflow(testData);

        String entity = uaMflow.getEntity();
        String event = uaMflow.getEvent();
        String targetStatus = uaMflow.getTargetStatus();
        String key = uaMflow.getKey();
        String value = uaMflow.getValue();
        String action = uaMflow.getAction();
        String code = uaMflow.getCode();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        String expectedText = BrowserUtils.lookupMessage(Global_VARS.CONFIRMATION_MESSAGE_CONFIG_FILE, code);
        userSummary.processUserAccessManagement(key, value, action, expectedText, targetStatus, code);
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc016_enableUserApprove(String rowID,
                                        String description,
                                        JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc017_closeUser(String rowID,
                                String description,
                                JSONObject testData) throws Exception {
        UAMflow uaMflow = new UAMflow(testData);

        String entity = uaMflow.getEntity();
        String event = uaMflow.getEvent();
        String targetStatus = uaMflow.getTargetStatus();
        String key = uaMflow.getKey();
        String value = uaMflow.getValue();
        String action = uaMflow.getAction();
        String code = uaMflow.getCode();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        String expectedText = BrowserUtils.lookupMessage(Global_VARS.CONFIRMATION_MESSAGE_CONFIG_FILE, code);
        userSummary.processUserAccessManagement(key, value, action, expectedText, targetStatus, code);
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc018_closeUserApprove(String rowID,
                                String description,
                                JSONObject testData) throws Exception {
        Workflow workflow = new Workflow(testData);
        String entity = workflow.getEntity();
        String event = workflow.getEvent();
        String key = workflow.getKey();
        String value = workflow.getValue();
        String action = workflow.getAction();
        String code = workflow.getCode();
        String targetStatus = workflow.getTargetStatus();

        System.out.println(entity+", "+event+", "+key+", "+value+", "+action+", "+code+", "+targetStatus);

        login.doContextualLogin(entity, event);
        userSummary.navigateToUser();

        if (entity.contains("user")){
            userSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("role")){
            roleSummary.navigateToRole();
            roleSummary.authFilter(key,value,action, targetStatus);
        }if (entity.contains("rule")){
            ruleSummary.navigateToRule();
            ruleSummary.authFilter(key,value,action, targetStatus);
        }
    }


}

