package pages.Broadcast;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Summary<M extends WebElement> extends Base {

    // local variables
    Base base = new Base();

    //constructor
    public Summary() throws Exception {
        super();

    }

    @FindBy(xpath = "//button[@title='Broadcast message']")
    protected M broadCastIcon;

    @FindBy(xpath = "//button[contains(text(),'Create a new broadcast message')]")
    protected M createBroadCastMessage;

    public void navigateToBroadcast() throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(broadCastIcon, Global_VARS.TIMEOUT_MINUTE);
        try {
            if (!broadCastIcon.isSelected()) {
                base.clickTheMenu(broadCastIcon);
                System.out.println("Clicked the Broadcast menu");
            }
        } catch (Exception e) {
            System.out.println("Exception in navigating to broadcast message tab " + e);
        }

    }

    public void assertCreatedData(String targetStatus) {}
}
