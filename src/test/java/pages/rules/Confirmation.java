package pages.rules;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base {

    // local variables
    Base base = new Base();

    // constructor
    public Confirmation() throws Exception {
        super();

    }
    // elements
    @FindBy(xpath = "//div/h3")
    protected M modal;

    @FindBy(xpath = "//div[@class='ng-star-inserted']/span")
    protected M confirmationMessage;

    @FindBy(xpath = "//div[@class='modal-footer']/a[@class='go-back-to-rules']")
    protected M ruleSummaryNav;

    @FindBy(xpath = "//div[@class='modal-footer']/button")
    protected M createAnotherRule;

    public void checkRuleSubmission(String ruleName, String domain, String targetStatus) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(confirmationMessage,TIMEOUT_MINUTE);
            if (!confirmationMessage.getText().isEmpty()){
                System.out.println("Rule creation flow is completed.");
                System.out.println("Confirmation Text: "+confirmationMessage.getText());
            }else System.out.println("Create Error in Rule");
            BrowserUtils.waitForClickable(ruleSummaryNav,TIMEOUT_MINUTE);
            ruleSummaryNav.click();

        } catch (Exception e){
            System.out.println("Exception in creating the rule "+e);
        }
    }

    private void assertCreatedData(String targetStatus) {
        WebDriver driver = CreateDriver.getInstance().getDriver();

    }
}
