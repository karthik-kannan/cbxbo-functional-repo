package tests;

import com.google.sitebricks.client.Web;
import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import framework.JSONDataProvider;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.LoginEnhance;
import pages.reports.Confirmation;
import pages.reports.Create;
import pages.reports.Summary;
import tests.model.*;

public class ReportGeneration {
    // local vars
    private LoginEnhance<WebElement> login = null;
    private pages.users.Summary userSummary = null;
    private Summary reportSummary = null;
    private Create reportCreate = null;
    private Confirmation reportConfirm = null;

    private static final String DATA_FILE = "src\\data\\GenerateReport.json";


    // constructor
    public ReportGeneration() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes

        login = new LoginEnhance<>();
        userSummary = new pages.users.Summary<WebElement>();
        reportSummary = new Summary<WebElement>();
        reportCreate = new Create<WebElement>();
        reportConfirm = new Confirmation<WebElement>();

        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;

    }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {

    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {
    }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);
        userSummary.navigateToUser();
        login.logoutTargetApp();

    }

    // test methods

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_generateRoleEntitlementReport(String rowID,
                                                    String description,
                                                    JSONObject testData) throws Exception {

        RoleEntitlementReport roleEntitlementReport = new RoleEntitlementReport(testData);
        String entity = roleEntitlementReport.getEntity();
        String event = roleEntitlementReport.getEvent();
        String targetStatus = roleEntitlementReport.getTargetStatus();
        String reportName = roleEntitlementReport.getReportName();
        String format = roleEntitlementReport.getFormat();
        String organisation = roleEntitlementReport.getReportKey().get("Organisation").toString();
        String domain = roleEntitlementReport.getReportKey().get("domain").toString();
        String criteria = roleEntitlementReport.getReportKey().get("criteria").toString();
        String value = roleEntitlementReport.getReportKey().get("value").toString();

        login.doContextualLogin(entity, event);
        reportSummary.navigateToReports();
        reportCreate.loadReportForm();

        if (reportName.contains("Entitlement report") && (criteria.contains("Role"))){
            reportCreate.generateRoleEntitlementReport(entity, event, targetStatus, reportName, format, organisation, domain, criteria, value);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }if (reportName.contains("Entitlement report") && (criteria.contains("User"))){
            reportCreate.generateUserEntitlementReport(entity, event, targetStatus, reportName, format, organisation, domain, criteria, value);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }

    }

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc002_generateUserEntitlementReport(String rowID,
                                                    String description,
                                                    JSONObject testData) throws Exception {

        UserEntitlementReport userEntitlementReport = new UserEntitlementReport(testData);
        String entity = userEntitlementReport.getEntity();
        String event = userEntitlementReport.getEvent();
        String targetStatus = userEntitlementReport.getTargetStatus();
        String reportName = userEntitlementReport.getReportName();
        String format = userEntitlementReport.getFormat();
        String organisation = userEntitlementReport.getReportKey().get("Organisation").toString();
        String domain = userEntitlementReport.getReportKey().get("domain").toString();
        String criteria = userEntitlementReport.getReportKey().get("criteria").toString();
        String value = userEntitlementReport.getReportKey().get("value").toString();

        login.doContextualLogin(entity, event);
        reportSummary.navigateToReports();
        reportCreate.loadReportForm();

        if (reportName.contains("Entitlement report") && (criteria.contains("Role"))){
            reportCreate.generateRoleEntitlementReport(entity, event, targetStatus, reportName, format, organisation, domain, criteria, value);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }if (reportName.contains("Entitlement report") && (criteria.contains("User"))){
            reportCreate.generateUserEntitlementReport(entity, event, targetStatus, reportName, format, organisation, domain, criteria, value);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }

    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc003_generateAuditReport(String rowID,
                                          String description,
                                          JSONObject testData) throws Exception {

        AuditReport auditReport = new AuditReport(testData);
        String entity = auditReport.getEntity();
        String event = auditReport.getEvent();
        String targetStatus = auditReport.getTargetStatus();
        String reportName = auditReport.getReportName();
        String format = auditReport.getFormat();
        String user = auditReport.getUser();
        String organisation = auditReport.getReportKey().get("Organisation").toString();
        String domain = auditReport.getReportKey().get("domain").toString();
        String fromDate = auditReport.getReportKey().get("fromDate").toString();
        String toDate = auditReport.getReportKey().get("toDate").toString();
        JSONArray actions = (JSONArray) auditReport.getReportKey().get("actions");

        login.doContextualLogin(entity, event);
        reportSummary.navigateToReports();
        reportCreate.loadReportForm();

        if (reportName.contains("Audit report")){
            reportCreate.generateAuditReport(entity, event, targetStatus, reportName, format, user, organisation, domain, fromDate, toDate, actions);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc004_generateUserListReport(String rowID,
                                             String description,
                                             JSONObject testData) throws Exception {

        UserListReport userlistReport = new UserListReport(testData);
        String entity = userlistReport.getEntity();
        String event = userlistReport.getEvent();
        String targetStatus = userlistReport.getTargetStatus();
        String reportName = userlistReport.getReportName();
        String format = userlistReport.getFormat();
        String organisation = userlistReport.getReportKey().get("Organisation").toString();
        String domain = userlistReport.getReportKey().get("domain").toString();
        JSONArray statuses = (JSONArray) userlistReport.getReportKey().get("status");

        login.doContextualLogin(entity, event);
        reportSummary.navigateToReports();
        reportCreate.loadReportForm();

        if (reportName.contains("User list report")){
            reportCreate.generateUserListReport(entity, event, targetStatus, reportName, format, organisation, domain, statuses);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc006_generateCustomerProfileReport(String rowID,
                                                    String description,
                                                    JSONObject testData) throws Exception {

        CustomerProfileReport customerProfileReport = new CustomerProfileReport(testData);
        String entity = customerProfileReport.getEntity();
        String event = customerProfileReport.getEvent();
        String targetStatus = customerProfileReport.getTargetStatus();
        String reportName = customerProfileReport.getReportName();
        String format = customerProfileReport.getFormat();
        String organisation = customerProfileReport.getReportKey().get("Organisation").toString();
        String domain = customerProfileReport.getReportKey().get("domain").toString();

        login.doContextualLogin(entity, event);
        reportSummary.navigateToReports();
        reportCreate.loadReportForm();

        if (reportName.contains("Customer profile report")){
            reportCreate.generateCustomerProfileReport(entity, event, targetStatus, reportName, format, organisation, domain);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }
    }

    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc005_generateCustomerUserAuditReport(String rowID,
                                             String description,
                                             JSONObject testData) throws Exception {

        CustomerUserAuditReport customerUserAuditReport = new CustomerUserAuditReport(testData);

        String entity = customerUserAuditReport.getEntity();
        String event = customerUserAuditReport.getEvent();
        String targetStatus = customerUserAuditReport.getTargetStatus();
        String reportName = customerUserAuditReport.getReportName();
        String format = customerUserAuditReport.getFormat();
        String organisation = customerUserAuditReport.getReportKey().get("Organisation").toString();
        String domain = customerUserAuditReport.getReportKey().get("domain").toString();
        String customerEntity = customerUserAuditReport.getReportKey().get("entity").toString();
        String user = customerUserAuditReport.getReportKey().get("user").toString();
        String fromDate = customerUserAuditReport.getReportKey().get("fromDate").toString();
        String toDate = customerUserAuditReport.getReportKey().get("toDate").toString();
        JSONArray functions = (JSONArray) customerUserAuditReport.getReportKey().get("functions");
        JSONArray transactionStatus = (JSONArray) customerUserAuditReport.getReportKey().get("transactionStatus");

        login.doContextualLogin(entity, event);
        reportSummary.navigateToReports();
        reportCreate.loadReportForm();

        if (reportName.contains("Customer user audit report")){
            reportCreate.generateCustomerUserAuditReport(entity, event, targetStatus, reportName, format, organisation, domain, customerEntity, user, fromDate, toDate, functions, transactionStatus);
            reportConfirm.checkReportGeneration(targetStatus);
            reportSummary.downloadGeneratedReport();
        }
    }

}
