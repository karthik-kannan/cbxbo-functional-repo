package pages.Broadcast;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base {

    Base base = new Base();
    // local variables

    // constructor
    public Confirmation() throws Exception {
        super();

    }
    //elements
    @FindBy(xpath = "//div/h3")
    protected M modal;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div[1]")
    protected M confirmationMessage;

    @FindBy(xpath = "//div[1]/strong[1]")
    protected M generatedBroadcastId;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a")
    protected M broadcastMessageSummaryNav;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//clr-dg-column[1]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M refColFilter;

    @FindBy(xpath = "//input[@name='search']")
    protected M search;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//clr-dg-row/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M refNameElement;

    @FindBy(xpath = "//button[@title='Go to message details']")
    protected M goToDetails;

    @FindBy(xpath = "//clr-dg-cell[5]/record-status-component/div/div[2]/div[1]/span")
    protected M statusColAssert;





    public void checkBroadCastMessageSubmission(String targetStatus) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(confirmationMessage,TIMEOUT_MINUTE);
            if (!confirmationMessage.getText().isEmpty()){
                System.out.println("Broadcast Message creation flow is completed.");
                System.out.println("Confirmation Text: "+confirmationMessage.getText());
                String broadcastId = generatedBroadcastId.getText();
                System.out.println("Created broadcast message identifier: "+broadcastId);
                BrowserUtils.waitForClickable(broadcastMessageSummaryNav,TIMEOUT_MINUTE);
                broadcastMessageSummaryNav.click();
                assertCreatedData(broadcastId, targetStatus);


            }else System.out.println("Create Error in Broadcast Message");



        } catch (Exception e){
            System.out.println("Exception in creating the role "+e);
        }

    }

    private void assertCreatedData(String broadcastId, String targetStatus) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterCreatedRecord(broadcastId);
        try {
            if (!statusColAssert.getText().isEmpty()) {
                BrowserUtils.waitForVisibility(statusColAssert, TIMEOUT_MINUTE);
                Assert.assertEquals(statusColAssert.getText(), targetStatus);
                waitForPageLoad();
            }
        } catch (Exception e) {
            System.out.println("Exception in asserting the value " + e);
        }

    }

    public void waitForPageLoad() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
        BrowserUtils.waitForClickable(rowCellData1, TIMEOUT_MINUTE);

    }

    private void filterCreatedRecord(String broadcastId) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);

            refColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(broadcastId);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(refNameElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(refNameElement, broadcastId, TIMEOUT_MINUTE);
            if (refNameElement.getText().contains(broadcastId)) {
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                System.out.println("Filtered the record " + broadcastId);
            } else {
                System.out.println("Not filtered");
                System.out.println(refNameElement.getText() + ", " + broadcastId);
            }

            //  }

        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }



    }
}
