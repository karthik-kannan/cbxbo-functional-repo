package tests.model;

import org.json.simple.JSONObject;

public class AuditReport {
    private String entity, event, targetStatus, reportName, format, user;
    private JSONObject reportKey;

    public AuditReport(JSONObject object) {
        setEntity(object.get("entity").toString());
        setEvent(object.get("event").toString());
        setTargetStatus(object.get("targetStatus").toString());
        setReportName(object.get("reportName").toString());
        setFormat(object.get("format").toString());
        setReportKey((JSONObject) object.get("rptKey"));
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public JSONObject getReportKey() {
        return reportKey;
    }

    public void setReportKey(JSONObject reportKey) {
        this.reportKey = reportKey;
    }


}
