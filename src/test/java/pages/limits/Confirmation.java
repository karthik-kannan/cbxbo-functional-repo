package pages.limits;

import framework.BrowserUtils;
import framework.CreateDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Confirmation<M extends WebElement> extends Base {

    //local variables

    //constructor
    public Confirmation() throws Exception{
        super();
    }

    //elements
    @FindBy(xpath = "//div/h3")
    protected M modal;

    @FindBy(xpath = "//div[@class='ng-star-inserted']/span")
    protected M confirmationMessage;

    @FindBy(xpath = "//div/a[contains(text(),'Go back to limits')]")
    protected M limitSummaryNav;

    public void checkLimitSubmission() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(modal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(confirmationMessage,TIMEOUT_MINUTE);
            if (!confirmationMessage.getText().isEmpty()){
                System.out.println("Limit creation flow is completed.");
                System.out.println("Confirmation Text: "+confirmationMessage.getText());
            }else System.out.println("Create Error in Limits");
            BrowserUtils.waitForClickable(limitSummaryNav,TIMEOUT_MINUTE);
            limitSummaryNav.click();

        } catch (Exception e){
            System.out.println("Exception in creating the limit "+e);
        }

    }
}
