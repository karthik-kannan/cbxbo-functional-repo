package pages.users;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pages.Base;

import java.util.List;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Summary<M extends WebElement> extends Base {

    // local variables

    String rejectionData = "Rejecting the record";
    private pages.roles.Summary<WebElement> roleSummary = null;
    private pages.rules.Summary<WebElement> ruleSummary = null;

    Base base = new Base();

    //constructor
    public Summary() throws Exception {
        super();

    }

    @FindBy(xpath = "//ul/li/a[contains(text(),'Users')]")
    protected M userTab;

    @FindBy(xpath = "//ul/li/a[contains(text(),'Roles')]")
    protected M roleTab;

    @FindBy(xpath = "//clr-dg-column[1]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M userColFilter;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//input[@name='search']")
    protected M search;

    @FindBy(xpath = "//div/div/button/clr-icon")
    protected M close;

    @FindBy(xpath = "//clr-dg-cell[1]/div/div[2]")
    protected M loginIdElement;

    @FindBy(xpath = "//button[@title='Approve']")
    protected M approveSummary;

    @FindBy(xpath = "//button[@title='Reject']")
    protected M rejectSummary;

    @FindBy(xpath = "//span[@title='Approve']")
    protected M approveDetails;

    @FindBy(xpath = "//span[@title='Reject']")
    protected M rejectDetails;

    @FindBy(xpath = "//button[@title='Disable']")
    protected M disableSummary;

    @FindBy(xpath = "//button[@title='Enable']")
    protected M enableSummary;

    @FindBy(xpath = "//button[@title='close']")
    protected M closeSummary;

    @FindBy(xpath = "//button[@title='REOPEN']")
    protected M reopenSummary;

    @FindBy(xpath = "//button[@title='Delete']")
    protected M deleteSummary;

    @FindBy(xpath = "//button[2]/span[contains(text(),'Delete')]")
    protected M deleteDetails;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/h2")
    protected M deletePrompt;

    @FindBy(xpath = "//div[@class='modal-body']/div")
    protected M deletePromptMessage;

    @FindBy(xpath = "//button[contains(text(),'Yes, delete')]")
    protected M deletePromptYes;

    @FindBy(xpath = "//button[contains(text(),'No, don')]")
    protected M deletePromptNo;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/h2")
    protected M deleteConfirmationModal;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div")
    protected M deleteConfirmationText;

    @FindBy(xpath = "//textarea[@name='reason']")
    protected M rejectReason;

    @FindBy(xpath = "//button/span[contains(text(),'Reject')]")
    protected M rejectSubmit;

    @FindBy(xpath = "//button[@title='Go to user detail']")
    protected M goToDetails;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a[contains(text(),'Go back to user summary')]")
    protected M goToUserSummary;

    @FindBy(xpath = "//div/h2")
    protected M authConfirmationTitle;

    @FindBy(xpath = "//div/h2")
    protected M uamConfirmationTitle;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div")
    protected M uamConfirmationText;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]")
    protected M authConfirmationText;

    ////button[contains(text(),'Go back to user summary')]
    @FindBy(xpath = "//div/a[contains(text(),'Go back to user summary')]")
    protected M goToSummary;

    @FindBy(xpath = "//button[contains(text(),'Go back to user summary')]")
    protected M goToUsers;

    @FindBy(xpath = "//clr-dg-cell[5]/record-status-component/div/div[2]/div/span")
    protected M statusColAssert;

    public void authFilter(String key, String value, String action, String targetStatus) throws Exception{

        filterRecord(key,value);
        authenticate(key,value,action);
        verifyAuthentication(value);
        asssertValue(key, value, targetStatus);

    }

    private void verifyAuthentication(String value) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(authConfirmationTitle,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(authConfirmationText,TIMEOUT_MINUTE);
            if (!authConfirmationText.getText().isEmpty()){
                System.out.println("User authentication flow is completed.");
                System.out.println("Confirmation Text: "+authConfirmationText.getText());
            }else System.out.println("Authentication Error in User");

        }catch (Exception e){
            System.out.println("Exception in verifying the authentication "+e);
        }finally {
            BrowserUtils.waitForClickable(goToSummary,TIMEOUT_MINUTE);
            goToSummary.click();
            waitForPageLoad();
        }
    }

    private void authenticate(String key, String value, String action) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] authIn = action.split("_");
        System.out.println("Size of authentication data is "+authIn.length +", "+authIn[0]+", "+authIn[1]);

        try{
            if (authIn[0].contains("authorize") && authIn[1].contains("summary")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForGridDataLoad(loginIdElement,value,TIMEOUT_MINUTE);
                rowCellData1.click();
                BrowserUtils.waitForClickable(approveSummary,TIMEOUT_MINUTE);
                System.out.println("Authorising the user with login id "+value+" from Summary");
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",approveSummary);
                approveSummary.click();
            } else if (authIn[0].contains("reject") && authIn[1].contains("summary")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForGridDataLoad(loginIdElement,value,TIMEOUT_MINUTE);
                rowCellData1.click();
                BrowserUtils.waitForClickable(rejectSummary,TIMEOUT_MINUTE);
                System.out.println("Rejecting the user with login id "+value+" from Summary");
                reject(rejectSummary);
            }else if (authIn[0].contains("authorize") && authIn[1].contains("details")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(approveDetails,TIMEOUT_MINUTE);

                System.out.println("Authorising the user with login id "+value+" from Details");
                approveDetails.click();
            }else if (authIn[0].contains("reject") && authIn[1].contains("details")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                //BrowserUtils.waitForGridDataLoad(rejectDetails,value,TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(rejectDetails,TIMEOUT_MINUTE);
                System.out.println("Rejecting the user with login id "+value+" from Details");
                reject(rejectDetails);
            }

        }catch (Exception e){
            System.out.println("Exception in authenticating the filtered record "+e);

        }
    }
    private void reject(M element) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(element,TIMEOUT_MINUTE);
        try{
            element.click();
            rejectReason.sendKeys(rejectionData);
            rejectSubmit.click();
            return;
        }catch (Exception e){
            System.out.println("Exception while rejecting the record "+e);
        }
    }

    private void filterRecord(String key, String value) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
         try{
            if (!userTab.isSelected()){
                navigateToUser();
                BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
            }
            userColFilter.click();
            BrowserUtils.waitForClickable(search,TIMEOUT_MINUTE);
            search.sendKeys(value);
            BrowserUtils.waitForClickable(close,TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
          //  List<WebElement> userElements = driver.findElements(By.xpath("//div/div[2]/div/clr-dg-cell[1]"));
           // for (WebElement userElement: userElements){
             BrowserUtils.waitForVisibility(loginIdElement,TIMEOUT_MINUTE);
             BrowserUtils.waitForGridDataLoad(loginIdElement,value,TIMEOUT_MINUTE);
                if (loginIdElement.getText().contains(value)){
                    ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                    System.out.println("Filtered the record for processing "+value);
                     }else{
                    System.out.println("Not filtered the record");
                    System.out.println(loginIdElement.getText()+", "+value);
                }

          //  }

        }catch (Exception e){
             System.out.println("Exception in filtering the record "+e);

        }
    }

    public void navigateToUser() throws Exception{
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
        //BrowserUtils.waitForClickable(userTab, TIMEOUT_MINUTE);
        try{
            if (!userTab.isSelected()){
                System.out.println("User tab is not selected. Clicking the User tab");
                BrowserUtils.waitForClickable(userTab, TIMEOUT_MINUTE);
                base.clickTheMenu(userTab);
                System.out.println("Clicked the User tab");
            }
        }catch (Exception e){
            System.out.println("Exception in navigating to users tab "+e);
        }finally {
            System.out.println("User tab is selected");
        }
    }


    public void deleteFilter(String key, String value, String action, String targetStatus) throws Exception {
        filterRecord(key,value);
        delete(key,value,action);
        verifyDeletion(value);
        asssertValue(key, value, targetStatus);

    }

    private void verifyDeletion(String value) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(deleteConfirmationModal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(deleteConfirmationModal,TIMEOUT_MINUTE);
            if (deleteConfirmationText.getText().contains(value)){
                System.out.println("User deletion flow is completed "+deleteConfirmationText.getText());
            }else System.out.println("User value not in confirmation text");

        }catch (Exception e){
            System.out.println("Exception in verifying the deletion "+e);
        }finally {
            goToUserSummary.click();
        }
    }

    private void delete(String key, String value, String action) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] deleteIn = action.split("_");
        System.out.println("Size of deletion data is "+deleteIn.length +", "+deleteIn[0]+", "+deleteIn[1]);

        try{
            if (deleteIn[0].contains("delete") && deleteIn[1].contains("summary")){
                System.out.println(deleteIn[0]+", "+deleteIn[1]);
                rowCellData1.click();
                BrowserUtils.waitForClickable(deleteSummary,TIMEOUT_MINUTE);
                System.out.println("Deleting the user with login id "+value+" from Summary");
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",deleteSummary);
                deleteSummary.click();
                proceedWithDeletion(deletePromptYes);
            }else if (deleteIn[0].contains("delete") && deleteIn[1].contains("details")){
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(deleteDetails,TIMEOUT_MINUTE);
                System.out.println("Deleting the user with login id "+value+" from Details");
                deleteDetails.click();
                proceedWithDeletion(deletePromptYes);
            }

        }catch (Exception e){
            System.out.println("Exception in deleting the filtered record "+e);

        }

    }

    private void proceedWithDeletion(M deletePromptYes) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(deletePromptYes,TIMEOUT_MINUTE);
        try{
            if (deletePrompt.getText().contains("Please confirm")){
                String text = deletePromptMessage.getText();
                System.out.println(text);
                BrowserUtils.waitForClickable(deletePromptYes,TIMEOUT_MINUTE);
                deletePromptYes.click();
            }
        }catch (Exception e){
            System.out.println("Exception in deleting the user "+e.getMessage());
        }

    }

    public void processUserAccessManagement(String key, String value, String action, String expectedText, String targetStatus, String code) throws Exception {
        getExpectedConfirmationText(code,action);
        filterRecord(key,value);
        doProcess(key,value,action, expectedText);
        asssertValue(key, value, targetStatus);
    }

    private void getExpectedConfirmationText(String code, String action) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            String expectedText = BrowserUtils.lookupMessage(Global_VARS.CONFIRMATION_MESSAGE_CONFIG_FILE, code);
            String expectedtFinalText = expectedText.replace("{action}", action);
            System.out.println(expectedtFinalText);


        }catch (Exception e){

        }

    }

    private void asssertValue(String key, String value, String targetStatus) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterRecord(key,value);
        try {
            if (!statusColAssert.getText().isEmpty()){
                BrowserUtils.waitForVisibility(statusColAssert,TIMEOUT_MINUTE);
                Assert.assertEquals(statusColAssert.getText(),targetStatus);
                System.out.println("Asserting the filtered record is completed");
                waitForPageLoad();
            }
        }catch (Exception e){
            System.out.println("Exception in asserting the value "+e);

        }

    }

    private void verifyProcess(String expectedText, String expectedEvent) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(uamConfirmationTitle,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(uamConfirmationTitle,TIMEOUT_MINUTE);
            String expectedFinalText = expectedText.replace("{action}", expectedEvent);
            if (uamConfirmationText.getText().contains(expectedFinalText)){
                System.out.println("User Access Management flow is completed.");
                System.out.println("Expected Final Text is: "+expectedFinalText);
                System.out.println("Actual Text is:  "+uamConfirmationText.getText());
            }else
                System.out.println("User value not in confirmation text");
                System.out.println("Expected Final Text is: "+expectedFinalText);
                System.out.println("Actual Text is:  "+uamConfirmationText.getText());
            goToUsers.click();
        }catch (Exception e){
            System.out.println("Exception in verifying the uam "+e);
        }

    }

    private void doProcess(String key, String value, String action, String expectedText) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] uamIn = action.split("_");
        System.out.println("Size of user access management data is "+uamIn.length +", "+uamIn[0]+", "+uamIn[1]);

        try{
            if (uamIn[0].contains("disable") && uamIn[1].contains("summary")){
                System.out.println(uamIn[0]+", "+uamIn[1]);
                rowCellData1.click();
                BrowserUtils.waitForClickable(disableSummary,TIMEOUT_MINUTE);
                System.out.println("Performing "+uamIn[0]+" action for user with login id "+value+" from "+uamIn[1]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",disableSummary);
                disableSummary.click();
                verifyProcess(expectedText, uamIn[0]);
               }else if (uamIn[0].contains("enable") && uamIn[1].contains("summary")){
                System.out.println(uamIn[0]+", "+uamIn[1]);
                rowCellData1.click();
                BrowserUtils.waitForClickable(enableSummary,TIMEOUT_MINUTE);
                System.out.println("Performing "+uamIn[0]+" action for user with login id "+value+" from "+uamIn[1]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",enableSummary);
                enableSummary.click();
                verifyProcess(expectedText, uamIn[0]);
               } else if (uamIn[0].contains("close") && uamIn[1].contains("summary")){
                System.out.println(uamIn[0]+", "+uamIn[1]);
                rowCellData1.click();
                BrowserUtils.waitForClickable(closeSummary,TIMEOUT_MINUTE);
                System.out.println("Performing "+uamIn[0]+" action for user with login id "+value+" from "+uamIn[1]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",closeSummary);
                closeSummary.click();
                verifyProcess(expectedText, uamIn[0]);
             }else if (uamIn[0].contains("reopen") && uamIn[1].contains("summary")){
                System.out.println(uamIn[0]+", "+uamIn[1]);
                rowCellData1.click();
                BrowserUtils.waitForClickable(reopenSummary,TIMEOUT_MINUTE);
                System.out.println("Performing "+uamIn[0]+" action for user with login id "+value+" from "+uamIn[1]);
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",reopenSummary);
                reopenSummary.click();
                verifyProcess(expectedText, uamIn[0]);
                }

        }catch (Exception e){
            System.out.println("Exception in processing the filtered record "+e);

        }
    }

    public void waitForPageLoad() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
        BrowserUtils.waitForClickable(rowCellData1,TIMEOUT_MINUTE);

    }

    public void navigateToTargetMenu(String entity) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(userTab,TIMEOUT_MINUTE);

        if (entity.contains("user")){
            try{
                if (!userTab.isSelected()){
                    userTab.click();
                }
            }catch (Exception e){
                System.out.println("Exception in navigating to the user tab "+e);
            }
        } else if (entity.contains("role")){
            try{
                if (!roleTab.isSelected()){
                    roleSummary.navigateToRole();
                }
            }catch (Exception e){
                System.out.println("Exception in navigating to the role tab "+e);
            }
        } else if (entity.contains("rule")){
            try{
                if (userTab.isSelected()){
                    ruleSummary.navigateToRule();
                }
            }catch (Exception e){
                System.out.println("Exception in navigating to the rule tab "+e);
            }

        }


    }

    public void processAuthentication(String entity, String key, String value, String action, String expectedText, String targetStatus, String code) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(userTab,TIMEOUT_MINUTE);

        if (entity.contains("user")){
            try{
                authFilter(key,value,action,targetStatus);
            }catch (Exception e){
                System.out.println("Exception in authentication of user "+e);
            }
           }else if (entity.contains("role")){
            try{
                roleSummary.authFilter(key,value,action,targetStatus);
            }catch (Exception e){
                System.out.println("Exception in authentication of role "+e);
            }
        } else if (entity.contains("rule")){
            try{
                ruleSummary.authFilter(key,value,action,targetStatus);
            }catch (Exception e){
                System.out.println("Exception in authentication of rule "+e);
            }
        }

    }

    public void processAuthorizationRequest(String entity, String key, String value, String action, String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (entity.contains("user")){
                authFilter(key,value,action, targetStatus);
            }if (entity.contains("role")){
                roleSummary.navigateToRole();
                roleSummary.authFilter(key,value,action, targetStatus);
            }if (entity.contains("rule")){
                ruleSummary.navigateToRule();
                ruleSummary.authFilter(key,value,action, targetStatus);
            }
        }catch (Exception e){
            System.out.println("Exception in switching tabs "+e);
        }

    }

    public void assertCreatedData(String targetStatus, String loginId) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterCreatedRecord(loginId);
        try{
            if (!statusColAssert.getText().isEmpty()){
                BrowserUtils.waitForVisibility(statusColAssert,TIMEOUT_MINUTE);
                Assert.assertEquals(statusColAssert.getText(),targetStatus);
                waitForPageLoad();
            }
        }catch (Exception e){
            System.out.println("Exception in asserting the value "+e);
        }
    }

    private void filterCreatedRecord(String value) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (!userTab.isSelected()){
                navigateToUser();
                BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
            }
            userColFilter.click();
            BrowserUtils.waitForClickable(search,TIMEOUT_MINUTE);
            search.sendKeys(value);
            BrowserUtils.waitForClickable(close,TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
            //  List<WebElement> userElements = driver.findElements(By.xpath("//div/div[2]/div/clr-dg-cell[1]"));
            // for (WebElement userElement: userElements){
            BrowserUtils.waitForVisibility(loginIdElement,TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(loginIdElement,value,TIMEOUT_MINUTE);
            if (loginIdElement.getText().contains(value)){
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                System.out.println("Filtered the record "+value);
            }else{
                System.out.println("Not filtered");
                System.out.println(loginIdElement.getText()+", "+value);
            }

            //  }

        }catch (Exception e){
            System.out.println("Exception in filtering the record "+e);

        }
    }
}

