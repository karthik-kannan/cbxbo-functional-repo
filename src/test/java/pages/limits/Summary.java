package pages.limits;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Summary<M extends WebElement> extends Base {

    //local variables
    Base base = new Base();

    //constructor
    public Summary() throws Exception{
        super();
    }

    String rejectionData = "Rejecting the record";

    //elements
    @FindBy(xpath = "//button[contains(text(),'Create a new limit')]")
    protected M createLimits;

    @FindBy(xpath = "//ul/li/a[contains(text(),'Limits')]")
    protected M limitTab;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//clr-dg-column[1]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M limitColFilter;

    @FindBy(xpath = "//input[@name='search']")
    protected M search;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//clr-dg-cell[1]/div/div[1]")
    protected M limitNameElement;

    @FindBy(xpath = "//clr-dg-cell[4]/record-status-component/div/div[2]/div/span")
    protected M statusColAssert;

    @FindBy(xpath = "//button[@title='Go to user detail']")
    protected M goToDetails;

    @FindBy(xpath = "//button[@title='Delete']")
    protected M deleteSummary;

    @FindBy(xpath = "//button[2]/span[contains(text(),'Delete')]")
    protected M deleteDetails;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/h2")
    protected M deletePrompt;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div")
    protected M deletePromptMessage;

    @FindBy(xpath = "//button[contains(text(),'Yes, delete')]")
    protected M deletePromptYes;

    @FindBy(xpath = "//button[contains(text(),' No, don')]")
    protected M deletePromptNo;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[1]/div/h2")
    protected M deleteConfirmationModal;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]/div")
    protected M deleteConfirmationText;

    @FindBy(xpath = "//button[@title='Approve']")
    protected M approveSummary;

    @FindBy(xpath = "//button[@title='Reject']")
    protected M rejectSummary;

    @FindBy(xpath = "//span[@title='Approve']")
    protected M approveDetails;

    @FindBy(xpath = "//span[@title='Reject']")
    protected M rejectDetails;

    @FindBy(xpath = "//textarea[@name='reason']")
    protected M rejectReason;

    @FindBy(xpath = "//button/span[contains(text(),'Reject')]")
    protected M rejectSubmit;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[3]/a[contains(text(),' Go back to limit summary ')]")
    protected M goToLimitSummary;

    @FindBy(xpath = "//div/h2")
    protected M authConfirmationTitle;

    @FindBy(xpath = "//clr-modal/div/div[1]/div[2]/div/div[2]")
    protected M authConfirmationText;

    public void navigateToLimits() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
        //BrowserUtils.waitForClickable(roleTab, Global_VARS.TIMEOUT_MINUTE);

        // BrowserUtils.waitForClickable(roleTab, Global_VARS.TIMEOUT_MINUTE);
        try {
            if (!limitTab.isSelected()) {
                BrowserUtils.waitForClickable(limitTab, Global_VARS.TIMEOUT_MINUTE);
                base.clickTheMenu(limitTab);
            }
        } catch (Exception e) {
            System.out.println("Exception in navigating to limits tab " + e);
        }

    }

    public void loadLimitForm() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(createLimits,TIMEOUT_MINUTE);
            createLimits.click();
            BrowserUtils.waitFor("Create limit",TIMEOUT_MINUTE);
            return;
        }catch(Exception e){
            System.out.println("Exception in loading the limit creation form");
        }

    }

    public void assertCreatedData(String targetStatus, String limitsProfileName) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterCreatedRecord(limitsProfileName);
        try {
            if (!statusColAssert.getText().isEmpty()) {
                BrowserUtils.waitForVisibility(statusColAssert, TIMEOUT_MINUTE);
                Assert.assertEquals(statusColAssert.getText(), targetStatus);
                System.out.println("Status of '"+limitsProfileName+"' is "+statusColAssert.getText());
                waitForPageLoad();
            }
        } catch (Exception e) {
            System.out.println("Exception in asserting the value " + e);
        }

    }

    private void waitForPageLoad() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
        BrowserUtils.waitForClickable(rowCellData1, TIMEOUT_MINUTE);


    }

    private void filterCreatedRecord(String value) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try {
            BrowserUtils.waitForClickable(rowCellData1, TIMEOUT_MINUTE);
            if (!limitTab.isSelected()) {
                navigateToLimits();
                BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            }
            limitColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(value);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(limitNameElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(limitNameElement, value, TIMEOUT_MINUTE);
            if (limitNameElement.getText().contains(value)) {
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                System.out.println("Filtered the record " + value);
            } else {
                System.out.println("Not filtered");
                System.out.println(limitNameElement.getText() + ", " + value);
            }

        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }



    }

    public void deleteFilter(String key, String value, String action, String targetStatus) throws Exception {

        filterRecord(key,value);
        delete(key,value,action);
        verifyDeletion(value);
        asssertValue(key, value, targetStatus);

    }

    private void asssertValue(String key, String value, String targetStatus) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        filterRecord(key,value);
        try {
            if (!statusColAssert.getText().isEmpty()){
                BrowserUtils.waitForVisibility(statusColAssert,TIMEOUT_MINUTE);
                org.testng.Assert.assertEquals(statusColAssert.getText(),targetStatus);
                System.out.println("Asserting the filtered record is completed");
                waitForPageLoad();
            }
        }catch (Exception e){
            System.out.println("Exception in asserting the value "+e);

        }


    }

    private void verifyDeletion(String value) throws Exception {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(deleteConfirmationModal,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(deleteConfirmationModal,TIMEOUT_MINUTE);
            if (deleteConfirmationText.getText().contains(value)){
                System.out.println("Role deletion flow is completed "+deleteConfirmationText.getText());
            }else System.out.println("Role value not in confirmation text");

        }catch (Exception e){
            System.out.println("Exception in verifying the deletion "+e);
        }finally {
            goToLimitSummary.click();
        }



    }

    private void delete(String key, String value, String action) {

        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] deleteIn = action.split("_");
        System.out.println("Size of deletion data is "+deleteIn.length +", "+deleteIn[0]+", "+deleteIn[1]);

        try{
            if (deleteIn[0].contains("delete") && deleteIn[1].contains("summary")){
                System.out.println(deleteIn[0]+", "+deleteIn[1]);
                rowCellData1.click();
                BrowserUtils.waitForClickable(deleteSummary,TIMEOUT_MINUTE);
                System.out.println("Deleting the user with login id "+value+" from Summary");
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",deleteSummary);
                deleteSummary.click();
                proceedWithDeletion(deletePromptYes);
            }else if (deleteIn[0].contains("delete") && deleteIn[1].contains("details")){
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(deleteDetails,TIMEOUT_MINUTE);
                System.out.println("Deleting the user with login id "+value+" from Details");
                deleteDetails.click();
                proceedWithDeletion(deletePromptYes);
            }

        }catch (Exception e){
            System.out.println("Exception in deleting the filtered record "+e);

        }




    }

    private void proceedWithDeletion(M deletePromptYes) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(deletePromptYes,TIMEOUT_MINUTE);
        try{
            if (deletePrompt.getText().contains("Please confirm")){
                String text = deletePromptMessage.getText();
                System.out.println(text);
                BrowserUtils.waitForClickable(deletePromptYes,TIMEOUT_MINUTE);
                deletePromptYes.click();
            }
        }catch (Exception e){
            System.out.println("Exception in deleting the record "+e.getMessage());
        }

    }

    private void filterRecord(String key, String value) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            if (!limitTab.isSelected()){
                navigateToLimits();
                BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
            }
            limitColFilter.click();
            BrowserUtils.waitForClickable(search,TIMEOUT_MINUTE);
            search.sendKeys(value);
            BrowserUtils.waitForClickable(close,TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(limitNameElement,TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(limitNameElement,value,TIMEOUT_MINUTE);
            if (limitNameElement.getText().contains(value)){
                System.out.println("Filtered the record for processing "+value);
            }else{
                System.out.println("Not filtered");
                System.out.println(limitNameElement.getText()+", "+value);
            }
        }catch (Exception e){
            System.out.println("Exception in filtering the record "+e);

        }



    }

    public void authFilter(String key, String value, String action, String targetStatus) throws Exception {
        filterRecord(key,value);
        authenticate(key,value,action);
        verifyAuthentication(value);
        asssertValue(key, value, targetStatus);
    }

    private void verifyAuthentication(String value) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(authConfirmationTitle,TIMEOUT_MINUTE);

        try{
            BrowserUtils.waitForVisibility(authConfirmationText,TIMEOUT_MINUTE);
            if (!authConfirmationText.getText().isEmpty()){
                System.out.println("Limit authentication flow is completed.");
                System.out.println("Confirmation Text: "+authConfirmationText.getText());
            }else System.out.println("Authentication Error in Limit");
        }catch (Exception e){
            System.out.println("Exception in verifying the authentication "+e);
        }finally {
            BrowserUtils.waitForClickable(goToLimitSummary,TIMEOUT_MINUTE);
            goToLimitSummary.click();
            waitForPageLoad();
        }
    }

    private void authenticate(String key, String value, String action) {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        String[] authIn = action.split("_");
        System.out.println("Size of authentication data is "+authIn.length +", "+authIn[0]+", "+authIn[1]);

        try{
            if (authIn[0].contains("authorize") && authIn[1].contains("summary")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForGridDataLoad(limitNameElement,value,TIMEOUT_MINUTE);
                rowCellData1.click();
                BrowserUtils.waitForClickable(approveSummary,TIMEOUT_MINUTE);
                System.out.println("Authorising the limit with limit name "+value+" from Summary");
                ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);",approveSummary);
                approveSummary.click();
            } else if (authIn[0].contains("reject") && authIn[1].contains("summary")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForGridDataLoad(limitNameElement,value,TIMEOUT_MINUTE);
                rowCellData1.click();
                BrowserUtils.waitForClickable(rejectSummary,TIMEOUT_MINUTE);
                System.out.println("Rejecting the limit with limit name "+value+" from Summary");
                reject(rejectSummary);
            }else if (authIn[0].contains("authorize") && authIn[1].contains("details")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(approveDetails,TIMEOUT_MINUTE);
                System.out.println("Authorising the limit with limit name "+value+" from Details");
                approveDetails.click();
            }else if (authIn[0].contains("reject") && authIn[1].contains("details")){
                System.out.println(authIn[0]+", "+authIn[1]);
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                goToDetails.click();
                BrowserUtils.waitForClickable(rejectDetails,TIMEOUT_MINUTE);
                System.out.println("Rejecting the limit with limit name "+value+" from Details");
                reject(rejectDetails);
            }}catch (Exception e){
            System.out.println("Exception in authenticating the filtered record "+e);
        }

    }

    private void reject(M element) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(element,TIMEOUT_MINUTE);
        try{
            element.click();
            rejectReason.sendKeys(rejectionData);
            rejectSubmit.click();
            return;
        }catch (Exception e){
            System.out.println("Exception while rejecting the record "+e);
        }


    }
}
