package framework;

import java.io.File;

public class Global_VARS {
    // browser defaults
    public static final String BROWSER = "chrome";
    public static final String PLATFORM = "Windows 10";
    //for cloud based execution, update the ENVIRONMENT to "remote"
    public static final String ENVIRONMENT = "local";
    public static String DEF_BROWSER = null;
    public static String DEF_PLATFORM = null;
    public static String DEF_ENVIRONMENT = null;

    // suite folder defaults
    public static String SUITE_NAME = null;
    public static final String TARGET_URL = "http://cbxbo-web-service-cim201.openshift-devops-ops01-ca59b22659737722f226d84b287c7ed8-0000.us-south.containers.appdomain.cloud/cbxbo/";
    public static String propFile = "src\\selenium.properties";
    public static final String SE_PROPS = new File(propFile).getAbsolutePath();
    public static final String TEST_OUTPUT_PATH = "test-output/";
    public static final String LOGFILE_PATH = TEST_OUTPUT_PATH + "Logs/";
    //public static final String REPORT_PATH = TEST_OUTPUT_PATH + "test-output\\Reports";
    public static final String REPORT_PATH = "test-output\\test-output\\Reports\\";
    public static final String REPORT_CONFIG_FILE = "src\\test\\java\\framework\\extent-config.xml";
    public static final String CONFIRMATION_MESSAGE_CONFIG_FILE = "src\\test\\confirmationMessages.properties";
    public static final String ENVIRONMENT_CONFIG_FILE = "src\\test\\environmentConfig.properties";
    public static final String TARGET_RUN = "DIT";

    // suite timeout defaults
    public static final int TIMEOUT_MINUTE = 60;
    public static final int TIMEOUT_ELEMENT = 10;

    // application login defaults
    public static final String USER_MAKER_EMAIL = "gordan.freeman@gmail.com";
    public static final String USER_CHECKER_EMAIL = "sean.maxwell@gmail.com";
    public static final String USER_PASSWORD = "Digital1234";


}
