package pages.Mailbox;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Summary<M extends WebElement> extends Base {
    // local variables
    Base base = new Base();

    // constructor
    public Summary() throws Exception {
        super();
    }

    // elements
    @FindBy(xpath = "//button[@title='Mail box']/span")
    protected M mailboxIcon;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div/div[1]")
    protected M rowCellData1;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div")
    protected M inboxrowCellData1;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div")
    protected M sentrowCellData1;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[1]/div")
    protected M archiverowCellData1;

    @FindBy(xpath = "//clr-dg-row[1]/div/div[2]/div/clr-dg-cell[2]/div")
    protected M refElementGrid;

    @FindBy(xpath = "//clr-dg-row/div/div[2]/div/clr-dg-cell[6]/div")
    protected M statusElementGrid;

    @FindBy(xpath = "//button/clr-accordion-title/div[1]/div[2]/div/div/span[2]")
    protected M messageStatus;

    @FindBy(xpath = "//nav/button/span[2]")
    protected M backFromDetails;

    @FindBy(xpath = "//nav/button/span[2]")
    protected M backFromMailboxSummary;

    @FindBy(xpath = "//div[2]/div/div/div[1]/div/div/div[1]/span[2]")
    protected M messageIdentifierInDetails;

    @FindBy(xpath = "//clr-accordion-panel/div/div[1]/button/clr-accordion-title")
    protected M messageData;

    @FindBy(xpath = "//clr-dg-row/div/div[2]/div/clr-dg-cell[2]/div")
    protected M messageIdElement;

    @FindBy(xpath = "//clr-dg-cell[7]/div/div[2]/button/span")
    protected M goToDetails;

    @FindBy(xpath = "//clr-dg-column[2]/div/clr-dg-string-filter/clr-dg-filter/button")
    protected M refColFilter;

    @FindBy(xpath = "//div[@role='combobox']/input")
    protected M domainKey;

    @FindBy(xpath = "//ng-select/ng-dropdown-panel/div/div[2]/div/div/div[1]/span")
    protected M domainValue;

    @FindBy(xpath = "//domain-control/div/div[1]/span")
    protected M domainId;

    @FindBy(xpath = "//div/input[@name='search']")
    protected M search;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//button[contains(text(),'Create a new Message')]")
    protected M createNewMessage;

    @FindBy(xpath = "//div[1]/div[1]/label[contains(text(),'INBOX')]")
    protected M inboxTab;

    @FindBy(xpath = "//div[1]/div/label[contains(text(),'SENT')]")
    protected M sentTab;

    @FindBy(xpath = "//div[1]/div/label[contains(text(),'ARCHIVE')]")
    protected M archiveTab;

    public void navigateToMailbox() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1,TIMEOUT_MINUTE);

        try {
            BrowserUtils.waitForClickable(mailboxIcon, Global_VARS.TIMEOUT_MINUTE);
            if (!mailboxIcon.isSelected()) {
                base.clickTheMenu(mailboxIcon);
                System.out.println("Clicked the Mailbox menu");
            }
        } catch (Exception e) {
            System.out.println("Exception in navigating to Mailbox  tab " + e);
        }


    }

    public void assertSentNewMessage(String messageId, String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(inboxrowCellData1, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(sentTab,TIMEOUT_MINUTE);
            sentTab.click();
            BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitFor("Sent mail - CBX",TIMEOUT_MINUTE);
            filterSentRecord(messageId);
            validateTargetStatus(targetStatus);
            return;
        }catch(Exception e){
            System.out.println("Exception in asserting the created message in SENT tab "+e);
        }

    }

    private void validateTargetStatus(String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(messageData, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(messageStatus,TIMEOUT_MINUTE);
            String identifier = messageIdentifierInDetails.getText();
            String messageActualStatus = messageStatus.getText();
            System.out.println("Actual message status for "+identifier +" is '"+messageActualStatus+"'");
            Assert.assertEquals(messageActualStatus, targetStatus);
            navigateToMailboxSummary();

        }catch(Exception e){
            System.out.println("Exception in loading the role creation form");
        }


    }

    private void navigateToMailboxSummary() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(backFromDetails,TIMEOUT_MINUTE);
            if(backFromDetails.isDisplayed()) {
                backFromDetails.click();
                BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
                System.out.println("Navigated to SENT summary");
            }

        }catch(Exception e){
            System.out.println("Exception is navigation to summary "+e);
        }finally {
            navigateToUserSummary();
        }


    }

    private void navigateToUserSummary() {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        try{
            BrowserUtils.waitForClickable(backFromMailboxSummary,TIMEOUT_MINUTE);
            if(backFromMailboxSummary.isDisplayed()) {
                backFromMailboxSummary.click();
                BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
                System.out.println("Navigated to default User summary");
            }

        }catch(Exception e){
            System.out.println("Exception is navigation to User summary "+e);
        }



    }

    private void filterSentRecord(String messageId) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
        try {
            BrowserUtils.waitForClickable(refColFilter, TIMEOUT_MINUTE);
            refColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(messageId);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(messageIdElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(messageIdElement, messageId, TIMEOUT_MINUTE);
            if (messageIdElement.getText().contains(messageId)) {
                ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(goToDetails,TIMEOUT_MINUTE);
                System.out.println("Filtered the record for message identifier '" + messageId+"'");
                System.out.println("Navigating to Message Details for the identifier '"+messageId+"'");
                goToDetails.click();
            } else {
                System.out.println("Not filtered");
                System.out.println(messageIdElement.getText() + ", " + messageId);
            }

        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);

        }



    }

    public void assertRepliedInboxMessage(String messageId, String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(inboxrowCellData1,TIMEOUT_MINUTE);
            filterRepliedRecord(messageId);
            validateTargetReplyStatus(targetStatus);
            return;
        }catch(Exception e){
            System.out.println("Exception in asserting the replied message in inbox tab "+e);
        }



    }

    private void validateTargetReplyStatus(String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(inboxrowCellData1, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(refElementGrid,TIMEOUT_MINUTE);
            if (refElementGrid.isDisplayed()){
                if (statusElementGrid.getText().contains(targetStatus)){
                    System.out.println("Status of the replied message is "+statusElementGrid.getText());
                    Assert.assertEquals(statusElementGrid.getText(), targetStatus);
                }else {
                    System.out.println("Status of replied message mismatch");
                }
            }
        }catch(Exception e){
            System.out.println("Exception in validating the targetted reply status "+e);
        }



    }

    public void assertRepliedSentMessage(String messageId, String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(sentTab,TIMEOUT_MINUTE);
            sentTab.click();
            BrowserUtils.waitForVisibility(sentrowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitFor("Sent mail - CBX",TIMEOUT_MINUTE);
            filterRepliedRecord(messageId);
            validateTargetReplyStatus(targetStatus);
            return;
        }catch(Exception e){
            System.out.println("Exception in asserting the created message in SENT tab "+e);
        }


    }

    private void filterRepliedRecord(String messageId) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
        try {
            BrowserUtils.waitForClickable(refColFilter, TIMEOUT_MINUTE);
            refColFilter.click();
            BrowserUtils.waitForClickable(search, TIMEOUT_MINUTE);
            search.sendKeys(messageId);
            BrowserUtils.waitForClickable(close, TIMEOUT_MINUTE);
            close.click();
            BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitForVisibility(messageIdElement, TIMEOUT_MINUTE);
            BrowserUtils.waitForGridDataLoad(messageIdElement, messageId, TIMEOUT_MINUTE);
            if (messageIdElement.getText().contains(messageId)) {
                ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                BrowserUtils.waitForClickable(goToDetails, TIMEOUT_MINUTE);
                System.out.println("Filtered the record for message identifier '" + messageId + "'");
            } else {
                System.out.println("Not filtered");
                System.out.println(messageIdElement.getText() + ", " + messageId);
            }

        } catch (Exception e) {
            System.out.println("Exception in filtering the record " + e);
        }
    }

    public void assertRepliedArchiveMessage(String messageId, String targetStatus) throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForVisibility(rowCellData1, TIMEOUT_MINUTE);
        try{
            BrowserUtils.waitForVisibility(archiveTab,TIMEOUT_MINUTE);
            archiveTab.click();
            BrowserUtils.waitForVisibility(archiverowCellData1, TIMEOUT_MINUTE);
            BrowserUtils.waitFor("Archive mail - CBX",TIMEOUT_MINUTE);
            filterRepliedRecord(messageId);
            validateTargetReplyStatus(targetStatus);
            return;
        }catch(Exception e){
            System.out.println("Exception in asserting the created message in SENT tab "+e);
        }



    }
}
