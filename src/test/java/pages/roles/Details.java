package pages.roles;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.Base;

import java.util.List;

import static framework.Global_VARS.TIMEOUT_MINUTE;

public class Details<M extends WebElement> extends Base {

    // local variables
    public static enum ROLE_COLUMN_TYPE { roleName, roleType, domain, status };

    Base base = new Base();

    //constructor
    public Details() throws Exception {
        super();

    }

    @FindBy(xpath = "//ul/li/a[contains(text(),'Roles')]")
    protected M roleTab;

    @FindBy(xpath = "//div[@class='datagrid-scrolling-cells']/clr-dg-cell/div/div")
    protected M cellData;

    @FindBy(xpath = "//div/input[@name='search']")
    protected M searchKey;

    @FindBy(xpath = "//button/clr-icon[@title='Close']")
    protected M close;

    @FindBy(xpath = "//clr-dg-row[\"+i+\"]/div/div[2]/div/clr-dg-cell[4]/record-status-component/div/div[2]/div[1]/span")
    protected M status;


    public void navigateToRole() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForClickable(roleTab, Global_VARS.TIMEOUT_MINUTE);
        base.clickTheMenu(roleTab);
    }

    public void checkCreatedRole() throws Exception {

    }

    public void checkForGridLoad() throws Exception {
        WebDriver driver = CreateDriver.getInstance().getDriver();
        BrowserUtils.waitForGridData(cellData, TIMEOUT_MINUTE);
    }

    public String[] filterRoleDetails(ROLE_COLUMN_TYPE role_column_type, String value) throws Exception {
        String indexQuery = null;
        String resultType = null;
        WebDriver driver = CreateDriver.getInstance().getDriver();

        switch(role_column_type) {
            case roleName:
                indexQuery = "1";
                resultType = "roleName";
                break;
            case roleType:
                indexQuery = "2";
                resultType = "roleType";
                break;
            case domain:
                indexQuery = "4";
                resultType = "domain";
                break;
            case status:
                indexQuery = "4";
                resultType = "status";
                break;
            }

        String customElement = "//clr-dg-column["+ indexQuery +"1]/div/clr-dg-string-filter/clr-dg-filter/button";

        try {
           // To customize >>> base.setFilter(cellData, customElement, searchKey, value, close);
            BrowserUtils.waitForGridData(cellData, TIMEOUT_MINUTE);
            driver.findElement(By.xpath(customElement)).click();
            BrowserUtils.waitForVisibility(searchKey, TIMEOUT_MINUTE);
            if (searchKey.isDisplayed()){
                searchKey.sendKeys(value);
                BrowserUtils.waitForGridData(cellData, TIMEOUT_MINUTE);
            }
            close.click();
            List<WebElement> filteredElements = driver.findElements(By.xpath("//clr-dg-row"));

            String[] elements = new String[filteredElements.size()];

            for (int i=0; i<filteredElements.size();i++){
                if (resultType == "roleName"){
                    String filteredRecords = "//clr-dg-row["+i+"]/div/div[2]/div/clr-dg-cell[1]/div/div[1]";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                } if (resultType == "roleType"){
                    String filteredRecords = "//clr-dg-row["+i+"]/div/div[2]/div/clr-dg-cell[1]/div/div[2]";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                }if (resultType == "domain"){
                    String filteredRecords = "//clr-dg-row["+i+"]/div/div[2]/div/clr-dg-cell[2]/div";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                }if (resultType == "status"){
                    String filteredRecords = "//clr-dg-row["+i+"]/div/div[2]/div/clr-dg-cell[4]/record-status-component/div/div[2]/div[1]/span";
                    elements[i] = driver.findElement(By.xpath(filteredRecords)).getText();
                }
                Assert.assertTrue(elements[i].contains(value));
            }
            System.out.println("Filtered results from Role is/ are "+elements);
        }
        catch(TimeoutException e) {
            System.out.println("Exception in filter by roles");
        }
        return new String[0];
    }
}
