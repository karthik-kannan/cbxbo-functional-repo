package tests;

import framework.BrowserUtils;
import framework.CreateDriver;
import framework.Global_VARS;
import framework.JSONDataProvider;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.WebElement;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;
import pages.LoginEnhance;
import pages.rules.Confirmation;
import pages.rules.Create;
import pages.rules.Review;
import pages.rules.Summary;
import tests.model.Rule;

import static framework.Global_VARS.TARGET_URL;


public class RuleCreation {
    // local vars
    private LoginEnhance<WebElement> login = null;
    private pages.users.Summary<WebElement> userSummary = null;
    private Summary<WebElement> summary = null;
    private Create<WebElement> ruleCreate = null;
    private Review<WebElement> ruleReview = null;
    private Confirmation<WebElement> ruleConfirm = null;

    private static final String DATA_FILE = "src\\data\\CreateRule.json";


    // constructor
    public RuleCreation() throws Exception {
    }

    // setup/teardown methods

    /**
     * suiteSetup method
     *
     * @param environment
     * @param context
     * @throws Exception
     */
    @Parameters({"environment"})
    @BeforeSuite(alwaysRun = true, enabled = true)
    protected void suiteSetup(@Optional(Global_VARS.ENVIRONMENT) String environment,
                              ITestContext context)
            throws Exception {

        Global_VARS.DEF_ENVIRONMENT = System.getProperty("environment", environment);
        Global_VARS.SUITE_NAME = context.getSuite().getXmlSuite().getName();
    }

    /**
     * suiteTeardown method
     *
     * @throws Exception
     */
    @AfterSuite(alwaysRun = true, enabled = true)
    protected void suiteTeardown() throws Exception {
    }

    /**
     * testSetup method
     *
     * @param browser
     * @param platform
     * @param includePattern
     * @param excludePattern
     * @param ctxt
     * @throws Exception
     */
    @Parameters({"browser", "platform", "includePattern", "excludePattern"})
    @BeforeTest(alwaysRun = true, enabled = true)
    protected void testSetup(@Optional (Global_VARS.BROWSER) String browser,
                             @Optional (Global_VARS.PLATFORM) String platform,
                             @Optional String includePattern,
                             @Optional String excludePattern,
                             ITestContext ctxt)
            throws Exception {

        // data provider filters
        if ( includePattern != null ) {
            System.setProperty("includePattern", includePattern);
        }

        if ( excludePattern != null ) {
            System.setProperty("excludePattern", excludePattern);
        }

        // global variables
        Global_VARS.DEF_BROWSER = System.getProperty("browser", browser);
        Global_VARS.DEF_PLATFORM = System.getProperty("platform", platform);

        // create driver
        CreateDriver.getInstance().setDriver(Global_VARS.DEF_BROWSER,
                Global_VARS.DEF_PLATFORM,
                Global_VARS.DEF_ENVIRONMENT);
    }

    /**
     * testTeardown method
     *
     * @throws Exception
     */
    @AfterTest(alwaysRun = true, enabled = true)
    protected void testTeardown() throws Exception {
        // close driver
        CreateDriver.getInstance().closeDriver();
    }

    /**
     * testClassSetup method
     *
     * @param context
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true, enabled = true)
    protected void testClassSetup(ITestContext context) throws Exception {
        // instantiate page object classes

        login = new LoginEnhance<>();
        userSummary = new pages.users.Summary<>();
        summary = new pages.rules.Summary<WebElement>();
        ruleCreate = new pages.rules.Create<WebElement>();
        ruleReview = new pages.rules.Review<WebElement>();
        ruleConfirm = new pages.rules.Confirmation<WebElement>();

        // set datafile for data provider
        JSONDataProvider.dataFile = DATA_FILE;
    }

    /**
     * testClassTeardown method
     *
     * @param context
     * @throws Exception
     */
    @AfterClass(alwaysRun = true, enabled = true)
    protected void testClassTeardown(ITestContext context) throws Exception {

    }

    /**
     * testMethodSetup method
     *
     * @param result
     * @throws Exception
     */
    @BeforeMethod(alwaysRun = true, enabled = true)
    protected void testMethodSetup(ITestResult result) throws Exception {
         }

    /**
     * testMethodTeardown method
     *
     * @param result
     * @throws Exception
     */
    @AfterMethod(alwaysRun = true, enabled = true)
    protected void testMethodTeardown(ITestResult result) throws Exception {
        BrowserUtils.screenShot(result);
        userSummary.navigateToUser();
        login.logoutTargetApp();

    }

    // test methods

    /**
     *
     * @param rowID
     * @param description
     * @param testData
     * @throws Exception
     */
    @Test(groups={"FUNCTIONAL"}, dataProvider="fetchData_JSON", dataProviderClass=JSONDataProvider.class, enabled=true)
    public void tc001_createRule(String rowID,
                                   String description,
                                   JSONObject testData) throws Exception {

        Rule rule = new Rule(testData);
        String entity = rule.getEntity();
        String event = rule.getEvent();
        String domain = rule.getDomain();
        String ruleName = rule.getRuleName();
        String channels = rule.getChannels();
        String from = rule.getFrom();
        String to = rule.getTo();
        String workFlowType = rule.getWorkFlowType();
        String product = rule.getProduct();
        JSONArray accounts = rule.getAccounts();
        JSONArray virtualAccounts = rule.getVirtualAccounts();
        String authRequired = rule.getAuthInfo().get("required").toString();
        String roleMap = rule.getAuthInfo().get("role").toString();
        String operator = rule.getAuthInfo().get("operator").toString();
        String group = rule.getAuthInfo().get("group").toString();
        String targetStatus = rule.getTargetStatus();

        login.doContextualLogin(entity, event);
        summary.navigateToRule();
        ruleCreate.loadRuleForm();
        ruleCreate.setDomainInfo(domain,ruleName,channels,from,to,product,workFlowType);

       // roleCreate.setProductInfo(subProducts);

        ruleCreate.setAccountInfo();//to add test data for selection of accounts

     //   ruleCreate.setVirtualAccountInfo(); //to add test data for selection of accounts

        ruleCreate.setAuthorisation(authRequired,roleMap,operator,group);

        ruleReview.verifyRuleWithVAInfo(ruleName,domain, product, channels, from, to, authRequired);

        ruleConfirm.checkRuleSubmission(ruleName,domain,targetStatus);

        summary.assertCreatedData(targetStatus,ruleName);

    }

}
